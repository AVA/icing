(**
  This file defines the value trees datatype and some functions operating on it.
  Value trees are described in Section 2.3 of our CAV 2019 paper
**)
open machine_ieeeTheory binary_ieeeTheory lift_ieeeTheory listTheory;
open icingPreamble;

val _ = new_theory "valueTrees";

(** Define datatypes for operations once and reuse them also on syntax level **)
val _ = Datatype `
  unop = Neg | Sqrt;
  binop = Plus | Minus | Mult | Div;
  terop = Fma;
  cmp = Less | Leq | Eq;
  pred = NaN;
  scope = Opt`;

(** Formal definition of value trees **)
val _ = Datatype `
  valTree = Wconst word64
            | UnVal unop valTree
            | BinVal binop valTree valTree
            | TerVal terop valTree valTree valTree
            | Scope scope valTree;

  cvalTree = Bconst bool
            | Cmp cmp valTree valTree
            | Pred pred valTree
            | Cscope scope cvalTree;

  result =  Val valTree
          | Cval cvalTree
          | Lval (valTree list)`;

(* Function tree2IEEE in the CAV paper **)
val tree2IEEE_def = Define `
  tree2IEEE (Wconst c) = c /\
  tree2IEEE (UnVal op v) =
    (case op of
    | Neg => fp64_negate
    | Sqrt => fp64_sqrt roundTiesToEven) (tree2IEEE v) /\
  tree2IEEE (BinVal b v1 v2) =
    (case b of
    | Plus => fp64_add
    | Minus => fp64_sub
    | Mult => fp64_mul
    | Div => fp64_div) roundTiesToEven (tree2IEEE v1) (tree2IEEE v2) /\
  tree2IEEE (TerVal op v1 v2 v3) =
    (case op of
    | Fma => fp64_mul_add roundTiesToEven)
      (tree2IEEE v1) (tree2IEEE v2) (tree2IEEE v3) /\
  tree2IEEE (Scope s v) = tree2IEEE v
  /\
  cTree2IEEE (Bconst b) = b /\
  cTree2IEEE (Pred p v) =
    (case p of
    |NaN => fp64_isNan (tree2IEEE v)) /\
  cTree2IEEE (Cmp cmp v1 v2) =
    ((case cmp of
    |Less => fp64_lessThan
    |Leq => fp64_lessEqual
    |Eq => fp64_equal) (tree2IEEE v1) (tree2IEEE v2)) /\
  cTree2IEEE (Cscope sc v) = cTree2IEEE v`;

val _ = export_theory();
