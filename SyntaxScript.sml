(**
  This file defines the syntax of Icing expressions and utility functions
  computing sets of variables.
  The syntax of Icing is described in section 2.1 of our CAV 2019 paper
**)
open machine_ieeeTheory binary_ieeeTheory lift_ieeeTheory;
open valueTreesTheory;

open icingPreamble;

val _ = new_theory "Syntax";

(** Formal definition of the syntax defined in Figure 1 **)
val _ = Datatype `
  expr =
        (* Leaf nodes, constants and variables *)
          Wconst word64
        | Var string
        (* Lists are lists of Icing expressions *)
        | List (expr list)
        (* Simple projection into lists *)
        | Ith num expr
        (* arithmetic, unary, binary, and ternary ops *)
        | Unop unop expr
        | Binop binop expr expr
        | Terop terop expr expr expr
        (* Control flow - let bindings and if-statements *)
        | Let string expr expr
        | Cond cexpr expr expr
        (* Scoping, make all things below optimizable *)
        | Scope scope expr
        (* List manipulating functions map and fold and their value tree
           counterparts which are only used in the semantics *)
        | Map string expr expr
        | Mapvl string expr (valTree list)
        | Fold (string # string) expr expr expr
        | Foldvl (string # string) expr expr (valTree list);
  (* Conditional expressions *)
  cexpr =
        (* Constants *)
        | Bconst bool
        (* Predicate checks; i.e. isNan/ value checks*)
        | Pred pred expr
        (* logical ops: <, <= and = *)
        | Cmp cmp expr expr
        (* boolean Scoping *)
        | Cscope scope cexpr`;

(**
  Functions to compute the free variables and the used variables of expressions.
  Free variables are not let-bound, and not parameters of lambdas for fold and
  map.
  Used variables are free variables, let-bound variables and parameters of
  lambdas **)
val freeVarsExp_def =  tDefine
  "freeVarsExp"
  `(freeVarsExp (Wconst _) = {}) /\
  (freeVarsExp (Var x) = {x}) /\
  (freeVarsExp (List el) = freeVarsExpl el) /\
  (freeVarsExp (Ith _ e) = freeVarsExp e) /\
  (freeVarsExp (Unop u e) = freeVarsExp e) /\
  (freeVarsExp (Binop b e1 e2) = (freeVarsExp e1) UNION (freeVarsExp e2)) /\
  (freeVarsExp (Terop t e1 e2 e3) = (freeVarsExp e1) UNION (freeVarsExp e2) UNION (freeVarsExp e3)) /\
  (freeVarsExp (Let x e1 e2) = (freeVarsExp e1) UNION ((freeVarsExp e2) DIFF {x})) /\
  (freeVarsExp (Cond c e1 e2) =  (freeVarsCexp c) UNION (freeVarsExp e1) UNION (freeVarsExp e2)) /\
  (freeVarsExp (Scope s e) = freeVarsExp e) /\
  (freeVarsExp (Map x f el) = (freeVarsExp f DIFF {x}) UNION (freeVarsExp el)) /\
  (freeVarsExp (Mapvl x f vl) = (freeVarsExp f DIFF {x})) /\
  (freeVarsExp (Fold (x,y) f i el) =
    (freeVarsExp f DIFF {x;y}) UNION (freeVarsExp i) UNION (freeVarsExp el)) /\
  (freeVarsExp (Foldvl (x,y) f i vl) =
    (freeVarsExp f DIFF {x;y}) UNION (freeVarsExp i))
  /\
  (freeVarsCexp (Bconst c) = {}) /\
  (freeVarsCexp (Pred p e) = freeVarsExp e) /\
  (freeVarsCexp (Cmp _ e1 e2) = (freeVarsExp e1) UNION (freeVarsExp e2)) /\
  (freeVarsCexp (Cscope sc ce) = freeVarsCexp ce)
  /\
  freeVarsExpl [] = {} /\
  freeVarsExpl (a::al) = (freeVarsExp a) UNION (freeVarsExpl al)`
  (WF_REL_TAC `measure
                (\x. case x of
                  |INL e => expr_size e
                  |INR (INL ce) => cexpr_size ce
                  |INR (INR el) => expr1_size el)`);

val usedVarsExp_def = tDefine
  "usedVarsExp"
  `(usedVarsExp (Wconst _) = {}) /\
  (usedVarsExp (Var x) = {x}) /\
  (usedVarsExp (List el) = usedVarsExpl el) /\
  (usedVarsExp (Ith _ e) = usedVarsExp e) /\
  (usedVarsExp (Unop u e) = usedVarsExp e) /\
  (usedVarsExp (Binop b e1 e2) = (usedVarsExp e1) UNION (usedVarsExp e2)) /\
  (usedVarsExp (Terop t e1 e2 e3) = (usedVarsExp e1) UNION (usedVarsExp e2) UNION (usedVarsExp e3)) /\
  (usedVarsExp (Let x e1 e2) = {x} UNION (usedVarsExp e1) UNION (usedVarsExp e2)) /\
  (usedVarsExp (Cond c e1 e2) = (usedVarsCexp c) UNION (usedVarsExp e1) UNION (usedVarsExp e2)) /\
  (usedVarsExp (Scope sc e) = usedVarsExp e) /\
  (usedVarsExp (Map x f el) = {x} UNION (usedVarsExp f) UNION (usedVarsExp el)) /\
  (usedVarsExp (Mapvl x f vl) = {x} UNION (usedVarsExp f)) /\
  (usedVarsExp (Fold (x,y) f i el) =
    {x;y} UNION (usedVarsExp f) UNION (usedVarsExp i) UNION (usedVarsExp el)) /\
  (usedVarsExp (Foldvl (x,y) f i vl) =
    {x;y} UNION (usedVarsExp f) UNION (usedVarsExp i))
  /\
  (usedVarsCexp (Bconst b) = {}) /\
  (usedVarsCexp (Pred _ e) = usedVarsExp e) /\
  (usedVarsCexp (Cmp _ e1 e2) = (usedVarsExp e1) UNION (usedVarsExp e2)) /\
  (usedVarsCexp (Cscope _ e) = usedVarsCexp e)
  /\
  (usedVarsExpl [] = {}) /\
  (usedVarsExpl (e::el) = (usedVarsExp e) UNION (usedVarsExpl el))`
  (WF_REL_TAC `measure
                (\x. case x of
                  |INL e => expr_size e
                  |INR (INL ce) => cexpr_size ce
                  |INR (INR el) => expr1_size el)`);

val _ = export_theory();
