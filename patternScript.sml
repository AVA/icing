(**
  Formalization of the pattern language and top-level matching and
  application functions which are later used to define rewriting
  The pattern language and rewriting are defined in section 2.2 of the CAV 2019
  paper. Proofs about the pattern matching and instantiation are in
  patternProofsScript.sml **)
open SyntaxTheory valueTreesTheory;

open icingPreamble;

val _ = new_theory "pattern";

(**
  Inductive Datatype for our pattern language
  Must support matching on any expression, variables are used for patterns
**)
val _ = Datatype `
  pat =
    Word word64
  | Bool bool
  | Var num
  | Unop unop pat
  | Binop binop pat pat
  | Terop terop pat pat pat
  | Pred pred pat
  | Cmp cmp pat pat
  | Scope scope pat;`

(* A rewrite rule is a pair of two patterns.
  The left hand side is the pattern to match against
  and the right hand side is the returned value/expression *)
val _ = type_abbrev ("rw_rule", ``:pat # pat``);
val _ = temp_type_abbrev ("val_subst", ``:num |-> valTree``);
val _ = temp_type_abbrev ("exp_subst", ``:num |-> expr``);

(* Use HOL4's new monad syntax *)
val _  = monadsyntax.enable_monadsyntax();
val _ = List.app monadsyntax.enable_monad ["option"];

(* Matching a pattern with the top-level of a value tree,
  if a matching exists an option with a substitution is returned.
  The matcher takes an additional substituion as argument to make sure
  that we do not double match a pattern to different expressions
*)
val matchesValTree_def = Define
  `matchesValTree (Word w1) (Wconst w2) s =
    (if (w1 = w2) then SOME s else NONE) /\
   matchesValTree (Var n) v s =
    (case FLOOKUP s n of
    | SOME v1 => if v1 = v then SOME s else NONE
    | NONE => SOME (s |+ (n,v))) /\
  matchesValTree (Unop op1 p) (UnVal op2 v) s =
    (if (op1 = op2)
     then matchesValTree p v s
     else NONE) /\
  matchesValTree (Binop b1 p1 p2) (BinVal b2 v1 v2) s =
    (if (b1 = b2)
     then
       do
       s1 <- matchesValTree p1 v1 s;
       matchesValTree p2 v2 s1;
       od
     else NONE) /\
  matchesValTree (Terop t1 p1 p2 p3) (TerVal t2 v1 v2 v3) s =
    (if (t1 = t2)
    then
      do
        s1 <- matchesValTree p1 v1 s;
        s2 <- matchesValTree p2 v2 s1;
        matchesValTree p3 v3 s2;
      od
    else NONE) /\
  matchesValTree (Scope sc1 p) (valueTrees$Scope sc2 v) s =
    (if sc1 = sc2 then matchesValTree p v s else NONE) /\
  matchesValTree _ _ s = NONE`

val matchesCvalTree = Define `
  matchesCvalTree (Bool b1) (Bconst b2) s =
    (if (b1 = b2) then SOME s else NONE) /\
  matchesCvalTree (Pred pred1 p) (Pred pred2 v) s =
    (if (pred1 = pred2) then matchesValTree p v s else NONE) /\
  matchesCvalTree (Cmp cmp1 p1 p2) (Cmp cmp2 v1 v2) s =
    (if (cmp1 = cmp2)
    then
      (do
        s1 <- matchesValTree p1 v1 s;
        matchesValTree p2 v2 s1;
      od)
    else NONE) /\
  matchesCvalTree (Scope sc1 p) (Cscope sc2 v) s =
    (if (sc1 = sc2) then matchesCvalTree p v s else NONE) /\
  matchesCvalTree _ _ s = NONE`

(* Instantiate a given pattern with a substitution into a value tree *)
val appValTree_def = Define `
    appValTree (Word w) s = SOME (Wconst w) /\
    appValTree (Var n) s = FLOOKUP s n /\
    appValTree (Unop u p) s = (do v <- appValTree p s; return (UnVal u v); od) /\
    appValTree (Binop op p1 p2) s =
      (do
        v1 <- appValTree p1 s;
        v2 <- appValTree p2 s;
        return (BinVal op v1 v2);
      od) /\
    appValTree (Terop op p1 p2 p3) s =
      (do
        v1 <- appValTree p1 s;
        v2 <- appValTree p2 s;
        v3 <- appValTree p3 s;
        return (TerVal op v1 v2 v3);
      od) /\
    appValTree (Scope sc p) s =
      (do
        v <- appValTree p s;
        return (Scope sc v);
      od) /\
    appValTree _ _ = NONE
  /\
    appCvalTree (Bool b) s = SOME (Bconst b) /\
    appCvalTree (Pred pr p1) s =
      (do
        v <- appValTree p1 s;
        return (Pred pr v);
      od) /\
    appCvalTree (Cmp cmp p1 p2) s =
      (do
        v1 <- appValTree p1 s;
        v2 <- appValTree p2 s;
        return (Cmp cmp v1 v2);
      od) /\
    appCvalTree (Scope sc p) s =
      (do
        vc <- appCvalTree p s;
        return (Cscope sc vc)
      od) /\
    appCvalTree _ _ = NONE`;

(* Same matching function for expressions *)
val matchesExpr_def = Define
  `matchesExpr (Word w1) (Wconst w2) s =
    (if (w1 = w2) then SOME s else NONE) /\
   matchesExpr (Var n) v s =
    (case FLOOKUP s n of
    | SOME v1 => if v1 = v then SOME s else NONE
    | NONE => SOME (s |+ (n,v))) /\
  matchesExpr (Unop op1 p) (Unop op2 v) s =
    (if (op1 = op2)
     then matchesExpr p v s
     else NONE) /\
  matchesExpr (Binop b1 p1 p2) (Binop b2 v1 v2) s =
    (if (b1 = b2)
     then
       do
       s1 <- matchesExpr p1 v1 s;
       matchesExpr p2 v2 s1;
       od
     else NONE) /\
  matchesExpr (Terop t1 p1 p2 p3) (Terop t2 v1 v2 v3) s =
    (if (t1 = t2)
    then
      do
        s1 <- matchesExpr p1 v1 s;
        s2 <- matchesExpr p2 v2 s1;
        matchesExpr p3 v3 s2;
      od
    else NONE) /\
  matchesExpr (Scope sc1 p) (Syntax$Scope sc2 v) s =
    (if sc1 = sc2 then matchesExpr p v s else NONE) /\
  matchesExpr _ _ s = NONE`;

val matchesCexpr_def = Define `
  matchesCexpr (Bool b1) (Bconst b2) s =
    (if (b1 = b2) then SOME s else NONE) /\
  matchesCexpr (Pred pr1 p) (Pred pr2 e) s =
    (if (pr1 = pr2) then matchesExpr p e s else NONE) /\
  matchesCexpr (Cmp cmp1 p1 p2) (Cmp cmp2 v1 v2) s =
    (if (cmp1 = cmp2)
    then
      (do
        s1 <- matchesExpr p1 v1 s;
        matchesExpr p2 v2 s1;
      od)
    else NONE) /\
  matchesCexpr (Scope sc1 p) (Cscope sc2 v) s =
    (if (sc1 = sc2) then matchesCexpr p v s else NONE) /\
  matchesCexpr _ _ s = NONE`;

(* Instantiate a given pattern with a substitution into an expression *)
val appExpr_def = Define `
    appExpr (Word w) s = SOME (Wconst w) /\
    appExpr (Var n) s = FLOOKUP s n /\
    appExpr (Unop u p) s = (do v <- appExpr p s; return (Unop u v); od) /\
    appExpr (Binop op p1 p2) s =
      (do
        v1 <- appExpr p1 s;
        v2 <- appExpr p2 s;
        return (Binop op v1 v2);
      od) /\
    appExpr (Terop op p1 p2 p3) s =
      (do
        v1 <- appExpr p1 s;
        v2 <- appExpr p2 s;
        v3 <- appExpr p3 s;
        return (Terop op v1 v2 v3);
      od) /\
    appExpr (Scope sc p) s =
      (do
        v <- appExpr p s;
        return (Scope sc v);
      od) /\
    appExpr _ _ = NONE
  /\
    appCexpr (Bool b) s = SOME (Bconst b) /\
    appCexpr (Pred pr p) s =
      (do
        v <- appExpr p s;
        return (Pred pr v);
      od) /\
    appCexpr (Cmp cmp p1 p2) s =
      (do
        v1 <- appExpr p1 s;
        v2 <- appExpr p2 s;
        return (Cmp cmp v1 v2);
      od) /\
    appCexpr (Scope sc p) s =
      (do
        vc <- appCexpr p s;
        return (Cscope sc vc)
      od) /\
    appCexpr _ _ = NONE`;

(** Rewriting on value trees is done in the semantics by picking a path
  that walks down the value tree structure and then applies the rewrite in place
  if it matches **)

(* Datatype for paths into a value tree. Here is the leaf node meaning that the
  rewrite should be applied *)
val _ = Datatype `
  path =
  | Left path | Right path | Center path | Here`;

(* Function rwPathValTree b rw p v recurses through value tree v using path p
  until p = Here or no further recursion is possible because of a mismatch.
  In case of a mismatch the function simply returns None.
  Flag b is used to track whether we have passed an `opt` annotation allowing
  optimizations to be applied.
  Only if b is true, and p = Here, the rewrite rw is applied. *)
val rwPathValTree_def = Define `
  rwPathValTree F rw Here v = NONE /\
  rwPathValTree T rw Here v =
    (let (lhs, rhs) = rw in
      (case matchesValTree lhs v FEMPTY of
        | NONE => NONE
        | SOME subst => appValTree rhs subst)) /\
  rwPathValTree b rw (Left p) (BinVal op v1 v2) =
    OPTION_MAP (\v1. BinVal op v1 v2) (rwPathValTree b rw p v1) /\
  rwPathValTree b rw (Right p) (BinVal op v1 v2) =
    OPTION_MAP (\v2. BinVal op v1 v2) (rwPathValTree b rw p v2) /\
  rwPathValTree b rw (Center p) (UnVal op v1) =
    OPTION_MAP (\v. UnVal op v) (rwPathValTree b rw p v1) /\
  rwPathValTree b rw (Left p) (TerVal op v1 v2 v3) =
    OPTION_MAP (\v1. TerVal op v1 v2 v3) (rwPathValTree b rw p v1) /\
  rwPathValTree b rw (Center p) (TerVal op v1 v2 v3) =
    OPTION_MAP (\v2. TerVal op v1 v2 v3) (rwPathValTree b rw p v2) /\
  rwPathValTree b rw (Right p) (TerVal op v1 v2 v3) =
    OPTION_MAP (\v3. TerVal op v1 v2 v3) (rwPathValTree b rw p v3) /\
  rwPathValTree b rw (Center p) (Scope sc v) =
    OPTION_MAP (\v. Scope sc v) (rwPathValTree (sc = Opt \/ b) rw p v) /\
  rwPathValTree _ _ _ _ = NONE
  /\
  rwPathCvalTree F crw Here cv = NONE /\
  rwPathCvalTree T crw Here cv =
    (let (lhs,rhs) = crw in
      (case matchesCvalTree lhs cv FEMPTY of
      | NONE => NONE
      | SOME subst => appCvalTree rhs subst)) /\
  rwPathCvalTree b rw (Center p) (Pred pr v) =
    OPTION_MAP (\v. Pred pr v) (rwPathValTree b rw p v) /\
  rwPathCvalTree b rw (Left p) (Cmp cmp v1 v2) =
    OPTION_MAP (\v1. Cmp cmp v1 v2) (rwPathValTree b rw p v1) /\
  rwPathCvalTree b rw (Right p) (Cmp cmp v1 v2) =
    OPTION_MAP (\v2. Cmp cmp v1 v2) (rwPathValTree b rw p v2) /\
  rwPathCvalTree b rw (Center p) (Cscope sc v) =
    OPTION_MAP (\v. Cscope sc v) (rwPathCvalTree (sc = Opt \/ b) rw p v) /\
  rwPathCvalTree _ _ _ _ = NONE`;

(* Datatype holding a single rewrite application in the form of a path into the
  value tree and a number giving the index of the rewrite to be used *)
val _ = Datatype `
  rewrite_app =
    | RewriteApp path num (* which rewrite rule *)`;

(* rwAllValTree rwApps canOpt rws v applies all the rewrite_app's in rwApps to
    value tree v using rwPathValTree *)
val rwAllValTree_def = Define `
  rwAllValTree [] canOpt rws v = SOME v /\
  rwAllValTree ((RewriteApp p index)::rs) canOpt rws v =
    case oEL index rws of
    | NONE => NONE
    | SOME rw =>
      (case rwPathValTree canOpt rw p v of
      | NONE => NONE
      | SOME v_new => rwAllValTree rs canOpt rws v_new)`;

val rwAllCvalTree_def = Define `
  rwAllCvalTree [] canOpt rws v = SOME v /\
  rwAllCvalTree ((RewriteApp p index)::rs) canOpt rws v =
    case oEL index rws of
    | NONE => NONE
    | SOME rw =>
      (case rwPathCvalTree canOpt rw p v of
      | NONE => NONE
      | SOME v_new => rwAllCvalTree rs canOpt rws v_new)`;

(* rewriteExp: Non-recursive, expression rewriting function applying all rewrites that match.
  A non-matching rewrite is silently ignored *)
val rewriteExp_def = Define `
  rewriteExp ([]:rw_rule list) (e:expr) = e /\
  rewriteExp ((lhs,rhs)::rwtl) e =
    (case matchesExpr lhs e FEMPTY of
    |  SOME subst =>
      (case appExpr rhs subst of
      | SOME e_opt => rewriteExp rwtl e_opt
      | NONE => rewriteExp rwtl e)
    | NONE => rewriteExp rwtl e)`;

val rewriteCexp_def = Define `
  rewriteCexp ([]:rw_rule list) (ce:cexpr) = ce /\
  rewriteCexp ((lhs, rhs)::rwtl) ce =
    (case matchesCexpr lhs ce FEMPTY of
      | SOME subst =>
        (case appCexpr rhs subst of
          | SOME ce_opt => rewriteCexp rwtl ce_opt
          | NONE => rewriteCexp rwtl ce)
      | NONE => rewriteCexp rwtl ce)`;

 val _ = export_theory();
