(**
  This file contains proofs about the matching and instantiation functions
  defined in patternScript.sml
  It also contains some compatibility lemmas for rwAllValTree, the value tree
  rewriting function
**)

open SyntaxTheory patternTheory;

open icingPreamble;

val _ = new_theory "patternProofs";

(**
  Sanity lemma: rewriting on expressions does not introduce new variables
**)
local
  val varMatch_tac =
      imp_res_tac (ONCE_REWRITE_RULE [SUBSET_DEF] FRANGE_FUPDATE_SUBSET)
      \\ fs[matchesExpr_def, matchesCexpr_def, option_case_eq, usedVarsExp_def, flookup_thm]
      \\ rveq
      \\ fs[IN_UNION, usedVarsExp_def, SUBSET_DEF];
in
Theorem image_subst_is_usedVars
  `(! e subst s_init pat.
   matchesExpr pat e s_init = SOME subst ==>
   !e_sub. e_sub IN (FRANGE subst) ==>
   e_sub IN (FRANGE s_init) \/
   usedVarsExp (e_sub) SUBSET (usedVarsExp e))
  /\
  (! c subst s_init cpat.
       matchesCexpr cpat c s_init = SOME subst ==>
       ! e_sub. e_sub IN (FRANGE subst) ==>
       e_sub IN (FRANGE s_init) \/
       usedVarsExp (e_sub) SUBSET (usedVarsCexp c))
  /\
  (!l. l = (l:expr list) ==> T)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac \\ fs[] \\ rpt strip_tac
  \\ (Cases_on `pat` ORELSE Cases_on `cpat`)
  \\ fs[matchesExpr_def, matchesCexpr_def, option_case_eq] \\ rveq
  \\ res_tac
  \\ varMatch_tac)
end;

Theorem usedVars_from_image_subst
  `!pat.
    (! e subst.
      appExpr pat subst = SOME e ==>
      usedVarsExp e SUBSET
        { v | ?n e. FLOOKUP subst n = SOME e /\ v IN usedVarsExp e})
    /\
    (! c subst.
      appCexpr pat subst = SOME c ==>
      usedVarsCexp c SUBSET
        { v | ? n e. FLOOKUP subst n = SOME e /\ v IN usedVarsExp e})`
  (Induct_on `pat` \\ rpt strip_tac
  \\ fs[appExpr_def, option_case_eq, usedVarsExp_def, SUBSET_DEF]
  \\ rpt strip_tac \\ rveq \\ fs[Once usedVarsExp_def]
  \\ asm_exists_tac \\ fs[]);

Theorem rewrite_preserves_usedVars
  `(!e e_new lhs rhs subst.
      matchesExpr lhs e FEMPTY = SOME subst /\
      appExpr rhs subst = SOME e_new ==>
      usedVarsExp e_new SUBSET usedVarsExp e) /\
    (!c c_new lhs rhs subst.
      matchesCexpr lhs c FEMPTY = SOME subst /\
      appCexpr rhs subst = SOME c_new ==>
      usedVarsCexp c_new SUBSET usedVarsCexp c)`
  (conj_tac \\ rpt strip_tac
  \\ irule SUBSET_TRANS
  \\ imp_res_tac usedVars_from_image_subst
  \\ asm_exists_tac \\ fs[]
  \\ imp_res_tac image_subst_is_usedVars
  \\ fs[FRANGE_FEMPTY, SUBSET_DEF]
  \\ rpt strip_tac
  \\ fs[FRANGE_FLOOKUP]
  \\ res_tac);

(* Substitutions are only added to but not overwritten *)
Theorem matchValTree_preserving
  `!p.
    (!s1 s2 v.
      matchesValTree p v s1 = SOME s2 ==>
      ! n val.
        FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val)
  /\
    (! s1 s2 cv.
      matchesCvalTree p cv s1 = SOME s2 ==>
      ! n val.
        FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[]
  \\ (Cases_on `cv` ORELSE Cases_on `v`)
  \\ fs[matchesValTree_def, matchesCvalTree_def] \\ rveq
  \\ fs[option_case_eq] \\ rveq \\ fs[]
  \\ res_tac \\ fs[]
  \\ irule FLOOKUP_SUBMAP \\ asm_exists_tac \\ fs[flookup_thm]);

Theorem matchExpr_preserving
  `! p.
    (! e s1 s2.
      matchesExpr p e s1 = SOME s2 ==>
      ! n val.
        FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val)
  /\
    (! ce s1 s2.
      matchesCexpr p ce s1 = SOME s2 ==>
      ! n val.
        FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[]
  \\ (Cases_on `ce` ORELSE Cases_on `e`)
  \\ fs[matchesExpr_def, matchesCexpr_def] \\ rpt strip_tac
  \\ res_tac
  \\ res_tac
  \\ fs[option_case_eq] \\ rveq \\ fs[]
  \\ irule FLOOKUP_SUBMAP \\ asm_exists_tac \\ fs[flookup_thm]);

(* We can add dummy substitutions *)
Theorem appValTree_weakening
  `! p.
    (! v s1 s2.
      (! n val. FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val) /\
      appValTree p s1 = SOME v ==>
      appValTree p s2 = SOME v)
  /\
    (! cv s1 s2.
      (! n val. FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val) /\
      appCvalTree p s1 = SOME cv ==>
      appCvalTree p s2 = SOME cv)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[appValTree_def]
  \\ fs[pair_case_eq, option_case_eq]
  \\ res_tac \\ fs[]);

Theorem appExpr_weakening
  `! p.
      (! e s1 s2.
        (! n val. FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val) /\
        appExpr p s1 = SOME e ==>
        appExpr p s2 = SOME e)
  /\
    (! ce s1 s2.
      (! n val. FLOOKUP s1 n = SOME val ==> FLOOKUP s2 n = SOME val) /\
      appCexpr p s1 = SOME ce ==>
      appCexpr p s2 = SOME ce)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[]
  \\ fs[appExpr_def, pair_case_eq, option_case_eq]
  \\ res_tac \\ fs[]);

(* Sanity lemmas *)
val valSolve_tac =
  (let
    val thms = CONJ_LIST 2 (SIMP_RULE std_ss [FORALL_AND_THM] appValTree_weakening)
  in
  (irule (hd thms) ORELSE irule (hd (tl thms)))
  end)
  \\ once_rewrite_tac [CONJ_COMM]
  \\ asm_exists_tac \\ fs[]
  \\ rpt strip_tac
  \\ imp_res_tac matchValTree_preserving \\ fs[];

Theorem subst_pat_is_val
  `! p.
    (! v s1 s2.
      matchesValTree p v s1 = SOME s2 ==>
      appValTree p s2 = SOME v)
  /\
    (! cv s1 s2.
      matchesCvalTree p cv s1 = SOME s2 ==>
      appCvalTree p s2= SOME cv)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[]
  \\ (Cases_on `v` ORELSE Cases_on `cv`)
  \\ fs[matchesValTree_def, matchesCvalTree_def, appValTree_def, option_case_eq]
  \\ rveq
  \\ fs[FLOOKUP_SUBMAP, flookup_thm]
  \\ res_tac \\ fs[]
  \\ rpt conj_tac \\ valSolve_tac);

val exprSolve_tac =
  (let
    val thms = CONJ_LIST 2 (SIMP_RULE std_ss [FORALL_AND_THM] appExpr_weakening)
  in
  (irule (hd thms) ORELSE irule (hd (tl thms)))
  end)
  \\ once_rewrite_tac [CONJ_COMM]
  \\ asm_exists_tac \\ fs[]
  \\ rpt strip_tac
  \\ imp_res_tac matchExpr_preserving \\ fs[];

Theorem subst_pat_is_exp
  `! p.
    (! e s1 s2.
          matchesExpr p e s1 = SOME s2 ==>
          appExpr p s2 = SOME e)
  /\
    (! ce s1 s2.
          matchesCexpr p ce s1 = SOME s2 ==>
          appCexpr p s2 = SOME ce)`
  (Induct_on `p`
  \\ rpt strip_tac \\ fs[] \\ rpt strip_tac
  \\ (Cases_on `e` ORELSE Cases_on `ce`)
  \\ fs[matchesExpr_def, matchesCexpr_def, option_case_eq]
  \\ rveq \\ fs[appExpr_def, FLOOKUP_UPDATE]
  \\ res_tac \\ fs[]
  \\ rpt conj_tac \\ exprSolve_tac);

Theorem app_match_sym
  `! p.
     (! s v. appValTree p s = SOME v ==> matchesValTree p v s = SOME s) /\
     (! s cv. appCvalTree p s = SOME cv ==> matchesCvalTree p cv s = SOME s)`
  (Induct_on `p` \\ fs[appValTree_def, matchesValTree_def, matchesCvalTree_def]
  \\ rpt strip_tac
  \\ rveq \\ res_tac
  \\ fs[matchesValTree_def, matchesCvalTree_def]);

val rwAllValTree_empty_rewrites = store_thm (
  "rwAllValTree_empty_rewrites[simp]",
  ``! canOpt insts v1 v2.
      rwAllValTree insts canOpt [] v1 = SOME v2 ==>
      v1 = v2 /\ insts = []``,
  Induct_on `insts` \\ fs[rwAllValTree_def]
  \\ ntac 4 strip_tac \\ CCONTR_TAC \\ fs[]
  \\ Cases_on `h` \\ Cases_on `v1` \\ fs[rwAllValTree_def, oEL_def]);

val rwAllCvalTree_empty_rewrites = store_thm (
  "rwAllCvalTree_empty_rewrites[simp]",
  ``! canOpt insts v1 v2.
      rwAllCvalTree insts canOpt [] v1 = SOME v2 ==>
      v1 = v2 /\ insts = []``,
  Induct_on `insts` \\ fs[rwAllCvalTree_def]
  \\ ntac 4 strip_tac \\ CCONTR_TAC \\ fs[]
  \\ Cases_on `h` \\ Cases_on `v1` \\ fs[rwAllCvalTree_def, oEL_def]);

val rwAllValTree_id = store_thm (
  "rwAllValTree_id[simp]",
  ``! canOpt rws v. ? insts. rwAllValTree insts canOpt rws v = SOME v``,
  rpt strip_tac \\ qexists_tac `[]` \\ EVAL_TAC);

val rwAllCvalTree_id = store_thm (
  "rwAllCvalTree_id[simp]",
  ``! canOpt rws v. ? insts. rwAllCvalTree insts canOpt rws v = SOME v``,
  rpt strip_tac \\ qexists_tac `[]` \\ EVAL_TAC);

(* val _ = export_rewrites ["rwAllValTree_id"]; *)

val rwAllValTree_up = store_thm (
  "rwAllValTree_up",
  ``! insts canOpt rws1 rws2 v1 v2.
      set rws1 SUBSET set rws2 /\
      rwAllValTree insts canOpt rws1 v1 = SOME v2 ==>
      ?insts2. rwAllValTree insts2 canOpt rws2 v1 = SOME v2``,
  Induct_on `insts` \\ fs[rwAllValTree_def] \\ rpt strip_tac
  \\ Cases_on `h` \\ rename1 `RewriteApp pth ind`
  \\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ fs[SUBSET_DEF, MEM_EL, oEL_EQ_EL]
  \\ `?n. n < LENGTH rws2 /\ rw = EL n rws2`
      by (first_x_assum (qspecl_then [`rw`] irule)
          \\ asm_exists_tac \\ fs[])
  \\ rename1` rwAllValTree insts3 canOpt rws2 v_new = SOME v2`
  \\ qexists_tac `RewriteApp pth n :: insts3`
  \\ fs[rwAllValTree_def, oEL_THM]
  \\ qpat_assum `EL n rws2 = _` (fn thm => once_rewrite_tac [GSYM thm])
  \\ fs[]);

val rwAllCvalTree_up = store_thm (
  "rwAllCvalTree_up",
  ``! insts canOpt rws1 rws2 v1 v2.
      set rws1 SUBSET set rws2 /\
     rwAllCvalTree insts canOpt rws1 v1 = SOME v2 ==>
      ?insts2. rwAllCvalTree insts2 canOpt rws2 v1 = SOME v2``,
  Induct_on `insts` \\ fs[rwAllCvalTree_def] \\ rpt strip_tac
  \\ Cases_on `h` \\ rename1 `RewriteApp pth ind`
  \\ fs[rwAllCvalTree_def, option_case_eq]
  \\ res_tac
  \\ fs[SUBSET_DEF, MEM_EL, oEL_EQ_EL]
  \\ `?n. n < LENGTH rws2 /\ rw = EL n rws2`
      by (first_x_assum (qspecl_then [`rw`] irule)
          \\ asm_exists_tac \\ fs[])
  \\ rename1` rwAllCvalTree insts3 canOpt rws2 v_new = SOME v2`
  \\ qexists_tac `RewriteApp pth n :: insts3`
  \\ fs[rwAllCvalTree_def, oEL_THM]
  \\ qpat_assum `EL n rws2 = _` (fn thm => once_rewrite_tac [GSYM thm])
  \\ fs[]);

val rwAllValTree_comp_unop = store_thm (
  "rwAllValTree_comp_unop",
  ``! v vres insts canOpt rws u.
      rwAllValTree insts canOpt rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (UnVal u v) = SOME (UnVal u vres)``,
  Induct_on `insts` \\ rpt strip_tac
  \\ fs[rwAllValTree_def]
  \\ Cases_on `h` \\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspec_then `u` assume_tac) \\ fs[]
  \\ qexists_tac `(RewriteApp (Center p) n):: insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_right = store_thm (
  "rwAllValTree_comp_right",
  ``! b v1 v2 vres insts canOpt rws.
      rwAllValTree insts canOpt rws v2 = SOME vres ==>
      ?insts_new.
        rwAllValTree insts_new canOpt rws (BinVal b v1 v2) = SOME (BinVal b v1 vres)``,
  Induct_on `insts` \\ rpt strip_tac
  \\ fs[rwAllValTree_def]
  \\ Cases_on `h` \\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`v1`, `b`] assume_tac)
  \\ fs[]
  \\ qexists_tac `(RewriteApp (Right p) n):: insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_left = store_thm (
  "rwAllValTree_comp_left",
  ``! b v1 v2 vres insts canOpt rws.
      rwAllValTree insts canOpt rws v1 = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (BinVal b v1 v2) = SOME (BinVal b vres v2)``,
  Induct_on `insts` \\ rpt strip_tac
  \\ fs[rwAllValTree_def]
  \\ Cases_on `h` \\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`v2`, `b`] assume_tac)
  \\ fs[]
  \\ qexists_tac `(RewriteApp (Left p) n):: insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_terop_l = store_thm (
  "rwAllValTree_comp_terop_l",
  ``! v vres v2 v3 insts canOpt rws t.
      rwAllValTree insts canOpt rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (TerVal t v v2 v3) = SOME (TerVal t vres v2 v3)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllValTree_def]
  \\ Cases_on `h `\\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`v3`, `v2`, `t`] assume_tac) \\ fs[]
  \\ qexists_tac `(RewriteApp (Left p) n)::insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_terop_r = store_thm (
  "rwAllValTree_comp_terop_r",
  ``! v vres v1 v2 insts canOpt rws t.
      rwAllValTree insts canOpt rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (TerVal t v1 v2 v) = SOME (TerVal t v1 v2 vres)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllValTree_def]
  \\ Cases_on `h `\\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`v2`, `v1`, `t`] assume_tac) \\ fs[]
  \\ qexists_tac `(RewriteApp (Right p) n)::insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_terop_c = store_thm (
  "rwAllValTree_comp_terop_c",
  ``! v vres v1 v2 insts canOpt rws t.
      rwAllValTree insts canOpt rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (TerVal t v1 v v2) = SOME (TerVal t v1 vres v2)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllValTree_def]
  \\ Cases_on `h `\\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`v2`, `v1`, `t`] assume_tac) \\ fs[]
  \\ qexists_tac `(RewriteApp (Center p) n)::insts_new`
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwPathValTree_cond_T = store_thm (
  "rwPathValTree_cond_T",
  ``! p. (! canOpt rw v v_opt.
      rwPathValTree canOpt rw p v = SOME v_opt ==>
      rwPathValTree T rw p v = SOME v_opt) /\
    (! canOpt rw cv cv_opt.
      rwPathCvalTree canOpt rw p cv = SOME cv_opt ==>
      rwPathCvalTree T rw p cv = SOME cv_opt)``,
  reverse (Induct_on `p`) \\ fs[] \\ rpt strip_tac
  >- (Cases_on `canOpt` \\ fs[rwPathValTree_def])
  >- (Cases_on `canOpt` \\ fs[rwPathValTree_def])
  \\ (Cases_on `v` ORELSE Cases_on `cv`) \\ fs[rwPathValTree_def] \\ res_tac);

val rwAllValTree_cond_T = store_thm (
  "rwAllValTree_cond_T",
  ``! insts. (! canOpt rws v v_opt.
      rwAllValTree insts canOpt rws v = SOME v_opt ==>
      rwAllValTree insts T rws v = SOME v_opt) /\
    (! canOpt rws cv cv_opt.
      rwAllCvalTree insts canOpt rws cv = SOME cv_opt ==>
      rwAllCvalTree insts T rws cv = SOME cv_opt)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllCvalTree_def,rwAllValTree_def]
  \\ Cases_on `h` \\ fs[rwAllCvalTree_def,rwAllValTree_def, option_case_eq]
  \\ imp_res_tac rwPathValTree_cond_T
  \\ asm_exists_tac \\ fs[]
  \\ first_x_assum irule \\ asm_exists_tac \\ fs[]);

val rwAllValTree_comp_scope_T = store_thm (
  "rwAllValTree_comp_scope_T",
  ``! sc v vres insts canOpt rws t.
      rwAllValTree insts T rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (Scope sc v) = SOME (Scope sc vres)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllValTree_def]
  \\ Cases_on `h `\\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`sc`, `v_new`, `vres`, `canOpt`, `rws`] assume_tac) \\ fs[]
  \\ res_tac
  \\ qexists_tac `(RewriteApp (Center p) n)::insts_new`
  \\ Cases_on `sc`
  \\ imp_res_tac rwPathValTree_cond_T
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_comp_scope = store_thm (
  "rwAllValTree_comp_scope",
  ``! sc v vres insts canOpt rws t.
      rwAllValTree insts canOpt rws v = SOME vres ==>
      ? insts_new.
        rwAllValTree insts_new canOpt rws (Scope sc v) = SOME (Scope sc vres)``,
  Induct_on `insts` \\ rpt strip_tac \\ fs[rwAllValTree_def]
  \\ Cases_on `h `\\ fs[rwAllValTree_def, option_case_eq]
  \\ res_tac
  \\ first_x_assum (qspecl_then [`sc`, `v_new`, `vres`, `canOpt`, `rws`] assume_tac) \\ fs[]
  \\ res_tac
  \\ qexists_tac `(RewriteApp (Center p) n)::insts_new`
  \\ Cases_on `sc`
  \\ imp_res_tac rwPathValTree_cond_T
  \\ fs[rwAllValTree_def, rwPathValTree_def]);

val rwAllValTree_chaining_exact = store_thm (
  "rwAllValTree_chaining_exact",
  ``!v1 v2 v3 insts1 insts2 canOpt rws.
    rwAllValTree insts1 canOpt rws v1 = SOME v2 /\
    rwAllValTree insts2 canOpt rws v2 = SOME v3 ==>
    rwAllValTree (APPEND insts1 insts2) canOpt rws v1 = SOME v3``,
  Induct_on `insts1` \\ rpt strip_tac
  \\ fs[rwAllValTree_def]
  \\ Cases_on `h` \\ fs[rwAllValTree_def, option_case_eq]);

val rwAllValTree_chaining = store_thm (
  "rwAllValTree_chaining",
  ``! insts1 v1 v2 v3 insts2 canOpt rws.
    rwAllValTree insts1 canOpt rws v1 = SOME v2 /\
    rwAllValTree insts2 canOpt rws v2 = SOME v3 ==>
    ?insts3. rwAllValTree insts3 canOpt rws v1 = SOME v3``,
  metis_tac[rwAllValTree_chaining_exact]);

val rwAllCvalTree_chaining_exact = store_thm (
  "rwAllCvalTree_chaining_exact",
  ``!v1 v2 v3 insts1 insts2 canOpt rws.
    rwAllCvalTree insts1 canOpt rws v1 = SOME v2 /\
    rwAllCvalTree insts2 canOpt rws v2 = SOME v3 ==>
    rwAllCvalTree (APPEND insts1 insts2) canOpt rws v1 = SOME v3``,
  Induct_on `insts1` \\ rpt strip_tac
  \\ fs[rwAllCvalTree_def]
  \\ Cases_on `h` \\ fs[rwAllCvalTree_def, option_case_eq]);

val rwAllCvalTree_chaining = store_thm (
  "rwAllCvalTree_chaining",
  ``! insts1 v1 v2 v3 insts2 canOpt rws.
    rwAllCvalTree insts1 canOpt rws v1 = SOME v2 /\
    rwAllCvalTree insts2 canOpt rws v2 = SOME v3 ==>
    ?insts3. rwAllCvalTree insts3 canOpt rws v1 = SOME v3``,
  metis_tac[rwAllCvalTree_chaining_exact]);

val _ = export_theory();
