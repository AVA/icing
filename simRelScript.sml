(**
  This file defines the simulation relations between value trees
  and executions
**)

open SyntaxTheory SemanticsTheory valueTreesTheory;
open icingPreamble;

val _ = new_theory "simRel";

val valEq_def = Define `
  valEq (Val v1) (Val v2) = (tree2IEEE v1 = tree2IEEE v2) /\
  valEq (Cval v1) (Cval v2) = (cTree2IEEE v1 = cTree2IEEE v2) /\
  valEq (Lval []) (Lval []) = T /\
  valEq (Lval (v1::vl1)) (Lval (v2::vl2)) =
    ((tree2IEEE v1 = tree2IEEE v2) /\ valEq (Lval vl1) (Lval vl2)) /\
  valEq _ _ = F`;

Theorem valEq_lval
  `valEq (Lval vl1) v2 = ? vl2. valEq (Lval vl1) (Lval vl2) /\ Lval vl2 = v2`
  (Cases_on `v2` \\ fs[valEq_def]);

Theorem valEq_val
  `valEq (Val v1) vr2 = ? v2. valEq (Val v1) (Val v2) /\ Val v2 = vr2`
  (Cases_on `vr2` \\ fs[valEq_def]);

Theorem valEq_cval
  `valEq (Cval v1) vr2 = ? v2. valEq (Cval v1) (Cval v2) /\ Cval v2 = vr2`
  (Cases_on `vr2` \\ fs[valEq_def]);

Theorem valEq_sym
  `valEq v1 v2 = valEq v2 v1`
  (Induct_on `v1` \\ Cases_on `v2` \\ fs[valEq_def]
  >- metis_tac []
  >- metis_tac []
  \\ Induct_on `l` \\ fs[valEq_def]
  >- (Cases_on `l` \\ metis_tac [valEq_def])
  \\ rpt strip_tac \\ Cases_on `l'` \\ fs[valEq_def]
  \\ metis_tac [] )

val val_valEq = curry save_thm "val_valEq" (ONCE_REWRITE_RULE [valEq_sym] valEq_val);

Theorem valEq_lval_length
  `! vl1 vl2. valEq (Lval vl1) (Lval vl2) ==> LENGTH vl1 = LENGTH vl2`
  (Induct_on `vl1` \\ Cases_on `vl2` \\ fs[valEq_def])

Theorem valEq_lval_val_eq
  `! vl1 vl2.
    valEq (Lval vl1) (Lval vl2) ==>
    (! n v1.
      oEL n vl1 = SOME v1 ==>
      ? v2. oEL n vl2 = SOME v2 /\ valEq (Val v1) (Val v2))`
  (Induct_on `vl1` \\ Cases_on `vl2` \\ fs[valEq_def, oEL_def]
  \\ rpt strip_tac
  \\ Cases_on `n = 0` \\ fs[])

Theorem valEq_refl[simp]
  `valEq v1 v1`
  (Induct_on `v1` \\ fs[valEq_def] \\ Induct_on `l` \\ fs[valEq_def]);

Theorem valEq_trans
  `! v1 v2 v3.
    valEq v1 v2 /\ valEq v2 v3 ==> valEq v1 v3`
  (Induct_on `v1` \\ fs[Once valEq_lval, Once valEq_val, Once valEq_cval, valEq_def]
  \\ rpt strip_tac \\ rveq \\ fs[Once valEq_lval, Once valEq_val, Once valEq_cval]
  \\ rveq \\ fs[valEq_def]
  \\ rpt (pop_assum mp_tac) \\ qid_spec_tac `vl2` \\ qid_spec_tac `vl2'`
  \\ Induct_on `l` \\ Cases_on `vl2` \\ Cases_on `vl2'` \\ fs[valEq_def]
  \\ rpt strip_tac \\ fs[] \\ first_x_assum irule \\ find_exists_tac \\ fs[])

Theorem valEq_wrap
  `! v1 v2.
    valEq v1 v2 ==> valEq (wrapScope v1) (wrapScope v2)`
  (Cases_on `v1` \\ Cases_on `v2` \\ fs[valEq_def, wrapScope_def, tree2IEEE_def]
  \\ qid_spec_tac `l'`
  \\ Induct_on `l` \\ Cases_on `l'` \\ rpt strip_tac
  \\ fs[wrapAll_def, valEq_def, tree2IEEE_def])

Theorem valEq_wrap_left
 `! v1 v2.
    valEq v1 v2 ==> valEq (wrapScope v1) v2`
  (Cases_on `v1` \\ Cases_on `v2` \\ fs[valEq_def, wrapScope_def, tree2IEEE_def]
  \\ qid_spec_tac `l'`
  \\ Induct_on `l` \\ Cases_on `l'` \\ rpt strip_tac
  \\ fs[wrapAll_def, valEq_def, tree2IEEE_def])

val (simExp_rules, simExp_ind, simExp_cases) = Hol_reln `
  (! x y. x = y ==> simExp (Var x) (Var y)) /\
  (! w1 w2. (w1 = w2) ==> simExp (Wconst w1) (Wconst w2)) /\
  (! l1 l2.  simExpL l1 l2 ==> simExp (List l1) (List l2)) /\
  (! n1 n2 e1 e2.
    (n1 = n2 /\ simExp e1 e2) ==>
    simExp (Ith n1 e1) (Ith n2 e2)) /\
  (! u1 u2 e1 e2.
    (u1 = u2 /\ simExp e1 e2) ==>
    simExp (Unop u1 e1) (Unop u2 e2)) /\
  (! b1 b2 e11 e21 e12 e22.
    (b1 = b2 /\ simExp e11 e21 /\ simExp e12 e22) ==>
    simExp (Binop b1 e11 e12) (Binop b2 e21 e22)) /\
  (! t1 t2 e11 e12 e13 e21 e22 e23.
    (t1 = t2 /\ simExp e11 e21 /\ simExp e12 e22 /\ simExp e13 e23) ==>
    simExp (Terop t1 e11 e12 e13) (Terop t2 e21 e22 e23)) /\
  (! x y e11 e12 e21 e22.
    (x = y /\ simExp e11 e21 /\ simExp e12 e22) ==>
    simExp (Let x e11 e12) (Let y e21 e22)) /\
  (! c1 c2 e11 e12 e21 e22.
    (simCexp c1 c2 /\ simExp e11 e21 /\ simExp e12 e22) ==>
    simExp (Cond c1 e11 e12) (Cond c2 e21 e22)) /\
  (! x y f g e1 e2.
    (x = y /\ simExp f g /\ simExp e1 e2) ==>
    simExp (Map x f e1) (Map y g e2)) /\
  (! x y f g vl1 vl2.
    (x = y /\ simExp f g /\ valEq (Lval vl1) (Lval vl2)) ==>
    simExp (Mapvl x f vl1) (Mapvl y g vl2)) /\
  (! x f g i1 i2 e1 e2.
    (x = y /\ simExp f g /\ simExp i1 i2 /\ simExp e1 e2) ==>
    simExp (Fold x f i1 e1) (Fold y g i2 e2)) /\
  (! x y f g i1 i2 vl1 vl2.
    (x = y /\ simExp f g /\ simExp i1 i2 /\ valEq (Lval vl1) (Lval vl2)) ==>
    simExp (Foldvl x f i1 vl1) (Foldvl y g i2 vl2)) /\
  (! sc1 sc2 e1 e2.
    (sc1 = sc2 /\ simExp e1 e2) ==>
    simExp (Scope sc1 e1) (Scope sc2 e2))
  /\
  (! b1 b2. b1 = b2 ==> simCexp (Bconst b1) (Bconst b2)) /\
  (! p1 p2 e1 e2.
    (p1 = p2 /\ simExp e1 e2) ==>
    simCexp (Pred p1 e1) (Pred p2 e2)) /\
  (! cmp1 cmp2 e11 e21 e12 e22.
    (cmp1 = cmp2 /\ simExp e11 e21 /\ simExp e12 e22) ==>
    simCexp (Cmp cmp1 e11 e12) (Cmp cmp2 e21 e22)) /\
  (! sc1 sc2 c1 c2.
    (sc1 = sc2 /\ simCexp c1 c2) ==>
    simCexp (Cscope sc1 c1) (Cscope sc2 c2))
  /\
  simExpL [] [] /\
  (! e1 e2 el1 el2.
    (simExp e1 e2 /\ simExpL el1 el2) ==>
    simExpL (e1 :: el1) (e2::el2))`;

val simExp_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once simExp_cases])
  [``simExp (Wconst w1) e``,
  ``simExp (Var x) e``,
  ``simExp (List l1) e``,
  ``simExp (Ith n1 e1) e``,
  ``simExp (Unop u1 e1) e``,
  ``simExp (Binop b1 e11 e12) e``,
  ``simExp (Terop t1 e11 e12 e13) e``,
  ``simExp (Let x e11 e12) e``,
  ``simExp (Cond c1 e11 e12) e``,
  ``simExp (Map x f e1) e``,
  ``simExp (Mapvl x f vl1) e``,
  ``simExp (Fold x f i1 e1) e``,
  ``simExp (Foldvl x f i1 vl1) e``,
  ``simExp (Scope sc1 e1) e``,
  ``simCexp (Bconst b1) e``,
  ``simCexp (Pred p1 e1) e``,
  ``simCexp (Cmp cmp1 e11 e12) e``,
  ``simCexp (Cscope sc1 c1) e``,
  ``simExpL [] expl``,
  ``simExpL (e1::el1) expl``]
  |> LIST_CONJ |> curry save_thm "simExp_cases";

Theorem simExp_refl[simp]
  `(! e. simExp e e) /\ (! c. simCexp c c) /\ (! l. simExpL l l)`
  (ho_match_mp_tac expr_induction \\ rpt strip_tac \\ fs[simExp_cases])


  (** Define a result relation. Given an evaluation of an expression to a value
  tree, its result should be the tree2Const evaluation of the value tree **)
val resultsIn_def = Define `
  resultsIn cfg E e vr =
    (? v. evalExpNonDet cfg E e v /\
      valEq v vr)`;

val choiceTo_def = Define `
  choiceTo cfg E ce r =  (? cv. evalCexpNonDet cfg E ce cv /\ cTree2IEEE cv = r)`;

(* Simulation relation on environments and some proofs about it *)
val simEnv_def = Define `
  simEnv E1 E2 =
    ! x (v1:result).
      E1 x = SOME v1 ==>
      ? v2. E2 x = SOME v2 /\
        valEq v1 v2`;

Theorem simEnv_refl[simp]
  `! E1. simEnv E1 E1`
  (fs[simEnv_def, valEq_refl] \\ rpt strip_tac \\ fs[]);

val _ = export_theory();
