(**
  This file shows semantic equivalence for the translation
  from Icing to CakeML.
  The description of the CakeML connection can be found in
  Section 5 in the paper.
  The translation function is defined in the file CakeMLconnScript.sml

  expToCML_forward proves the forward simulation,
  expToCML_backward proves the backwards simulation and
  expToCML_equiv combines them into the final equivalence theorem
**)

open patternTheory valueTreesTheory SyntaxTheory wellDefinedExpTheory
     SemanticsTheory simRelTheory CompilerTacticsLib CompilerProofsTheory
     CakeMLconnTheory;
open astTheory fpSemTheory evaluateTheory semanticPrimitivesTheory
     namespaceTheory namespacePropsTheory terminationTheory integerTheory
     miscTheory;
open machine_ieeeTheory binary_ieeeTheory;

open ListProgTheory ml_translatorLib ml_progLib
     ml_translatorTheory ml_progTheory;
open icingPreamble;

val _ = new_theory "CakeMLconnProofs";

val _ = translation_extends "ListProg";

Globals.max_print_depth := (~1);
(* val _ = translation_extends "basisProg"; *)

val state = get_env (ml_translatorLib.get_ml_prog_state());
val ffi_state = get_state (ml_translatorLib.get_ml_prog_state());

(* true_exp is true, false_exp is false *)

val true_exp_true = store_thm (
  "true_exp_true[simp]",
  ``! st env. evaluate st env [true_exp] = (st, Rval [Boolv T])``,
  fs[true_exp_def, evaluate_def, do_app_def, opb_lookup_def,
     state_component_equality]);

val false_exp_false = store_thm (
  "false_exp_false[simp]",
  ``! st env. evaluate st env [false_exp] = (st, Rval [Boolv F])``,
  fs[false_exp_def, evaluate_def, do_app_def, opb_lookup_def,
     state_component_equality]);

(* Map and Fold can be found in the loaded environments and evaluate to the
  correct function closures *)

Theorem map_defined
 `! ffi_state.
    evaluate ffi_state ^state [map_name] = (ffi_state, Rval [ListProg$map_1_v])`
  (rpt strip_tac \\ fs[evaluate_def, map_name_def] \\ EVAL_TAC);

Theorem fold_defined
 `! ffi_state.
    evaluate ffi_state ^state [fold_name] = (ffi_state, Rval [ListProg$foldr_v])`
  (rpt strip_tac \\ fs[evaluate_def, fold_name_def] \\ EVAL_TAC)

val coerce_def = Define `
  coerce (Litv (Word64 w)) = [w] /\
  coerce (Conv (SOME (TypeStamp "nil" 1)) []) = [] /\
  coerce (Conv (SOME (TypeStamp "::" 1)) ((Litv (Word64 w))::vl::[])) = w::(coerce vl)`;

Theorem fromIcingVal_toIcingVal_id
  `toIcingVal (fromIcingVal r) = SOME v ==>
  valEq v r`
  (Induct_on `r` \\ fs[fromIcingVal_def] \\ rpt strip_tac
  >- (fs[toIcingVal_def] \\ rveq \\ fs[valEq_def, tree2IEEE_def])
  >- (fs[Boolv_def] \\ Cases_on `cTree2IEEE c` \\ fs[toIcingVal_def]
      \\ rveq \\ fs[valEq_def, tree2IEEE_def])
  \\ rpt (pop_assum mp_tac)
  \\ qid_spec_tac `v` \\ qid_spec_tac `l`
  \\ Induct_on `l` \\ fs[fromIcingVal_def, toIcingVal_def]
  \\ rpt strip_tac
  \\ Cases_on `fromIcingVal (Lval l)` \\ fs[]
  \\ Cases_on `toIcingVal (Conv o' l')` \\ fs[]
  \\ Cases_on `x` \\ fs[] \\ rveq \\ fs[valEq_def, tree2IEEE_def]);

Theorem fromIcingVal_list
  `! vl.
    ? ts l.
      fromIcingVal (Lval vl) = Conv ts l /\
      ? vlNew.
      toIcingVal (Conv ts l) = SOME (Lval vlNew)`
  (Induct_on `vl` \\ fs[fromIcingVal_def, toIcingVal_def])

Theorem fromIcingVal_toIcingVal_defined
  `? vNew. (toIcingVal (fromIcingVal v)) = SOME vNew`
  (Induct_on `v` \\ fs[fromIcingVal_def, toIcingVal_def, Boolv_def]
  >- (strip_tac  \\ Cases_on `cTree2IEEE c` \\ fs[toIcingVal_def, bool_type_num_def])
  \\ qid_spec_tac `vNew` \\ Induct_on `l` \\ fs[fromIcingVal_def, toIcingVal_def]
  \\ rpt strip_tac
  \\ qspec_then `l` assume_tac fromIcingVal_list \\ fs[])

Theorem expToCML_env_no_list
  `(! e E s.
    nsLookup (expToCML_env E e) (Long "List" (Short s)) = NONE) /\
  (! c E s.
    nsLookup (cexpToCML_env E c) (Long "List" (Short s)) = NONE) /\
  (! (l:expr list).
    ! e E s. MEM e l ==>
      nsLookup (expToCML_env E e) (Long "List" (Short s)) = NONE)`
  (ho_match_mp_tac expr_induction
  \\ fs[expToCML_env_def, nsLookup_def] \\ rpt strip_tac
  >- (Cases_on `E s` \\ fs[])
  >- (rpt (pop_assum mp_tac)
      \\ qid_spec_tac `E` \\ qid_spec_tac `s` \\ qid_spec_tac `l`
      \\ Induct_on `l` \\ rw[expToCML_env_def, nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s'`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s'`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  >- (rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
      \\ fs[nsLookup_nsAppend_none])
  \\ rpt (first_x_assum (qspecl_then [`E`, `s`] assume_tac))
  \\ fs[nsLookup_nsAppend_none])

Theorem expToCML_env_no_listmod
  `(! e E. nsLookupMod (expToCML_env E e) ["List"] = NONE) /\
  (! c E. nsLookupMod (cexpToCML_env E c) ["List"] = NONE) /\
  (! (l:expr list) E. nsLookupMod (expToCML_env E (List l)) ["List"] = NONE)`
  (ho_match_mp_tac expr_induction
  \\ fs[nsLookupMod_def, expToCML_env_def] \\ rpt strip_tac
  \\ fs[nsLookupMod_nsAppend_none]
  \\ Cases_on `E s` \\ fs[] \\ EVAL_TAC);

val isExtendedEnv_def = Define `
  isExtendedEnv (env1:(string, string, v) namespace) env2 =
    (! x v.
      nsLookup env1 x = SOME v ==>
      nsLookup env2 x = SOME v)`;

Theorem isExtendedEnv_extend
  `! env1 env2 env3.
    isExtendedEnv env1 env2 ==>
    isExtendedEnv (nsAppend env3 env1) (nsAppend env3 env2)`
  (rpt strip_tac \\ fs[isExtendedEnv_def, nsLookup_nsAppend_some]
  \\ rpt strip_tac \\ fs[])

Theorem lookupMod_toCml_none
  `(! e E (x:(string, string) id) p1 p2.
    p1 <> [] /\ id_to_mods x = p1 ++ p2 ==>
    nsLookupMod (expToCML_env E e) p1 = NONE) /\
  (! c E (x:(string, string) id) p1 p2.
    p1 <> [] /\ id_to_mods x = p1 ++ p2 ==>
    nsLookupMod (cexpToCML_env E c) p1 = NONE) /\
  (! (l:expr list) E (x:(string, string) id) p1 p2.
    p1 <> [] /\ id_to_mods x = p1 ++ p2 ==>
    nsLookupMod (expToCML_env E (List l)) p1 = NONE)`
  (ho_match_mp_tac expr_induction \\ rpt strip_tac \\ fs[expToCML_env_def]
  >- (fs[nsEmpty_def] \\ Cases_on `p1` \\ fs[nsLookupMod_def])
  >- (Cases_on `E s` \\ fs[nsEmpty_def]
      >- (Cases_on `p1` \\ fs[nsLookupMod_def])
      \\ Cases_on `p1` \\ fs[nsBind_def, nsLookupMod_def])
  \\ res_tac \\ fs[nsLookupMod_nsAppend_none]
  \\ fs[nsEmpty_def] \\ Cases_on `p1` \\ fs[nsLookupMod_def]);

Theorem isExtendedEnv_extend_right
  `! env1 env2 env3.
    isExtendedEnv env1 env2 /\
    (! (x:(string, string) id) v. nsLookup env3 x = SOME v ==> nsLookup env2 x = NONE) /\
    (! (x:(string, string) id) p1 p2.
      p1 <> [] /\ id_to_mods x = p1 ++ p2 ==> nsLookupMod env2 p1 = NONE) ==>
    isExtendedEnv (nsAppend env1 env3) (nsAppend env2 env3)`
  (rpt strip_tac \\ fs[isExtendedEnv_def, nsLookup_nsAppend_some]
  \\ rpt strip_tac
  >- (res_tac \\ fs[])
  \\ res_tac
  \\ DISJ2_TAC \\ rpt conj_tac \\ fs[]
  \\ first_x_assum MATCH_ACCEPT_TAC);

Theorem isExtendedEnv_monotone
  `! env1 env2 env3.
  isExtendedEnv env1 env3 /\
  isExtendedEnv env2 env1 ==>
  isExtendedEnv env2 env3`
  (rpt strip_tac \\ fs[isExtendedEnv_def])

(** Value simulation for Icing -> CakeML simulation
  We want to establish that if a value is an Lval in Icing, it must have
  list shape on the CakeML side, a Val must be a 64 bit word, and a Cval
  must be a boolean value **)
val valEqCml_def = Define `
  valEqCml (Val v) l =
    (case l of
    |Litv (Word64 w) => tree2IEEE v = w
    | _ => F) /\
  valEqCml (Cval v) l = (Boolv (cTree2IEEE v) = l) /\
  valEqCml (Lval vl) l =
    (case vl of
    | [] => l = Conv (SOME (TypeStamp "[]" 1)) []
    | v::vl =>
      (case l of
      | Conv ts l =>
        (ts = SOME (TypeStamp "::" 1) /\
        (case l of
        | lit::[ls] => valEqCml (Val v) lit /\ valEqCml (Lval vl) ls
        | _ => F))
      | _ => F))`;

Theorem valEqCml_fromIcingVal
  `! v. valEqCml v (fromIcingVal v)`
  (Induct_on `v` \\ rpt strip_tac
  >- (fs[Once valEqCml_def, Once fromIcingVal_def])
  >- (fs[Once valEqCml_def, Once fromIcingVal_def])
  \\ Induct_on `l` \\ simp[Once valEqCml_def, Once fromIcingVal_def]
  \\ strip_tac \\ fs[Once valEqCml_def, Once fromIcingVal_def]);

Theorem valEqCml_wrapScope
  `! v r. valEqCml v r ==> valEqCml (wrapScope v) r`
  (rpt strip_tac \\ Cases_on `v`
  >- (fs[Once valEqCml_def] \\ Cases_on `r` \\ fs[]
      \\ Cases_on `l` \\ fs[] \\ rveq \\ fs[wrapScope_def]
      \\ fs[valEqCml_def, tree2IEEE_def])
  >- (fs[Once valEqCml_def] \\ rveq
      \\ fs[wrapScope_def]
      \\ fs[valEqCml_def, tree2IEEE_def])
  \\ pop_assum mp_tac
  \\ qid_spec_tac `l` \\ qid_spec_tac `r` \\ Induct_on `l` \\ rw[]
  >- (fs[Once valEqCml_def, wrapScope_def] \\ rveq
      \\ fs[wrapAll_def])
  \\ pop_assum mp_tac
  \\ Cases_on `r` \\ simp[Once valEqCml_def]
  \\ rw[] \\ rveq \\ Cases_on `l'` \\ fs[]
  \\ Cases_on `t` \\ fs[]
  \\ Cases_on `t'` \\ fs[]
  \\ res_tac
  \\ fs[wrapScope_def, wrapAll_def]
  \\ simp[Once valEqCml_def]
  \\ fs[Once valEqCml_def]
  \\ Cases_on `h'` \\ fs[] \\ Cases_on `l'` \\ fs[] \\ rveq \\ fs[tree2IEEE_def]);

Theorem expToCML_env_lookup_some
  `(! e E s v.
    nsLookup (expToCML_env E e) s = SOME v ==>
    ?vT n.
      s = Short n /\ E n = SOME vT /\ v = fromIcingVal vT /\ n IN usedVarsExp e) /\
  (! c E s v.
    nsLookup (cexpToCML_env E c) s = SOME v ==>
    ?vT w n.
      s = Short n /\ E n = SOME vT /\ v = fromIcingVal vT /\ n IN usedVarsCexp c) /\
  (! (l:expr list) e E s v.
    nsLookup (expToCML_env E (List l)) s = SOME v ==>
    ? vT n.
      s = Short n /\ E n = SOME vT /\ v = fromIcingVal vT /\ n IN usedVarsExpl l)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac \\ fs[expToCML_env_def]
  >- (Cases_on `E s` \\ fs[nsLookup_def, nsBind_def, nsEmpty_def]
      \\ Cases_on `s'` \\ fs[nsLookup_def] \\ rveq \\ fs[valEqCml_fromIcingVal, usedVarsExp_def])
  \\ fs[nsLookup_nsAppend_some]
  \\ res_tac \\ rveq \\ fs[usedVarsExp_def]
  \\ Cases_on `p` \\ fs[usedVarsExp_def]);

Theorem expToCML_env_lookup_eq
  `(! E e1 e2 v s.
    nsLookup (expToCML_env E e2) s = SOME v ==>
    nsLookup (expToCML_env E e1) s = SOME v \/
    nsLookup (expToCML_env E e1) s = NONE) /\
  (! E e1 e2 v s.
    nsLookup (cexpToCML_env E e2) s = SOME v ==>
    nsLookup (cexpToCML_env E e1) s = SOME v \/
    nsLookup (cexpToCML_env E e1) s = NONE) /\
  (! E c e v s.
    nsLookup (cexpToCML_env E c) s = SOME v ==>
    nsLookup (expToCML_env E e) s = SOME v \/
    nsLookup (expToCML_env E e) s = NONE) /\
  (! E c e v s.
    nsLookup (expToCML_env E e) s = SOME v ==>
    nsLookup (cexpToCML_env E c) s = SOME v \/
    nsLookup (cexpToCML_env E c) s = NONE)`
  (rpt strip_tac
  THENL (map Cases_on
          [`nsLookup (expToCML_env E e1) s`,
           `nsLookup (cexpToCML_env E e1) s`,
           `nsLookup (cexpToCML_env E c) s`,
           `nsLookup (expToCML_env E e) s`])
  \\ fs[]
  \\ imp_res_tac expToCML_env_lookup_some
  \\ rveq \\ fs[] \\ rveq \\ fs[] \\ rveq
  >- (Cases_on `nsLookup (expToCML_env E e) (Short n)`
      \\ fs[]
      \\ imp_res_tac expToCML_env_lookup_some
      \\ fs[] \\ rveq \\ fs[])
  \\ Cases_on `nsLookup (cexpToCML_env E c) (Short n)`
  \\ fs[]
  \\ qpat_x_assum `nsLookup (cexpToCML_env _ _) _ = _`
      (mp_then Any assume_tac (List.nth (CONJ_LIST 3 expToCML_env_lookup_some, 1)))
  \\ fs[] \\ fs[]);

local
  val solve_tac =
    fs[expToCML_env_def, nsLookup_nsAppend_some]
    \\ imp_res_tac expToCML_env_lookup_some
    \\ rveq \\ res_tac \\ fs[bind_def]
    \\ Cases_on `n = x` \\ fs[nsLookup_nsBind, nsLookup_nsAppend];
in
Theorem nsLookup_bind_optBind
  `(! e E x v vr s.
    nsLookup (expToCML_env (bind E x v) e) s = SOME vr ==>
    nsLookup (nsBind x (fromIcingVal v) (expToCML_env E e)) s = SOME vr)
  /\
  (! ce E x v vr s.
    nsLookup (cexpToCML_env (bind E x v) ce) s = SOME vr ==>
    nsLookup (nsBind x (fromIcingVal v) (cexpToCML_env E ce)) s = SOME vr)
  /\
  (! l E x v vr s.
    nsLookup (expToCML_env (bind E x v) (List l)) s = SOME vr ==>
    nsLookup (nsBind x (fromIcingVal v) (expToCML_env E (List l))) s = SOME vr)`
  (ho_match_mp_tac expr_induction \\ rpt strip_tac
  >- (rveq \\ fs[expToCML_env_def])
  >- (rveq
      \\ fs[nsLookup_def, nsBind_def, mk_id_def, bind_def, expToCML_env_def]
      \\ Cases_on `s = x` \\ rveq \\ Cases_on `E s` \\ Cases_on `s'`
      \\ fs[nsBind_def, nsLookup_def, nsEmpty_def]
      \\ rveq \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_assum (qspec_then `e` assume_tac) \\ fs[]
      \\ first_assum (qspec_then `e'` assume_tac) \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (solve_tac
      >- (first_x_assum (mp_then Any assume_tac
              (CONJUNCT2 (CONJUNCT2 (CONJUNCT2 expToCML_env_lookup_eq))))
          \\ first_x_assum (qspec_then `ce` assume_tac) \\ fs[])
      \\ first_assum (mp_then Any assume_tac
              (CONJUNCT2 (CONJUNCT2 (CONJUNCT2 expToCML_env_lookup_eq))))
      \\ first_x_assum (qspec_then `ce` assume_tac) \\ fs[]
      \\ first_x_assum (mp_then Any assume_tac (CONJUNCT1 expToCML_env_lookup_eq))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_assum (qspec_then `e` assume_tac) \\ fs[]
      \\ first_assum (qspec_then `e'` assume_tac) \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (rveq \\ fs[expToCML_env_def])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (solve_tac
      \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
      \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq
      \\ first_x_assum irule \\ fs[])
  >- (fs[expToCML_env_def] \\ rveq \\ fs[])
  \\ solve_tac
  \\ rveq \\ Cases_on `n = x` \\ fs[]
  \\ first_x_assum (mp_then Any assume_tac (hd (CONJ_LIST 2 expToCML_env_lookup_eq)))
  \\ first_x_assum (qspec_then `e` assume_tac) \\ fs[])
end;

Theorem nsLookup_nsBind_cases
  `! x y v env vE.
    nsLookup (nsBind x v env) y = SOME vE ==>
    (y = Short (x) /\  v = vE) \/ (y <> Short x /\ nsLookup env y = SOME vE)`
  (rw[] \\ Cases_on `y = Short x` \\ fs[nsLookup_nsBind])

Theorem isExtendedEnv_append_bin1
  `! e1 e2 env1 E.
  (! x. (x IN usedVarsExp e1 \/ x IN usedVarsExp e2) ==> nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e1) env1)
    (nsAppend (nsAppend (expToCML_env E1 e1) (expToCML_env E1 e2)) env1)`
  (rw[isExtendedEnv_def, nsLookup_nsAppend_some]
  >- (Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[])
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[]
  \\ Cases_on `nsLookup (expToCML_env E1 e2) x` \\ fs[]
  >- (simp[nsLookup_nsAppend_none, nsLookupMod_nsAppend_none]
      \\ rpt strip_tac \\ DISJ1_TAC
      \\ drule (CONJUNCT1 lookupMod_toCml_none)
      \\ disch_then imp_res_tac \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq
  \\ fs[usedVarsExp_def] \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_bin2
  `! e1 e2 env1 E.
  (! x. (x IN usedVarsExp e1 \/ x IN usedVarsExp e2) ==> nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e2) env1)
    (nsAppend (nsAppend (expToCML_env E1 e1) (expToCML_env E1 e2)) env1)`
  (rw[isExtendedEnv_def, nsLookup_nsAppend_some]
  >- (Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[]
    >- (DISJ1_TAC \\ rpt strip_tac \\ drule (hd (CONJ_LIST 3 lookupMod_toCml_none))
        \\ disch_then (qspecl_then [`e1`, `E1`] assume_tac) \\ res_tac)
    \\ first_assum (mp_then Any assume_tac (hd (CONJ_LIST 4 expToCML_env_lookup_eq)))
    \\ first_x_assum (qspec_then `e2` assume_tac)
    \\ imp_res_tac expToCML_env_lookup_some \\ rveq
    \\ fs[] \\ rveq \\ fs[])
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[nsLookup_nsAppend_none]
  >- (rpt strip_tac \\ drule (hd (CONJ_LIST 3 lookupMod_toCml_none))
     \\ disch_then (qspecl_then [`Binop b e1 e2`, `E1`] assume_tac) \\ res_tac
     \\ fs[expToCML_env_def])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq
  \\ fs[usedVarsExp_def] \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_ter1
  `! e1 e2 env1 E.
  (! x. ((x IN usedVarsExp e1 \/ x IN usedVarsExp e2) \/ x IN usedVarsExp e3) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e1) env1)
    (nsAppend
      (nsAppend
        (nsAppend (expToCML_env E1 e1) (expToCML_env E1 e2))
        (expToCML_env E1 e3)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[])
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[]
  \\ Cases_on `nsLookup (expToCML_env E1 e2) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e3) x` \\ fs[]
      >- (fs[nsLookupMod_nsAppend_none]
          \\ rpt strip_tac \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac
      \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_ter2
  `! e1 e2 env1 E.
  (! x. ((x IN usedVarsExp e1 \/ x IN usedVarsExp e2) \/ x IN usedVarsExp e3) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e2) env1)
    (nsAppend
      (nsAppend
        (nsAppend (expToCML_env E1 e1) (expToCML_env E1 e2))
        (expToCML_env E1 e3)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (drule (CONJUNCT1 expToCML_env_lookup_eq)
      \\ disch_then (qspec_then `e1` assume_tac) \\ fs[]
      \\ DISJ1_TAC \\ DISJ1_TAC
      \\ MATCH_ACCEPT_TAC (hd (CONJ_LIST 3 lookupMod_toCml_none)))
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e3) x` \\ fs[nsLookup_nsAppend_none]
      >- (rpt strip_tac \\ fs[nsLookupMod_nsAppend_none] \\ conj_tac
          >- (irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
              \\ find_exists_tac \\ fs[])
          \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq
      \\ res_tac \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_ter3
  `! e1 e2 env1 E.
  (! x. ((x IN usedVarsExp e1 \/ x IN usedVarsExp e2) \/ x IN usedVarsExp e3) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e3) env1)
    (nsAppend
      (nsAppend
        (nsAppend (expToCML_env E1 e1) (expToCML_env E1 e2))
        (expToCML_env E1 e3)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (drule (CONJUNCT1 expToCML_env_lookup_eq)
      \\ disch_then (qspec_then `e1` assume_tac) \\ fs[]
      \\ drule (CONJUNCT1 expToCML_env_lookup_eq)
      \\ disch_then (qspec_then `e2` assume_tac)
      \\ fs[nsLookup_nsAppend_none, nsLookup_nsAppend_some]
      >- (DISJ1_TAC \\ DISJ1_TAC
          \\ MATCH_ACCEPT_TAC (hd (CONJ_LIST 3 lookupMod_toCml_none)))
      \\ DISJ1_TAC \\ fs[nsLookupMod_nsAppend_none]
      \\ rpt strip_tac \\ drule (hd (CONJ_LIST 3 lookupMod_toCml_none))
      \\ disch_then drule
      >- (disch_then (qspecl_then [`e1`, `E1`] assume_tac) \\ fs[])
      \\ disch_then (qspecl_then [`e2`, `E1`] assume_tac) \\ fs[])
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e2) x` \\ fs[]
      >- (rpt strip_tac \\ fs[nsLookupMod_nsAppend_none] \\ conj_tac
          >- (irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
              \\ find_exists_tac \\ fs[])
          \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq
      \\ res_tac \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_cond1
  `! c e1 e2 env1 E.
  (! x. ((x IN usedVarsCexp c \/ x IN usedVarsExp e1) \/ x IN usedVarsExp e2) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (cexpToCML_env E1 c) env1)
    (nsAppend
      (nsAppend
        (nsAppend (cexpToCML_env E1 c) (expToCML_env E1 e1))
        (expToCML_env E1 e2)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (Cases_on `nsLookup (cexpToCML_env E1 c) x` \\ fs[])
  \\ Cases_on `nsLookup (cexpToCML_env E1 c) x` \\ fs[]
  \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e2) x` \\ fs[]
      >- (fs[nsLookupMod_nsAppend_none]
          \\ rpt strip_tac \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac
      \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_cond2
  `! c e1 e2 env1 E.
  (! x. ((x IN usedVarsCexp c \/ x IN usedVarsExp e1) \/ x IN usedVarsExp e2) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e1) env1)
    (nsAppend
      (nsAppend
        (nsAppend (cexpToCML_env E1 c) (expToCML_env E1 e1))
        (expToCML_env E1 e2)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (drule (CONJUNCT2 (CONJUNCT2 (CONJUNCT2 expToCML_env_lookup_eq)))
      \\ disch_then (qspec_then `c` assume_tac) \\ fs[]
      \\ DISJ1_TAC \\ DISJ1_TAC
      \\ MATCH_ACCEPT_TAC (hd (tl (CONJ_LIST 3 lookupMod_toCml_none))))
  \\ Cases_on `nsLookup (cexpToCML_env E1 c) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e2) x` \\ fs[nsLookup_nsAppend_none]
      >- (rpt strip_tac \\ fs[nsLookupMod_nsAppend_none] \\ conj_tac
          >- (irule (CONJUNCT1 (CONJUNCT2 lookupMod_toCml_none)) \\ fs[]
              \\ find_exists_tac \\ fs[])
          \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq
      \\ res_tac \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

Theorem isExtendedEnv_append_cond3
  `! c e1 e2 env1 E.
  (! x. ((x IN usedVarsCexp c \/ x IN usedVarsExp e1) \/ x IN usedVarsExp e2) ==>
    nsLookup env1 (Short x) = NONE) ==>
  isExtendedEnv (nsAppend (expToCML_env E1 e2) env1)
    (nsAppend
      (nsAppend
        (nsAppend (cexpToCML_env E1 c) (expToCML_env E1 e1))
        (expToCML_env E1 e2)) env1)`
  (rw[nsLookup_nsAppend_some, isExtendedEnv_def]
  >- (drule (CONJUNCT2 (CONJUNCT2 (CONJUNCT2 expToCML_env_lookup_eq)))
      \\ disch_then (qspec_then `c` assume_tac) \\ fs[]
      \\ drule (CONJUNCT1 expToCML_env_lookup_eq)
      \\ disch_then (qspec_then `e1` assume_tac)
      \\ fs[nsLookup_nsAppend_none, nsLookup_nsAppend_some]
      >- (DISJ1_TAC \\ DISJ1_TAC
          \\ MATCH_ACCEPT_TAC (hd (tl (CONJ_LIST 3 lookupMod_toCml_none))))
      \\ DISJ1_TAC \\ fs[nsLookupMod_nsAppend_none]
      \\ rpt strip_tac \\ drule (hd (tl (CONJ_LIST 3 lookupMod_toCml_none)))
      \\ disch_then drule
      >- (disch_then (qspecl_then [`c`, `E1`] assume_tac) \\ fs[])
      \\ disch_then kall_tac
      \\ drule (CONJUNCT1 lookupMod_toCml_none) \\ disch_then drule
      \\ disch_then (qspecl_then [`e1`, `E1`] assume_tac) \\ fs[])
  \\ Cases_on `nsLookup (cexpToCML_env E1 c) x` \\ fs[nsLookup_nsAppend_none]
  >- (Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[]
      >- (rpt strip_tac \\ fs[nsLookupMod_nsAppend_none] \\ conj_tac
          >- (irule (CONJUNCT1 (CONJUNCT2 lookupMod_toCml_none)) \\ fs[]
              \\ find_exists_tac \\ fs[])
          \\ DISJ1_TAC
          \\ irule (CONJUNCT1 lookupMod_toCml_none) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ imp_res_tac expToCML_env_lookup_some \\ rveq
      \\ res_tac \\ fs[])
  \\ imp_res_tac expToCML_env_lookup_some \\ rveq \\ res_tac \\ fs[]);

val lookup_hd_thm =
  EVAL ``nsLookup
                (merge_env ListProg_env_8
                   (merge_env ListProg_env_2
                      (merge_env ListProg_env_1
                         (merge_env ListProg_env_0
                            (merge_env ListProg_env
                               (merge_env OptionProg_env_10 init_env)))))).v
                (Short "hd")``;

val lookup_nil_thm =
  EVAL ``nsLookup
              (merge_env ListProg_env_4
                 (merge_env ListProg_env_2
                    (merge_env ListProg_env_1
                       (merge_env ListProg_env_0
                          (merge_env ListProg_env
                             (merge_env OptionProg_env_10 init_env)))))).c
              (Short "[]")``;

val lookup_nil2_thm =
  EVAL ``nsLookup
                      (merge_env ListProg_env_5
                         (merge_env ListProg_env_2
                            (merge_env ListProg_env_1
                               (merge_env ListProg_env_0
                                  (merge_env ListProg_env
                                     (merge_env OptionProg_env_10 init_env)))))).
                      c (Short "[]")``;

val lookup_cons_thm =
  EVAL ``nsLookup
              (merge_env ListProg_env_4
                 (merge_env ListProg_env_2
                    (merge_env ListProg_env_1
                       (merge_env ListProg_env_0
                          (merge_env ListProg_env
                             (merge_env OptionProg_env_10 init_env)))))).c
              (Short "::")``;

val lookup_cons2_thm =
  EVAL ``nsLookup
                      (merge_env ListProg_env_5
                         (merge_env ListProg_env_2
                            (merge_env ListProg_env_1
                               (merge_env ListProg_env_0
                                  (merge_env ListProg_env
                                     (merge_env OptionProg_env_10 init_env)))))).
                      c (Short "::")``;

val lookup_TL_thm =
  EVAL ``nsLookup
                        (merge_env ListProg_env_8
                           (merge_env ListProg_env_2
                              (merge_env ListProg_env_1
                                 (merge_env ListProg_env_0
                                    (merge_env ListProg_env
                                       (merge_env OptionProg_env_10 init_env)))))).
                        v (Short "tl")``;

Theorem num_inject_pos
  `! (n:num). (0:int) <= &n`
  (rpt strip_tac
  \\ fs[]);

Theorem num_inject_not_neg
  `! (n:num). n <> 0 ==> ~ (&n - 1 < 0:int)`
  (rpt strip_tac
  \\ Cases_on `n`
  \\ fs[int_of_num, INT_1, int_arithTheory.elim_minus_ones]
  \\ qspec_then `n'` assume_tac num_inject_pos
  \\ `&n' < &n'`
      by (drule integerTheory.INT_LTE_TRANS
          \\ disch_then (qspec_then `&n'` destruct)
          \\ fs[integerTheory.INT_0])
  \\ fs[])

Theorem int_simp_sub
  `! (n:num). n <> 0 ==> &n -(1:int) = &(n-1)`
  (rpt strip_tac \\ Cases_on `n`
  \\ fs[integerTheory.INT, int_arithTheory.elim_minus_ones]);

Theorem valEqCml_is_fromVal
  `valEqCml v1 r ==> r = fromIcingVal v1`
  (Induct_on `v1`
  >- (rpt strip_tac \\ Cases_on `r` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[fromIcingVal_def])
  >- (rpt strip_tac
      \\ Cases_on `r` \\ fs[Once valEqCml_def, Boolv_def, fromIcingVal_def])
  \\ qid_spec_tac `r` \\ Induct_on `l` \\ rpt strip_tac
  >- (Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq \\ fs[fromIcingVal_def])
  \\ first_x_assum (fn thm => assume_tac (ONCE_REWRITE_RULE [valEqCml_def] thm))
  \\ Cases_on `r` \\ simp[Once valEqCml_def] \\ rveq \\ Cases_on `l` \\ fs[]
  >- (Cases_on `l'` \\ fs[] \\ Cases_on `t` \\ fs[]
      \\ Cases_on `t'` \\ fs[fromIcingVal_def]
      \\ Cases_on `h'` \\ fs[Once valEqCml_def, fromIcingVal_def]
      \\ Cases_on `l` \\ fs[])
  \\ Cases_on `l'` \\ fs[]
  \\ Cases_on `t'` \\ fs[] \\ Cases_on `t''` \\ fs[]
  \\ res_tac \\ rveq
  \\ fs[fromIcingVal_def, Once valEqCml_def]
  \\ Cases_on `h''` \\ fs[] \\ Cases_on `l` \\ fs[]);

val map_v_find_thm =
  EVAL ``evaluate st (merge_env ListProg_env_67 init_env) [map_name]``

Theorem list_nth_forward_thm
  `∀vl E1 e v' r n env1 stres.
       isExtendedEnv
         (nsAppend (expToCML_env E1 e) (merge_env ListProg_env_67 init_env).v)
         env1.v ⇒
       (∀x.
            x ∈ usedVarsExp e ⇒
            nsLookup (merge_env ListProg_env_67 init_env).v (Short x) = NONE) ⇒
       n < LENGTH vl ⇒
       LLOOKUP vl n = SOME v' ⇒
       valEqCml (Lval vl) r ⇒
       ∃stres' r'.
           evaluate (stres with clock := (n * 3) + 1 + stres.clock)
             (merge_env ListProg_env_8
                (merge_env ListProg_env_2
                   (merge_env ListProg_env_1
                      (merge_env ListProg_env_0
                         (merge_env ListProg_env
                            (merge_env OptionProg_env_10 init_env))))) with
              v :=
                nsBind "v2" (Litv (IntLit (&n)))
                  (nsBind "v1" r
                     (build_rec_env
                        [("nth","v1",
                          Fun "v2"
                            (If
                               (App Equality
                                  [Var (Short "v2"); Lit (IntLit 0)])
                               (App Opapp
                                  [Var (Short "hd"); Var (Short "v1")])
                               (App Opapp
                                  [App Opapp
                                     [Var (Short "nth");
                                      App Opapp
                                        [Var (Short "tl"); Var (Short "v1")]];
                                   Let (SOME "k")
                                     (App (Opn Minus)
                                        [Var (Short "v2"); Lit (IntLit 1)])
                                     (If
                                        (App (Opb Lt)
                                           [Var (Short "k"); Lit (IntLit 0)])
                                        (Lit (IntLit 0)) (Var (Short "k")))])))]
                        (merge_env ListProg_env_8
                           (merge_env ListProg_env_2
                              (merge_env ListProg_env_1
                                 (merge_env ListProg_env_0
                                    (merge_env ListProg_env
                                       (merge_env OptionProg_env_10 init_env))))))
                        (merge_env ListProg_env_8
                           (merge_env ListProg_env_2
                              (merge_env ListProg_env_1
                                 (merge_env ListProg_env_0
                                    (merge_env ListProg_env
                                       (merge_env OptionProg_env_10 init_env)))))).
                        v)))
             [If (App Equality [Var (Short "v2"); Lit (IntLit 0)])
                (App Opapp [Var (Short "hd"); Var (Short "v1")])
                (App Opapp
                   [App Opapp
                      [Var (Short "nth");
                       App Opapp [Var (Short "tl"); Var (Short "v1")]];
                    Let (SOME "k")
                      (App (Opn Minus) [Var (Short "v2"); Lit (IntLit 1)])
                      (If (App (Opb Lt) [Var (Short "k"); Lit (IntLit 0)])
                         (Lit (IntLit 0)) (Var (Short "k")))])] =
           (stres',Rval [r']) ∧ valEqCml (Val v') r'`
  (Induct_on `vl` \\ fs[]
  \\ rpt strip_tac \\ rveq \\ fs[LLOOKUP_def]
  \\ Cases_on `n = 0` \\ fs[] \\ rveq
  >- (`? w l. r = Conv (SOME (TypeStamp "::" 1)) [Litv (Word64 w); l]`
        by (Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq
            \\ Cases_on `l` \\ fs[] \\ Cases_on `t` \\ fs[]
            \\ Cases_on `t'` \\ fs[Once valEqCml_def]
            \\ Cases_on `h'` \\ fs[] \\ Cases_on `l` \\ fs[])
      \\ rveq \\ fs[Once valEqCml_def]
      \\ fs[evaluate_def, do_app_def, do_eq_def, do_if_def,
            build_rec_env_def, nsLookup_def, do_opapp_def, lookup_hd_thm,
            hd_v_def]
      \\ fs[ALL_DISTINCT, pat_bindings_def, pmatch_def, lookup_nil_thm,
            lookup_cons_thm, same_type_def, same_ctor_def, Once valEqCml_def])
  \\ `? w l. r = Conv (SOME (TypeStamp "::" 1)) [Litv (Word64 w); l]`
      by (Cases_on `r` \\ fs[Once valEqCml_def]
          \\ Cases_on `l` \\ fs[]
          \\ Cases_on `t` \\ fs[]
          \\ Cases_on `t'` \\ fs[] \\ rveq
          \\ Cases_on `h'` \\ fs[Once valEqCml_def]
          \\ Cases_on `l` \\ fs[])
  \\ rveq \\ fs[Once valEqCml_def]
  \\ simp[evaluate_def, do_app_def, do_eq_def, lit_same_type_def, do_if_def,
          nsLookup_def, nsOptBind_def, opb_lookup_def, opn_lookup_def]
  \\ `~ (&n - 1:int < 0)`
    by (drule num_inject_not_neg  \\ fs[])
  \\ fs[]
  \\ simp[evaluate_def, nsLookup_def, build_rec_env_def, lookup_TL_thm,
          do_opapp_def, tl_v_def, ALL_DISTINCT, pat_bindings_def, pmatch_def,
          lookup_nil2_thm, lookup_cons2_thm, do_app_def, do_eq_def,
          lit_same_type_def, do_if_def]
  \\ simp [same_type_def, same_ctor_def, Once find_recfun_def, dec_clock_def]
  (* now peel away a lambda abstraction for `v2` *)
  \\ simp[Once evaluate_def]
  \\ `?m. n = SUC m` by (Cases_on `n` \\ fs[])
  \\ fs[] \\ rveq \\ once_rewrite_tac [MULT_SUC] \\ simp[]
  \\ first_x_assum drule
  \\ disch_then drule
  \\ disch_then (qspecl_then [`v'`, `l`, `m`, `stres`] destruct)
  >- (fs[usedVarsExp_def]
      \\ qpat_x_assum `valEqCml (Lval _) _`
          (fn thm => fs[ONCE_REWRITE_RULE[Once valEqCml_def] thm]))
  \\ qexistsl_tac [`stres'`, `r'`]
  \\ fs[]
  \\ qpat_x_assum `_ = (stres', Rval [r'])` (fn thm => once_rewrite_tac [GSYM thm])
  \\ rpt (AP_THM_TAC)
  \\ MK_COMB_TAC
  >- (MK_COMB_TAC \\ fs[state_component_equality])
  \\ fs[build_rec_env_def]
  \\ AP_THM_TAC \\ rpt (AP_TERM_TAC)
  \\ MK_COMB_TAC \\ fs[]
  \\ MK_COMB_TAC \\ fs[int_simp_sub]);

Theorem map_forward_thm
  `! vl ck1 v r env1 (stres:'a semanticPrimitives$state) E1 s els e1.
       (∀E1 v (st:'a semanticPrimitives$state) env1 ck.
            isExtendedEnv
              (nsAppend (expToCML_env E1 e1)
                 (merge_env ListProg_env_67 init_env).v) env1.v ∧
          (* the environment contains the constructors *)
        (! x v. nsLookup (^state).c x = SOME v ==>
          nsLookup env1.c x = SOME v) /\
            (∀x.
                 x ∈ usedVarsExp e1 ⇒
                 nsLookup (merge_env ListProg_env_67 init_env).v (Short x) =
                 NONE) ∧ evalExpNonDet empty_cfg E1 e1 v ∧
            evalExpClocked E1 e1 ck ⇒
            ∃stres r.
                evaluate (st with clock := ck) env1 [expToCML e1] =
                (stres,Rval [r]) ∧ valEqCml v r) ⇒
       isExtendedEnv
         (nsAppend (expToCML_env E1 (Map s e1 els))
            (merge_env ListProg_env_67 init_env).v) env1.v ⇒
       (∀x.
            x ∈ usedVarsExp (Map s e1 els) ⇒
            nsLookup (merge_env ListProg_env_67 init_env).v (Short x) = NONE) ⇒
        (! x v. nsLookup (^state).c x = SOME v ==>
          nsLookup env1.c x = SOME v) ==>
       evalExpNonDet empty_cfg E1 (Mapvl s e1 vl) v ⇒
       evalExpClockedIM E1 (Mapvl s e1 vl) ck1 ⇒
       valEqCml (Lval vl) r ⇒
       ∃stres' r'.
           evaluate (stres with clock := ck1)
             (merge_env ListProg_env_15
                (merge_env ListProg_env_2
                   (merge_env ListProg_env_1
                      (merge_env ListProg_env_0
                         (merge_env ListProg_env
                            (merge_env OptionProg_env_10 init_env))))) with
              v :=
                nsBind "v5" r
                  (nsBind "v4" (Closure env1 s (expToCML e1))
                     (build_rec_env
                        [("map","v4",
                          Fun "v5"
                            (Mat (Var (Short "v5"))
                               [(Pcon (SOME (Short "[]")) [],
                                 Con (SOME (Short "[]")) []);
                                (Pcon (SOME (Short "::"))
                                   [Pvar "v3"; Pvar "v2"],
                                 Let (SOME "v1")
                                   (App Opapp
                                      [Var (Short "v4"); Var (Short "v3")])
                                   (Con (SOME (Short "::"))
                                      [Var (Short "v1");
                                       App Opapp
                                         [App Opapp
                                            [Var (Short "map");
                                             Var (Short "v4")];
                                          Var (Short "v2")]]))]))]
                        (merge_env ListProg_env_15
                           (merge_env ListProg_env_2
                              (merge_env ListProg_env_1
                                 (merge_env ListProg_env_0
                                    (merge_env ListProg_env
                                       (merge_env OptionProg_env_10 init_env))))))
                        (merge_env ListProg_env_15
                           (merge_env ListProg_env_2
                              (merge_env ListProg_env_1
                                 (merge_env ListProg_env_0
                                    (merge_env ListProg_env
                                       (merge_env OptionProg_env_10 init_env)))))).
                        v)))
             [Mat (Var (Short "v5"))
                [(Pcon (SOME (Short "[]")) [],Con (SOME (Short "[]")) []);
                 (Pcon (SOME (Short "::")) [Pvar "v3"; Pvar "v2"],
                  Let (SOME "v1")
                    (App Opapp [Var (Short "v4"); Var (Short "v3")])
                    (Con (SOME (Short "::"))
                       [Var (Short "v1");
                        App Opapp
                          [App Opapp [Var (Short "map"); Var (Short "v4")];
                           Var (Short "v2")]]))]] = (stres',Rval [r']) ∧
           valEqCml v r'`
  (Induct_on `vl` \\ rpt gen_tac
  \\ qmatch_goalsub_abbrev_tac `evaluate _ Mapenv [Mapbody] = (_, _)`
  \\ rw[]
  >- (qunabbrev_tac `Mapbody`
      (* r is actually the empty list *)
      \\ Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq
      \\ fs[Once evaluate_def]
      \\ `! s. evaluate s Mapenv [Var (Short "v5")] =
            (s, Rval [Conv (SOME (TypeStamp "[]" 1)) []])`
          by (rw[] \\  qunabbrev_tac `Mapenv` \\ EVAL_TAC)
      \\ simp[]
      \\ simp[Once evaluate_def]
      \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
      \\ `nsLookup Mapenv.c (Short "[]") = SOME (0, TypeStamp "[]" 1)`
          by (qunabbrev_tac `Mapenv` \\ EVAL_TAC)
      \\ simp[same_type_def, same_ctor_def]
      \\ simp[Once evaluate_def]
      \\ simp[do_con_check_def, build_conv_def]
      \\ fs[evalExpNonDet_cases, Once valEqCml_def])
  \\ qunabbrev_tac `Mapbody`
  \\ Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq
  \\ Cases_on `l` \\ fs[] \\ Cases_on `t` \\ fs[]
  \\ Cases_on `t'` \\ fs[]
  \\ fs[evalExpNonDet_cases] \\ rveq
  \\ simp[Once evaluate_def]
  \\ `! s. evaluate s Mapenv [Var (Short "v5")] =
        (s, Rval [(Conv (SOME (TypeStamp "::" 1)) [h'; h''])])`
      by (rpt strip_tac \\ qunabbrev_tac `Mapenv` \\ EVAL_TAC)
  \\ simp[]
  \\ simp[Once evaluate_def]
  \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
  \\ `nsLookup Mapenv.c (Short "[]") = SOME (0, TypeStamp "[]" 1)`
      by (qunabbrev_tac `Mapenv` \\ EVAL_TAC)
  \\ simp[same_type_def, same_ctor_def]
  \\ simp[Once evaluate_def]
  \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
  \\ `nsLookup Mapenv.c (Short "::") = SOME (2, TypeStamp "::" 1)`
      by (qunabbrev_tac `Mapenv` \\ EVAL_TAC)
  \\ simp[same_type_def, same_ctor_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ fs[evalExpClocked_cases] \\ rveq
  \\ `nsLookup Mapenv.v (Short "v4") = SOME (Closure env1 s (expToCML e1))`
      by (qunabbrev_tac `Mapenv` \\ EVAL_TAC)
  \\ simp[do_opapp_def, dec_clock_def]
  \\ qpat_x_assum `evalExpClocked _ e1 _`
      (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
  \\ first_x_assum
      (qspecl_then
        [`Val v_res`, `stres`, `env1 with v := nsBind s h' env1.v`] destruct)
  >- (fs[usedVarsExp_def, isExtendedEnv_def]
      \\ rpt strip_tac \\ fs[nsLookup_nsAppend_some, nsOptBind_def]
      >- (imp_res_tac nsLookup_bind_optBind
          \\ imp_res_tac valEqCml_is_fromVal \\ rveq
          \\ imp_res_tac nsLookup_nsBind_cases \\ rveq \\ fs[nsLookup_nsBind]
          \\ first_x_assum irule \\ fs[expToCML_env_def, nsLookup_nsAppend_some])
     \\ `Short s <> x`
         by (Cases_on `Short s = x` \\ fs[] \\ rveq
             \\ first_x_assum (qspec_then `s` destruct) \\ fs[])
     \\ fs[nsLookup_nsBind]
     \\ first_x_assum irule \\ fs[]
     \\ DISJ2_TAC \\ conj_tac
     >- (fs[expToCML_env_def, nsLookup_nsAppend_none]
         \\ Cases_on `nsLookup (expToCML_env E1 e1) x` \\ fs[]
         >- (Cases_on `nsLookup (expToCML_env E1 els) x` \\ fs[]
             \\ imp_res_tac expToCML_env_lookup_some \\ rveq
             \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
         \\ imp_res_tac expToCML_env_lookup_some \\ rveq
         \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
     \\ MATCH_ACCEPT_TAC (CONJUNCT1 lookupMod_toCml_none))
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `ck2 + 2` destruct) \\ fs[]
  \\ simp[Once evaluate_def, do_con_check_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ `nsLookup
                (nsOptBind (SOME "v1") r
                   (nsBind "v2" h'' (nsBind "v3" h' Mapenv.v)))
                (Short "v2") = SOME h''` by (EVAL_TAC)
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ simp[Once evaluate_def]
  \\ `nsLookup
                    (nsOptBind (SOME "v1") r
                       (nsBind "v2" h'' (nsBind "v3" h' Mapenv.v)))
                    (Short "v4") = SOME (Closure env1 s (expToCML e1))`
      by (qunabbrev_tac `Mapenv` \\ EVAL_TAC)
  \\ simp[]
  \\ `! s.
        evaluate s (Mapenv with v := nsOptBind (SOME "v1") r (nsBind "v2" h'' (nsBind "v3" h' Mapenv.v)))
          [Var (Short "map")] = (s, Rval [map_1_v])`
      by (rpt strip_tac \\ simp[evaluate_def]
          \\ qunabbrev_tac `Mapenv` \\ fs[map_1_v_def]
          \\ EVAL_TAC)
  \\ simp[do_opapp_def, map_1_v_def, Once find_recfun_def]
  \\ simp[Once evaluate_def, dec_clock_def]
  \\ qpat_x_assum `evalExpClockedIM _ _ _`  resolve_tac
  \\ first_x_assum (qspecl_then [`Lval vrl`, `h''`, `env1`, `stres'`, `els`] destruct)
  >- (fs[usedVarsExp_def] \\ rpt conj_tac \\ TRY (first_x_assum MATCH_ACCEPT_TAC)
      \\ qpat_x_assum `valEqCml (Lval _) _`
          (fn thm => fs[ONCE_REWRITE_RULE [Once valEqCml_def] thm]))
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `stres'.clock` destruct) \\ fs[]
  \\ simp[evaluate_def]
  \\ `nsLookup
            (nsOptBind (SOME "v1") r
               (nsBind "v2" h'' (nsBind "v3" h' Mapenv.v))) (Short "v1") = SOME r`
      by (EVAL_TAC)
  \\ simp[build_conv_def]
  \\ simp[Once valEqCml_def]);

Theorem fold_forward_thm
  `∀vl ck1 v i (stres2:'a semanticPrimitives$state) r env1 E1 r' vi y x els f.
    (∀E1 v (st:'a semanticPrimitives$state) env1 ck.
         isExtendedEnv
           (nsAppend (expToCML_env E1 f)
              (merge_env ListProg_env_67 init_env).v) env1.v ∧
        (! x v. nsLookup (^state).c x = SOME v ==>
          nsLookup env1.c x = SOME v) /\
         (∀x.
              x ∈ usedVarsExp f ⇒
              nsLookup (merge_env ListProg_env_67 init_env).v (Short x) =
              NONE) ∧ evalExpNonDet empty_cfg E1 f v ∧
         evalExpClocked E1 f ck ⇒
         ∃stres r.
             evaluate (st with clock := ck) env1 [expToCML f] =
             (stres,Rval [r]) ∧ valEqCml v r) ⇒
    isExtendedEnv
      (nsAppend (expToCML_env E1 (Fold (x,y) f i els))
         (merge_env ListProg_env_67 init_env).v) env1.v ⇒
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) ==>
    (∀x'.
         x' ∈ usedVarsExp (Fold (x,y) f i els) ⇒
         nsLookup (merge_env ListProg_env_67 init_env).v (Short x') = NONE) ⇒
    evalExpNonDet empty_cfg E1 (Foldvl (x,y) f i vl) v ⇒
    evalExpClockedIM E1 (Foldvl (x,y) f i vl) ck1 ⇒
    valEqCml (Lval vl) r ⇒
    evalExpNonDet empty_cfg E1 i vi ⇒
    valEqCml vi r' ⇒
    ∃stres'' r''.
        evaluate (stres2 with clock := ck1)
          (merge_env ListProg_env_27
             (merge_env ListProg_env_26
                (merge_env ListProg_env_25
                   (merge_env ListProg_env_23
                      (merge_env ListProg_env_22
                         (merge_env ListProg_env_17
                            (merge_env ListProg_env_16
                               (merge_env ListProg_env_2
                                  (merge_env ListProg_env_1
                                     (merge_env ListProg_env_0
                                        (merge_env ListProg_env
                                           (merge_env OptionProg_env_10
                                              init_env))))))))))) with
           v :=
             nsBind "v5" r
               (nsBind "v3" r'
                  (nsBind "v4" (Closure env1 y (Fun x (expToCML f)))
                     (build_rec_env
                        [("foldr","v4",
                          Fun "v3"
                            (Fun "v5"
                               (Mat (Var (Short "v5"))
                                  [(Pcon (SOME (Short "[]")) [],
                                    Var (Short "v3"));
                                   (Pcon (SOME (Short "::"))
                                      [Pvar "v2"; Pvar "v1"],
                                    App Opapp
                                      [App Opapp
                                         [Var (Short "v4");
                                          Var (Short "v2")];
                                       App Opapp
                                         [App Opapp
                                            [App Opapp
                                               [Var (Short "foldr");
                                                Var (Short "v4")];
                                             Var (Short "v3")];
                                          Var (Short "v1")]])])))]
                        (merge_env ListProg_env_27
                           (merge_env ListProg_env_26
                              (merge_env ListProg_env_25
                                 (merge_env ListProg_env_23
                                    (merge_env ListProg_env_22
                                       (merge_env ListProg_env_17
                                          (merge_env ListProg_env_16
                                             (merge_env ListProg_env_2
                                                (merge_env ListProg_env_1
                                                   (merge_env
                                                      ListProg_env_0
                                                      (merge_env
                                                         ListProg_env
                                                         (merge_env
                                                            OptionProg_env_10
                                                            init_env))))))))))))
                        (merge_env ListProg_env_27
                           (merge_env ListProg_env_26
                              (merge_env ListProg_env_25
                                 (merge_env ListProg_env_23
                                    (merge_env ListProg_env_22
                                       (merge_env ListProg_env_17
                                          (merge_env ListProg_env_16
                                             (merge_env ListProg_env_2
                                                (merge_env ListProg_env_1
                                                   (merge_env
                                                      ListProg_env_0
                                                      (merge_env
                                                         ListProg_env
                                                         (merge_env
                                                            OptionProg_env_10
                                                            init_env)))))))))))).
                        v))))
          [Mat (Var (Short "v5"))
             [(Pcon (SOME (Short "[]")) [],Var (Short "v3"));
              (Pcon (SOME (Short "::")) [Pvar "v2"; Pvar "v1"],
               App Opapp
                 [App Opapp [Var (Short "v4"); Var (Short "v2")];
                  App Opapp
                    [App Opapp
                       [App Opapp [Var (Short "foldr"); Var (Short "v4")];
                        Var (Short "v3")]; Var (Short "v1")]])]] =
        (stres'',Rval [r'']) ∧ valEqCml v r''`
  (Induct_on `vl` \\ rpt gen_tac
  \\ qmatch_goalsub_abbrev_tac `evaluate _ Foldenv [Foldbody] = (_, _)`
  \\ rw[]
  >- (qunabbrev_tac `Foldbody`
      (* r is actually the empty list *)
      \\ Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq
      \\ fs[Once evaluate_def]
      \\ `! s. evaluate s Foldenv [Var (Short "v5")] =
            (s, Rval [Conv (SOME (TypeStamp "[]" 1)) []])`
          by (rw[] \\  qunabbrev_tac `Foldenv` \\ EVAL_TAC)
      \\ simp[]
      \\ simp[Once evaluate_def]
      \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
      \\ `nsLookup Foldenv.c (Short "[]") = SOME (0, TypeStamp "[]" 1)`
          by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
      \\ simp[same_type_def, same_ctor_def]
      \\ simp[Once evaluate_def]
      \\ `nsLookup Foldenv.v (Short "v3") = SOME r'`
          by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
      \\ simp[]
      \\ fs[evalExpNonDet_cases]
      \\ `v = vi`
          by (irule (CONJUNCT1 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
              \\ fs[empty_cfg_def])
      \\ rveq \\ fs[])
  \\ fs[evalExpNonDet_cases, evalExpClocked_cases] \\ rveq \\ fs[]
  \\ qunabbrev_tac `Foldbody`
  (* Case analysis on result of list expression *)
  \\ Cases_on `r` \\ fs[Once valEqCml_def] \\ rveq
  \\ Cases_on `l` \\ fs[] \\ Cases_on `t` \\ fs[]
  \\ Cases_on `t'` \\ fs[]
  \\ simp[Once evaluate_def]
  \\ `! s. evaluate s Foldenv [Var (Short "v5")] =
        (s, Rval [(Conv (SOME (TypeStamp "::" 1)) [h'; h''])])`
      by (rpt strip_tac \\ qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ simp[]
  \\ simp[Once evaluate_def]
  (* Evaluate match with [] *)
  \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
  \\ `nsLookup Foldenv.c (Short "[]") = SOME (0, TypeStamp "[]" 1)`
      by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ simp[same_type_def, same_ctor_def]
  \\ simp[Once evaluate_def]
  (* evaluate match with :: *)
  \\ simp[ALL_DISTINCT, pat_bindings_def, pmatch_def]
  \\ `nsLookup Foldenv.c (Short "::") = SOME (2, TypeStamp "::" 1)`
      by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ simp[same_type_def, same_ctor_def]
  \\ ntac 8 (simp[Once evaluate_def])
  \\ `nsLookup Foldenv.v (Short "v3") = SOME r'`
      by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ ntac 3 (simp[Once evaluate_def])
  \\ `nsLookup Foldenv.v (Short "v4") = SOME (Closure env1 y (Fun x (expToCML f)))`
      by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ simp[Once evaluate_def]
  \\ fs[build_rec_env_def]
  \\ `nsLookup Foldenv.v (Short "foldr") = SOME (
    (Recclosure
                        (merge_env ListProg_env_27
                           (merge_env ListProg_env_26
                              (merge_env ListProg_env_25
                                 (merge_env ListProg_env_23
                                    (merge_env ListProg_env_22
                                       (merge_env ListProg_env_17
                                          (merge_env ListProg_env_16
                                             (merge_env ListProg_env_2
                                                (merge_env ListProg_env_1
                                                   (merge_env
                                                      ListProg_env_0
                                                      (merge_env
                                                         ListProg_env
                                                         (merge_env
                                                            OptionProg_env_10
                                                            init_env))))))))))))
                        [("foldr","v4",
                          Fun "v3"
                            (Fun "v5"
                               (Mat (Var (Short "v5"))
                                  [(Pcon (SOME (Short "[]")) [],
                                    Var (Short "v3"));
                                   (Pcon (SOME (Short "::"))
                                      [Pvar "v2"; Pvar "v1"],
                                    App Opapp
                                      [App Opapp
                                         [Var (Short "v4");
                                          Var (Short "v2")];
                                       App Opapp
                                         [App Opapp
                                            [App Opapp
                                               [Var (Short "foldr");
                                                Var (Short "v4")];
                                             Var (Short "v3")];
                                          Var (Short "v1")]])])))]
                        "foldr"))`
    by (qunabbrev_tac `Foldenv` \\ EVAL_TAC)
  \\ simp[do_opapp_def, dec_clock_def, Once find_recfun_def]
  (* evaluate away lambda abstractions *)
  \\ ntac 2 (simp[Once evaluate_def])
  (* now use IH *)
  \\ first_x_assum drule
  \\ rpt (disch_then drule)
  \\ disch_then
      (qspecl_then [`stres2`, `h''`, `r'`, `vi`] destruct)
  >- (fs[]
      \\ qpat_x_assum `valEqCml (Lval _) _`
          (fn thm => fs[ONCE_REWRITE_RULE[Once valEqCml_def] thm]))
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `ck1' + ck2 + 2` destruct)
  \\ fs[build_rec_env_def]
  (* last call to argument function f *)
  \\ ntac 4 (simp[Once evaluate_def])
  \\ simp[do_opapp_def, dec_clock_def]
  \\ simp[evaluate_def]
  \\ rpt (qpat_x_assum `evaluate _ _ _ = (_, Rval [r'3'])` kall_tac)
  \\ rpt (qpat_x_assum `nsLookup  _ _ = SOME _` kall_tac)
  \\ `v_rec = v_rec'`
      by (irule (CONJUNCT1 evalNonDet_deterministic)
         \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
         \\ fs[empty_cfg_def])
  \\ rveq
  \\ qpat_x_assum `evalExpClocked (bind _ _ _) f ck2` resolve_tac
  \\ qpat_x_assum `evalExpNonDet _ _ f _` resolve_tac
  \\ first_x_assum
      (qspecl_then [`stres''`,
        `env1 with v := nsBind x r'3' (nsBind y h' env1.v)`] destruct)
  >- (fs[usedVarsExp_def, isExtendedEnv_def]
      \\ rpt strip_tac \\ fs[nsLookup_nsAppend_some, nsOptBind_def]
      (* Cleanup... *)
      \\ last_x_assum kall_tac
      >- (imp_res_tac nsLookup_bind_optBind
          \\ imp_res_tac valEqCml_is_fromVal \\ rveq
          \\ imp_res_tac nsLookup_nsBind_cases \\ rveq \\ fs[nsLookup_nsBind]
          \\ imp_res_tac nsLookup_bind_optBind
          \\ imp_res_tac valEqCml_is_fromVal \\ rveq
          \\ imp_res_tac nsLookup_nsBind_cases \\ rveq \\ fs[nsLookup_nsBind]
          \\ first_x_assum irule \\ fs[expToCML_env_def, nsLookup_nsAppend_some])
     \\ `Short x <> x'`
         by (Cases_on `Short x = x'` \\ fs[] \\ rveq
             \\ first_x_assum (qspec_then `x` destruct) \\ fs[])
     \\ `Short y <> x'`
         by (Cases_on `Short y = x'` \\ fs[] \\ rveq
             \\ first_x_assum (qspec_then `y` destruct) \\ fs[])
     \\ fs[nsLookup_nsBind]
     \\ first_x_assum irule \\ fs[]
     \\ DISJ2_TAC \\ conj_tac
     >- (fs[expToCML_env_def, nsLookup_nsAppend_none]
         \\ Cases_on `nsLookup (expToCML_env E1 f) x'` \\ fs[]
         >- (Cases_on `nsLookup (expToCML_env E1 i) x'` \\ fs[]
            >- (Cases_on `nsLookup (expToCML_env E1 els) x'` \\ fs[]
                \\ imp_res_tac expToCML_env_lookup_some \\ rveq
                \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
            \\ imp_res_tac expToCML_env_lookup_some \\ rveq
            \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
         \\ imp_res_tac expToCML_env_lookup_some \\ rveq
         \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
     \\ MATCH_ACCEPT_TAC (CONJUNCT1 lookupMod_toCml_none))
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `stres''.clock + ck1'` destruct) \\ fs[]);

Theorem valTree2CML_forward
  `! v st env.
    ? r. evaluate st env [vTree2CML v] = (st, Rval [r]) /\ valEqCml (Val v) r`
  (Induct_on `v`
  \\ rpt strip_tac
  \\ simp[evaluate_def, vTree2CML_def, Once valEqCml_def, tree2IEEE_def]
  \\ rpt (first_x_assum (qspecl_then [`st`, `env`] assume_tac) \\ fs[])
  \\ fs[do_app_def]
  >- (Cases_on `r` \\ fs[valEqCml_def]
      \\ Cases_on `l` \\ fs[] \\ rveq \\ fs[state_component_equality]
      \\ Cases_on `u` \\ fs[toUop_def, fp_uop_def, tree2IEEE_def])
  >- (Cases_on `r` \\ Cases_on `r'` \\ fs[valEqCml_def]
      \\ Cases_on `l` \\ Cases_on `l'` \\ fs[] \\ rveq
      \\ fs[state_component_equality]
      \\ Cases_on `b` \\ fs[toBop_def, fp_bop_def, tree2IEEE_def])
  >- (Cases_on `r` \\ Cases_on `r'` \\ Cases_on `r''` \\ fs[valEqCml_def]
      \\ Cases_on `l` \\ Cases_on `l'` \\ Cases_on `l''` \\ fs[]
      \\ rveq \\ fs[state_component_equality]
      \\ Cases_on `t` \\ fs[toTop_def, fp_top_def, tree2IEEE_def])
  \\ Cases_on `r` \\ fs[valEqCml_def]);

Theorem valTrees2CML_forward
  `! vl st env.
    nsLookup env.c (Short "[]") = SOME (0, TypeStamp "[]" 1) /\
    nsLookup env.c (Short "::") = SOME (2, TypeStamp "::" 1) ==>
    ? r. evaluate st env [valTrees2CML vl] = (st, Rval [r]) /\
      valEqCml (Lval vl) r`
  (Induct_on `vl`
  \\ rpt strip_tac
  \\ simp[evaluate_def, valTrees2CML_def, nil_exp_def, cons_exp_def,
          do_con_check_def, build_conv_def, Once valEqCml_def]
  \\ first_x_assum (qspecl_then [`st`, `env`] destruct)
  \\ fs[]
  \\ qspecl_then [`h`, `st`, `env`] assume_tac valTree2CML_forward
  \\ fs[]);

(** FORWARD SIMULATION THEOREM **)
Theorem expToCML_forward
  `(!e E1 v (st:'a semanticPrimitives$state) (env1:v sem_env) ck.
    (* the environment contains at least the free variable values and the stdlib *)
    isExtendedEnv (nsAppend (expToCML_env E1 e) (^state).v) env1.v /\
    (* the environment contains the constructors *)
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) /\
    (* The Icing program does not overwrite stdlib names *)
    (! x. x IN (usedVarsExp e) ==> nsLookup (^state).v (Short x) = NONE) /\
    (* The program evaluates in Icing without optimizations *)
    evalExpNonDet (empty_cfg) E1 e v /\
    evalExpClocked E1 e ck ==>
    ? stres r.
      evaluate (st with clock := ck) env1 [expToCML e] = (stres, Rval [r]) /\
      valEqCml v r)
  /\
  (! ce E1 v (st:'a semanticPrimitives$state) (env1:v sem_env) ck.
    (* the environment contains at least the free variable values and the stdlib *)
    isExtendedEnv (nsAppend (cexpToCML_env E1 ce) (^state).v) env1.v /\
    (* the environment contains the constructors *)
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) /\
    (* The Icing program does not overwrite stdlib names *)
    (! x. x IN (usedVarsCexp ce) ==> nsLookup (^state).v (Short x) = NONE) /\
    (* The program evaluates in Icing without optimizations *)
    evalCexpNonDet empty_cfg E1 ce v /\
    evalCexpClocked E1 ce ck ==>
    (? stres b.
      evaluate (st with clock := ck) env1 [cexpToCML ce] = (stres, Rval [Boolv b]) /\
      valEqCml (Cval v) (Boolv b)))
  /\
  (! (l:expr list) E1 v (st:'a semanticPrimitives$state) (env1:v sem_env) ck.
    (* the environment contains at least the free variable values and the stdlib *)
    isExtendedEnv (nsAppend (expToCML_env E1 (List l)) (^state).v) env1.v /\
    (* the environment contains the constructors *)
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) /\
    (* The Icing program does not overwrite stdlib names *)
    (! x. x IN (usedVarsExp (List l)) ==> nsLookup (^state).v (Short x) = NONE) /\
    (* The program evaluates in Icing without optimizations *)
    evalExpNonDet empty_cfg E1 (List l) v /\
    evalExpClocked E1 (List l) ck ==>
    ? stres r.
      evaluate (st with clock := ck) env1 [expToCML (List l)] = (stres, Rval [r]) /\
      valEqCml v r)`
  (ho_match_mp_tac expr_induction \\ rpt strip_tac
  \\ fs[expToCML_def, evalExpNonDet_cases, evaluate_def, rewritesTo_def]
  \\ rpt strip_tac \\ rveq
  >- (fs[valEqCml_def, tree2IEEE_def, toIcingVal_def])
  >- (fs[expToCML_env_def, nsLookup_def, nsBind_def, mk_id_def, isExtendedEnv_def]
      \\ last_x_assum (qspec_then `Short s` assume_tac)
      \\ fs[nsLookup_def, nsAppend_def, nsBind_def]
      \\ irule valEqCml_fromIcingVal)
  >- (qpat_x_assum `evalExpNonDet _ E1 e _` resolve_tac
      \\ fs[Once evalExpClocked_cases]
      \\ rveq \\ rename1 `evalExpClocked E1 e ck1`
      \\ first_x_assum (qspecl_then [`st`, `env1`, `ck1`] destruct)
      \\ fs[ expToCML_env_def, usedVarsExp_def]
      \\ first_x_assum (mp_then Any assume_tac evaluatePropsTheory.evaluate_add_to_clock)
      \\ first_x_assum (qspec_then `3 * n + 3` destruct) \\ fs[]
      \\ `evaluate (stres with clock := 3 * n + (stres.clock + 3)) env1 [ith_name] =
            (stres with clock := 3 * n + (stres.clock + 3), Rval [nth_v])`
          by (fs[ith_name_def, evaluate_def]
              \\ `nsLookup (nsAppend (expToCML_env E1 e) ^state.v)
                    (Long "List" (Short "nth")) = SOME nth_v`
                  by (once_rewrite_tac [nsLookup_nsAppend_some]
                      \\ DISJ2_TAC
                      \\ rpt conj_tac
                      >- (irule (CONJUNCT1 expToCML_env_no_list))
                      >- (EVAL_TAC)
                      \\ rpt strip_tac \\ fs[id_to_mods_def]
                      \\ Cases_on `p1` \\ fs[] \\ rveq
                      \\ fs[expToCML_env_no_listmod])
              \\ fs[isExtendedEnv_def] \\ res_tac \\ fs[])
      \\ qpat_x_assum `evaluate _ _ [ith_name] = _`
          (fn thm => once_rewrite_tac [thm])
      \\ simp[do_opapp_def, nth_v_def, Once find_recfun_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ qpat_x_assum `evaluate _ _ _ = _` kall_tac
      \\ qpat_x_assum `evalExpNonDet empty_cfg _ _ _` kall_tac
      \\ qpat_x_assum `evalExpClocked E1 e _` kall_tac
      \\ drule list_nth_forward_thm
      \\ rpt (disch_then drule)
      \\ disch_then (qspec_then `stres` assume_tac) \\ fs[])
  (* Unary operator *)
  >- (rename1 `evalExpNonDet _ E1 e (Val v1)`
      \\ first_x_assum (qspecl_then [`E1`, `Val v1`, `st`, `env1`, `ck`] destruct)
      \\ fs[ expToCML_env_def, usedVarsExp_def, do_app_def,
            state_component_equality, evalExpClocked_cases]
      \\ Cases_on `r'` \\ fs [Once valEqCml_def]
      \\ Cases_on `l` \\ fs[empty_cfg_def]
      \\ imp_res_tac rwAllValTree_empty_rewrites \\ rveq
      \\ rveq \\ Cases_on `u` \\ fs[fp_uop_def, toUop_def, tree2IEEE_def])
  (* Binary operators *)
  >- (rename1 `evalExpNonDet _ E1 e1 (Val v1)`
      \\ rename1 `evalExpNonDet _ E1 e2 (Val v2)`
      \\ fs[evalExpClocked_cases] \\ rveq
      \\ first_x_assum (qspecl_then [`E1`, `Val v2`, `st`, `env1`, `ck2`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_bin2 \\ fs[usedVarsExp_def]
          \\ first_x_assum MATCH_ACCEPT_TAC)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1` destruct) \\ fs[]
      \\ first_x_assum (qspecl_then [`E1`, `Val v1`, `stres`, `env1`, `ck1`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_bin1 \\ fs[usedVarsExp_def]
          \\ first_x_assum MATCH_ACCEPT_TAC)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[]
      \\ fs[do_app_def]
      \\ Cases_on `r'` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[]
      \\ Cases_on `r''` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[empty_cfg_def] \\ rveq
      \\ Cases_on `b` \\ fs[tree2IEEE_def, fp_bop_def, toBop_def])
  (* Ternary operators *)
  >- (rename1 `evalExpNonDet _ E1 e1 (Val v1)`
      \\ rename1 `evalExpNonDet _ E1 e2 (Val v2)`
      \\ rename1 `evalExpNonDet _ E1 e3 (Val v3)`
      \\ fs[evalExpClocked_cases] \\ rveq
      \\ first_x_assum (qspecl_then [`E1`, `Val v3`, `st`, `env1`, `ck3`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_ter3 \\ fs[usedVarsExp_def])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + ck2` destruct) \\ fs[]
      \\ first_x_assum (qspecl_then [`E1`, `Val v2`, `stres`, `env1`, `ck2`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_ter2 \\ fs[usedVarsExp_def])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + stres.clock` destruct) \\ fs[]
      \\ rename1 `stres2 with clock := ck1 + (stres.clock + stres2.clock)`
      \\ first_x_assum (qspecl_then [`E1`, `Val v1`, `stres2`, `env1`, `ck1`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_ter1 \\ fs[usedVarsExp_def])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock + stres2.clock` destruct) \\ fs[]
      \\ fs[do_app_def]
      \\ Cases_on `r'` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[]
      \\ Cases_on `r''` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[empty_cfg_def] \\ rveq
      \\ Cases_on `r'3'` \\ fs[]
      \\ Cases_on `l` \\ fs[]
      \\ Cases_on `t` \\ fs[tree2IEEE_def, fp_top_def, toTop_def])
  (* Let-binding *)
  >- (rename1 `Let x e1 e2`
      \\ fs[evalExpClocked_cases] \\ rveq
      \\ last_x_assum (qspecl_then [`E1`, `v1`, `st`, `env1`, `ck1`] destruct)
      >- (fs[ expToCML_env_def, usedVarsExp_def, do_app_def,
            state_component_equality, evalExpClocked_cases]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_bin1
          \\ rw[] \\ res_tac)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck2` destruct) \\ fs[]
      \\ `v1 = v1'`
          by (irule (CONJUNCT1 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`]
              \\ find_exists_tac
              \\ simp[empty_cfg_def])
      \\ rveq
      \\ first_x_assum
          (qspecl_then [`bind E1 x v1`, `v`, `stres`,
            `env1 with v := nsOptBind (SOME x) (fromIcingVal v1) env1.v`, `ck2`]
          destruct)
      >- (fs[usedVarsExp_def, isExtendedEnv_def]
          \\ rpt strip_tac \\ fs[nsLookup_nsAppend_some, nsOptBind_def]
          >- (imp_res_tac nsLookup_bind_optBind
              \\ imp_res_tac nsLookup_nsBind_cases \\ rveq \\ fs[nsLookup_nsBind]
              \\ first_x_assum irule \\ fs[expToCML_env_def, nsLookup_nsAppend_some]
              \\ DISJ1_TAC
              \\ imp_res_tac expToCML_env_lookup_eq
              \\ last_x_assum (qspec_then `e1` assume_tac) \\ fs[]
              \\ MATCH_ACCEPT_TAC (CONJUNCT1 lookupMod_toCml_none))
          \\ `Short x <> x'`
              by (Cases_on `Short x = x'` \\ fs[] \\ rveq
                  \\ first_x_assum (qspec_then `x` destruct) \\ fs[])
          \\ fs[nsLookup_nsBind]
          \\ first_x_assum irule \\ fs[]
          \\ DISJ2_TAC \\ conj_tac
          >- (fs[expToCML_env_def, nsLookup_nsAppend_none]
              \\ Cases_on `nsLookup (expToCML_env E1 e1) x'` \\ fs[]
              >- (Cases_on `nsLookup (expToCML_env E1 e2) x'` \\ fs[]
                  \\ imp_res_tac expToCML_env_lookup_some \\ rveq
                  \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
              \\ imp_res_tac expToCML_env_lookup_some \\ rveq
              \\ first_x_assum (qspec_then `n` destruct) \\ fs[])
          \\ MATCH_ACCEPT_TAC (CONJUNCT1 lookupMod_toCml_none))
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[]
      \\ imp_res_tac valEqCml_is_fromVal \\ rveq
      \\ asm_exists_tac \\ fs[])
  (* Conditionals *)
  >- (rename1 `evalExpNonDet empty_cfg E1 e v1`
      \\ fs[evalExpClocked_cases] \\ rveq
      >- (`cv = cv'`
          by (irule (CONJUNCT2 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
              \\ fs[empty_cfg_def])
          \\ rveq \\ fs[]
          \\ qpat_x_assum `evalCexpClocked E1 ce ck3` resolve_tac
          \\ first_x_assum (qspecl_then [`cv`, `st`, `env1`] destruct)
          >- (fs[usedVarsExp_def]
              \\ irule isExtendedEnv_monotone \\ find_exists_tac
              \\ fs[expToCML_env_def]
              \\ irule isExtendedEnv_append_cond1 \\ fs[])
          \\ drule evaluatePropsTheory.evaluate_add_to_clock
          \\ disch_then (qspec_then `ck1` destruct) \\ fs[]
          \\ fs[Once valEqCml_def, do_if_def]
          \\ qpat_x_assum `evalExpClocked E1 e ck1` resolve_tac
          \\ first_x_assum (qspecl_then [`v1`, `stres`, `env1`] destruct)
          >- (fs[usedVarsExp_def]
              \\ irule isExtendedEnv_monotone \\ find_exists_tac
              \\ fs[expToCML_env_def]
              \\ irule isExtendedEnv_append_cond2 \\ fs[])
          \\ drule evaluatePropsTheory.evaluate_add_to_clock
          \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[])
      \\ `cv = cv'`
          by (irule (CONJUNCT2 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
              \\ fs[empty_cfg_def])
      \\ fs[])
  (* Conditionals *)
  >- (rename1 `evalExpNonDet empty_cfg E1 e2 v2`
      \\ fs[evalExpClocked_cases] \\ rveq
      >- (`cv = cv'`
          by (irule (CONJUNCT2 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
              \\ fs[empty_cfg_def])
          \\ fs[])
      \\ `cv = cv'`
          by (irule (CONJUNCT2 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`] \\ find_exists_tac
              \\ fs[empty_cfg_def])
      \\ rveq \\ fs[]
      \\ qpat_x_assum `evalCexpClocked E1 ce ck3` resolve_tac
      \\ first_x_assum (qspecl_then [`cv`, `st`, `env1`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_cond1 \\ fs[])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck2` destruct) \\ fs[]
      \\ fs[Once valEqCml_def, do_if_def]
      \\ qpat_x_assum `evalExpClocked E1 e2 ck2` resolve_tac
      \\ first_x_assum (qspecl_then [`v2`, `stres`, `env1`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_cond3 \\ fs[])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[])
  (* Scopes *)
  >- (rename1 `evalExpNonDet _ E1 e v`
      \\ fs[evalExpClocked_cases]
      \\ qpat_x_assum `evalExpClocked _ _ _` resolve_tac
      \\ first_x_assum (qspecl_then [`v`, `st`, `env1`] destruct)
      >- (fs[usedVarsExp_def, expToCML_env_def, empty_cfg_def]
          \\ irule (CONJUNCT1 evalNonDet_cfg_det) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ find_exists_tac \\ fs[]
      \\ irule valEqCml_wrapScope \\ fs[])
  (* Map case *)
  >- (fs[evalExpClocked_cases] \\ rveq
      \\ `Lval vl = Lval vl'`
          by (irule (CONJUNCT1 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`]
              \\ find_exists_tac \\ fs[empty_cfg_def])
      \\ fs[] \\ rveq
      \\ qpat_x_assum `evalExpNonDet empty_cfg E1 _ (Lval _)` resolve_tac
      \\ first_x_assum (qspecl_then [`st`, `env1`, `ck2`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_bin2
          \\ rw[] \\ res_tac)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + 2` destruct)
      \\ fs[]
      \\ `! s. evaluate s env1 [map_name] = (s, Rval [map_1_v])`
          by (fs[map_name_def, evaluate_def]
              \\ `nsLookup
                    (nsAppend (expToCML_env E1 (Map s e e'))
                      (merge_env ListProg_env_67 init_env).v)
                    (Long "List" (Short "map")) = SOME map_1_v`
                  by (once_rewrite_tac [nsLookup_nsAppend_some]
                      \\ DISJ2_TAC
                      \\ rpt conj_tac
                      >- (irule (CONJUNCT1 expToCML_env_no_list))
                      >- (EVAL_TAC)
                      \\ rpt strip_tac \\ fs[id_to_mods_def]
                      \\ Cases_on `p1` \\ fs[] \\ rveq
                      \\ fs[expToCML_env_no_listmod])
              \\ fs[isExtendedEnv_def] \\ res_tac \\ fs[])
      \\ asm_rewrite_tac []
      \\ simp_tac std_ss [pair_case_thm, result_case_def]
      \\ simp[do_opapp_def, map_1_v_def, Once find_recfun_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ drule map_forward_thm
      \\ rpt (disch_then drule)
      \\ disch_then (qspec_then `stres` assume_tac) \\ fs[]
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[])
  >- (`?r. evaluate (st with clock := ck) env1 [valTrees2CML l] =
        (st with clock := ck, Rval [r]) /\
        valEqCml (Lval l) r`
        by (qspecl_then [`l`, `st with clock := ck`, `env1`]
              destruct valTrees2CML_forward
            >- (conj_tac \\ first_x_assum irule \\ EVAL_TAC)
            \\ fs[])
      \\ fs[]
      \\ `! s. evaluate s env1 [map_name] = (s, Rval [map_1_v])`
          by (fs[map_name_def, evaluate_def]
              \\ `nsLookup
                    (nsAppend (expToCML_env E1 (Mapvl s e l))
                      (merge_env ListProg_env_67 init_env).v)
                    (Long "List" (Short "map")) = SOME map_1_v`
                  by (once_rewrite_tac [nsLookup_nsAppend_some]
                      \\ DISJ2_TAC
                      \\ rpt conj_tac
                      >- (irule (CONJUNCT1 expToCML_env_no_list))
                      >- (EVAL_TAC)
                      \\ rpt strip_tac \\ fs[id_to_mods_def]
                      \\ Cases_on `p1` \\ fs[] \\ rveq
                      \\ fs[expToCML_env_no_listmod])
              \\ fs[isExtendedEnv_def] \\ res_tac \\ fs[])
      \\ simp[]
      \\ fs[evalExpClocked_cases] \\ rveq
      \\ simp[do_opapp_def, map_1_v_def, Once find_recfun_def]
      \\ simp[dec_clock_def]
      \\ simp[Once evaluate_def]
      \\ drule map_forward_thm
      \\ rpt (disch_then drule)
      \\ disch_then
          (qspecl_then [`l`, `ck1`, `v`, `r`, `env1`, `st`, `E1`, `s`, `List []`] destruct)
      >- (fs[usedVarsExp_def, expToCML_env_def]
          \\ first_x_assum MATCH_ACCEPT_TAC)
      \\ fs[])
  (* Fold case *)
  >- (Cases_on `p` \\ rename1 `Fold (x,y) f i els`
      \\ fs[Once evalExpClocked_cases, evalExpNonDet_cases, expToCML_def] \\ rveq
      \\ `Lval vl = Lval vl'`
          by (irule (CONJUNCT1 evalNonDet_deterministic)
              \\ qexistsl_tac [`E1`, `empty_cfg`]
              \\ find_exists_tac \\ fs[empty_cfg_def])
      \\ fs[] \\ rveq
      \\ simp[evaluate_def]
      \\ qpat_assum `evalExpNonDet empty_cfg E1 _ (Lval _)` resolve_tac
      \\ qpat_x_assum `evalExpClocked E1 els _` resolve_tac
      \\ first_x_assum (qspecl_then [`st`, `env1`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_ter3 \\ rw[] \\ res_tac)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + ck2 + 3` destruct)
      \\ fs[]
      (* now show that the accumulator evaluates to a value *)
      \\ `?vi. evalExpNonDet empty_cfg E1 i vi`
          by (qpat_x_assum `evalExpNonDet _ _ (Foldvl _ _ _ _) _` mp_tac
              \\ rpt (pop_assum kall_tac)
              \\ qid_spec_tac `E1` \\ qid_spec_tac `x` \\ qid_spec_tac `y`
              \\ qid_spec_tac  `f` \\ qid_spec_tac `i` \\ qid_spec_tac `v`
              \\ qid_spec_tac `vl`
              \\ Induct_on `vl` \\ rw[evalExpNonDet_cases] \\ res_tac
              \\ find_exists_tac \\ fs[])
      \\ fs[]
      \\ qpat_assum `evalExpNonDet _ E1 i _` resolve_tac
      \\ qpat_x_assum `evalExpClocked E1 i _` resolve_tac
      \\ first_x_assum (qspecl_then [`stres`, `env1`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_ter2 \\ rw[] \\ res_tac)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + stres.clock + 3` destruct) \\ fs[]
      \\ `! s. evaluate s env1 [fold_name] = (s, Rval [foldr_v])`
          by (fs[fold_name_def, evaluate_def]
              \\ `nsLookup
                    (nsAppend (expToCML_env E1 (Fold (x,y) f i els))
                      (merge_env ListProg_env_67 init_env).v)
                    (Long "List" (Short "foldr")) = SOME foldr_v`
                  by (once_rewrite_tac [nsLookup_nsAppend_some]
                      \\ DISJ2_TAC
                      \\ rpt conj_tac
                      >- (irule (CONJUNCT1 expToCML_env_no_list))
                      >- (EVAL_TAC)
                      \\ rpt strip_tac \\ fs[id_to_mods_def]
                      \\ Cases_on `p1` \\ fs[] \\ rveq
                      \\ fs[expToCML_env_no_listmod])
              \\ fs[isExtendedEnv_def] \\ res_tac \\ fs[])
      \\ asm_rewrite_tac []
      \\ simp_tac std_ss [pair_case_thm, result_case_def]
      \\ simp[do_opapp_def, foldr_v_def, Once find_recfun_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ drule fold_forward_thm
      \\ rpt (disch_then drule)
      \\ disch_then (qspecl_then [`stres'`] assume_tac) \\ fs[]
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock + stres'.clock` destruct)
      \\ fs[])
  (* SIMILAR to before *)
  >- (Cases_on `p` \\ fs[expToCML_def, evalExpNonDet_cases, evalExpClocked_cases]
      \\ rveq
      \\ ntac 7 (simp[Once evaluate_def])
      \\ `? rl. evaluate (st with clock := ck1 + (ck2 + 3)) env1 [valTrees2CML l] =
            (st with clock := ck1 + ck2 + 3, Rval [rl]) /\
          valEqCml (Lval l) rl`
          by (qspecl_then [`l`, `st with clock := ck1 + (ck2 + 3)`, `env1`]
                destruct valTrees2CML_forward
              >- (conj_tac \\ first_x_assum irule \\ EVAL_TAC)
              \\ fs[])
      \\ simp[]
      (* now show that the accumulator evaluates to a value *)
      \\ rename1 `Foldvl (x,y) f i vl`
      \\ `?vi. evalExpNonDet empty_cfg E1 i vi`
          by (qpat_x_assum `evalExpNonDet _ _ (Foldvl _ _ _ _) _` mp_tac
              \\ rpt (pop_assum kall_tac)
              \\ qid_spec_tac `E1` \\ qid_spec_tac `x` \\ qid_spec_tac `y`
              \\ qid_spec_tac  `f` \\ qid_spec_tac `i` \\ qid_spec_tac `v`
              \\ qid_spec_tac `vl`
              \\ Induct_on `vl` \\ rw[evalExpNonDet_cases] \\ res_tac
              \\ find_exists_tac \\ fs[])
      \\ fs[]
      \\ qpat_assum `evalExpNonDet _ E1 i _` resolve_tac
      \\ qpat_x_assum `evalExpClocked E1 i _` resolve_tac
      \\ first_x_assum (qspecl_then [`st`, `env1`] destruct)
      >- (fs[usedVarsExp_def]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac
          \\ fs[expToCML_env_def]
          \\ irule isExtendedEnv_append_bin2 \\ rw[] \\ res_tac)
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1 + 3` destruct) \\ fs[]
      \\ `! s. evaluate s env1 [fold_name] = (s, Rval [foldr_v])`
          by (fs[fold_name_def, evaluate_def]
              \\ `nsLookup
                    (nsAppend (expToCML_env E1 (Foldvl (x,y) f i vl))
                      (merge_env ListProg_env_67 init_env).v)
                    (Long "List" (Short "foldr")) = SOME foldr_v`
                  by (once_rewrite_tac [nsLookup_nsAppend_some]
                      \\ DISJ2_TAC
                      \\ rpt conj_tac
                      >- (irule (CONJUNCT1 expToCML_env_no_list))
                      >- (EVAL_TAC)
                      \\ rpt strip_tac \\ fs[id_to_mods_def]
                      \\ Cases_on `p1` \\ fs[] \\ rveq
                      \\ fs[expToCML_env_no_listmod])
              \\ fs[isExtendedEnv_def] \\ res_tac \\ fs[])
      \\ asm_rewrite_tac []
      \\ simp_tac std_ss [pair_case_thm, result_case_def]
      \\ simp[do_opapp_def, foldr_v_def, Once find_recfun_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ simp[Once evaluate_def, dec_clock_def]
      \\ drule fold_forward_thm
      \\ rpt (disch_then drule)
      \\ disch_then
          (qspecl_then [`vl`, `ck1`, `v`, `i`, `stres`, `rl`, `env1`, `E1`, `r`,
                        `vi`, `y`, `x`, `List []`] destruct)
      \\ fs[]
      >- (fs [expToCML_env_def, usedVarsExp_def]
          \\ rw[] \\ first_x_assum irule \\ fs[])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct)
      \\ fs[])
  >- (Cases_on `b` \\ fs[evaluate_def, tree2IEEE_def, valEqCml_def])
  (* Unary predicate operator *)
  >- (rename1 `evalExpNonDet _ E1 e (Val v1)`
      \\ fs[empty_cfg_def, evalExpClocked_cases] \\ rveq
      \\ rename1 `evalExpClocked E1 e ck1`
      \\ first_assum (qspecl_then [`E1`, `Val v1`, `st`, `env1`, `ck1`] destruct)
      \\ fs[ expToCML_env_def, usedVarsExp_def, do_app_def,
            state_component_equality, evalExpClocked_cases]
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1` destruct) \\ fs[]
      \\ first_assum (qspecl_then [`E1`, `Val v1`, `stres`, `env1`, `ck1`] destruct)
      \\ fs[ expToCML_env_def, usedVarsExp_def, do_app_def,
            state_component_equality, evalExpClocked_cases]
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[]
      \\ Cases_on `r'` \\ fs [Once valEqCml_def]
      \\ Cases_on `l` \\ fs[]
      \\ Cases_on `r` \\ fs [Once valEqCml_def]
      \\ Cases_on `l` \\ fs[]
      \\ imp_res_tac rwAllValTree_empty_rewrites \\ rveq
      \\ fs[do_if_def, fp_cmp_def]
      \\ Cases_on `fp64_equal (tree2IEEE v1) (tree2IEEE v1)`
      \\ fs[tree2IEEE_def, GSYM fp64_eq_not_id_nan])
  (* Binary comparison operators *)
  >- (rename1 `evalExpNonDet _ E1 e1 (Val v1)`
      \\ rename1 `evalExpNonDet _ E1 e2 (Val v2)`
      \\ fs[evalExpClocked_cases] \\ rveq
      \\ first_x_assum (qspecl_then [`E1`, `Val v2`, `st`, `env1`, `ck2`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_bin2 \\ fs[usedVarsExp_def])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `ck1` destruct) \\ fs[]
      \\ first_x_assum (qspecl_then [`E1`, `Val v1`, `stres`, `env1`, `ck1`] destruct)
      >- (fs[ expToCML_env_def, do_app_def, usedVarsExp_def,
            state_component_equality]
          \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
          \\ irule isExtendedEnv_append_bin1 \\ fs[usedVarsExp_def])
      \\ drule evaluatePropsTheory.evaluate_add_to_clock
      \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[]
      \\ fs[do_app_def]
      \\ Cases_on `r'` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[]
      \\ Cases_on `r` \\ fs[Once valEqCml_def]
      \\ Cases_on `l` \\ fs[empty_cfg_def] \\ rveq
      \\ Cases_on `ce` \\ fs[tree2IEEE_def, fp_cmp_def, toCmp_def])
  (* Boolean scopes *)
  >- (rename1 `evalCexpNonDet _ E1 e v`
      \\ fs[evalExpClocked_cases]
      \\ qpat_x_assum `evalCexpClocked _ _ _` resolve_tac
      \\ first_x_assum (qspecl_then [`v`, `st`, `env1`] destruct)
      >- (fs[usedVarsExp_def, expToCML_env_def, empty_cfg_def]
          \\ irule (CONJUNCT2 evalNonDet_cfg_det) \\ fs[]
          \\ find_exists_tac \\ fs[])
      \\ find_exists_tac \\ fs[]
      \\ fs[valEqCml_def, tree2IEEE_def])
  >- (fs[nil_exp_def] \\ simp[evaluate_def, do_con_check_def]
      \\ fs[expToCML_env_def, isExtendedEnv_def]
     \\ `nsLookup (merge_env ListProg_env_67 init_env).c (Short "[]") =
          SOME (0, TypeStamp "[]" 1)`
        by (EVAL_TAC)
      \\ res_tac \\ fs[build_conv_def, valEqCml_def])
  \\ simp[Once evaluate_def, cons_exp_def, do_con_check_def]
  \\ `nsLookup (merge_env ListProg_env_67 init_env).c (Short "::") =
        SOME (2, TypeStamp "::" 1)`
      by (EVAL_TAC)
  \\ `nsLookup env1.c (Short "::") = SOME(2, TypeStamp "::" 1)`
      by (res_tac)
  \\ simp[evaluate_def]
  \\ qpat_x_assum `evalExpNonDet _ _ (List _) _` resolve_tac
  \\ fs[evalExpClocked_cases] \\ rveq
  \\ first_x_assum (qspecl_then [`st`, `env1`, `ck2`] destruct)
  >- (fs[usedVarsExp_def, expToCML_env_def]
      \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
      \\ irule isExtendedEnv_append_bin2
      \\ rw[] \\ first_x_assum irule \\ fs[usedVarsExp_def])
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `ck1` destruct) \\ fs[]
  \\ qpat_x_assum `evalExpNonDet _ _ e _` resolve_tac
  \\ first_x_assum (qspecl_then [`stres`, `env1`, `ck1`] destruct)
  >- (fs[usedVarsExp_def, expToCML_env_def]
      \\ irule isExtendedEnv_monotone \\ find_exists_tac \\ fs[]
      \\ irule isExtendedEnv_append_bin1
      \\ rw[] \\ first_x_assum irule \\ fs[usedVarsExp_def])
  \\ drule evaluatePropsTheory.evaluate_add_to_clock
  \\ disch_then (qspec_then `stres.clock` destruct) \\ fs[]
  \\ simp[build_conv_def, Once valEqCml_def]);

Theorem expToCML_backward
  `(! e E1 (st:'a semanticPrimitives$state) (env1:v sem_env) cfg ck stres r
      Gamma t.
    (* The environment is defined for the free variables, and the
      expression can be type-checked *)
    (! x.
           x IN freeVarsExp e ==>
           ? v.
             E1 x = SOME v /\
             ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
      wellDefined Gamma e = SOME t /\
    (* the environment contains the constructors *)
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) /\
    (* the environment contains at least the free variable values and the stdlib *)
    isExtendedEnv (nsAppend (expToCML_env E1 e) (^state).v) env1.v /\
    (* The Icing program does not overwrite stdlib names *)
    (! x. x IN (usedVarsExp e) ==> nsLookup ^state.v (Short x) = NONE) /\
    (* The program evaluates in Icing without optimizations *)
    evaluate (st with clock := ck) env1 [expToCML e] = (stres, Rval [r]) ==>
    ? v.
    evalExpNonDet empty_cfg E1 e v /\
    valEqCml v r)`
  (rw[]
  \\ assume_tac (CONJUNCT1 expToCML_forward)
  \\ qpat_x_assum `isExtendedEnv _ _` resolve_tac
  \\ `? v. evalExpNonDet (empty_cfg) E1 e v /\ hasType t v`
      by (irule (CONJUNCT1 wellDefined_gives_eval)
          \\ find_exists_tac \\ fs[])
  \\ `? ck. evalExpClocked E1 e ck`
      by (metis_tac [evalExpNonDet_is_clocked])
  \\ first_x_assum (qspecl_then [`v`, `st with clock := ck`, `ck'`] destruct)
  \\ fs[]
  \\ goal_assum (first_assum o mp_then Any mp_tac)
  \\ imp_res_tac evaluatePropsTheory.evaluate_add_to_clock
  \\ rpt (pop_assum destruct \\ fs[])
  \\ first_x_assum (qspec_then `ck'` assume_tac)
  \\ first_x_assum (qspec_then `ck` assume_tac)
  \\ fs[]);

(**
  Final equivalence theorem for the translation to CakeML of Icing expressions
**)
Theorem expToCML_equiv
  `! e E1 (st:'a semanticPrimitives$state) (env1:v sem_env) cfg ck Gamma t.
    (* The environment is defined for the free variables, and the
      expression can be type-checked *)
    (! x.
      x IN freeVarsExp e ==>
      ? v. E1 x = SOME v /\
      ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
    wellDefined Gamma e = SOME t /\
    (* the environment contains the constructors *)
    (! x v. nsLookup (^state).c x = SOME v ==>
      nsLookup env1.c x = SOME v) /\
    (* the environment contains at least the free variable values and the stdlib *)
    isExtendedEnv (nsAppend (expToCML_env E1 e) (^state).v) env1.v /\
    (* The Icing program does not overwrite stdlib names *)
    (! x. x IN (usedVarsExp e) ==> nsLookup ^state.v (Short x) = NONE) ==>
    (* The expression can be evaluated in cakeml semantics *)
    ((? stres r ck. evaluate (st with clock := ck) env1 [expToCML e] = (stres, Rval [r]))
      <=>
    (* The evaluation can be run in Icing semantics *)
    (? v. evalExpNonDet empty_cfg E1 e v)) /\
    (* and the results for any two evaluations always agree *)
    (! stres r v.
      evaluate (st with clock := ck) env1 [expToCML e] = (stres, Rval [r]) /\
      evalExpNonDet empty_cfg E1 e v ==>
      valEqCml v r)`
  (rpt strip_tac
  >- (once_rewrite_tac [EQ_IMP_THM] \\ rw[]
      >- (drule expToCML_backward
          \\ disch_then drule
          \\ disch_then imp_res_tac \\ find_exists_tac \\ fs[])
      \\ drule (CONJUNCT1 expToCML_forward)
      \\ rpt (disch_then drule)
      \\ `? ck. evalExpClocked E1 e ck`
          by (metis_tac [evalExpNonDet_is_clocked])
      \\ disch_then imp_res_tac
      \\ first_x_assum (qspec_then `st` assume_tac) \\ fs[]
      \\ find_exists_tac \\ fs[])
  \\ drule expToCML_backward
  \\ disch_then drule
  \\ disch_then imp_res_tac
  \\ `v = v'`
      by (irule (CONJUNCT1 evalNonDet_deterministic)
          \\ qexistsl_tac [`E1`, `empty_cfg`, `e`] \\ fs[empty_cfg_def])
  \\ fs[]);

val _ = export_theory();
