(**
  This file gives simulation proofs for each rewrite separately
  For each rewrite we show that its syntactic application at the top-level
  can be simulated by adding the rewrite to the set of rewrites allowed
  for the semantics
**)
open SyntaxTheory CompilerTheory rewriteRulesTheory patternTheory
     patternProofsTheory SemanticsTheory;
open CompilerTacticsLib;

open icingPreamble;

val _ = new_theory "rewriteRulesProofs";

Theorem ASSOCRULE_GEN_cases
  `!b1 b2 e11 e12.
      (?b3 e21 e22.
        (e11 = Binop b2 e21 e22 /\ b1 = b2 /\ b2 = b3) /\
        rewriteExp [ASSOCRULE_GEN b1] (Binop b1 (Binop b1 e21 e22) e12) =
          (Binop b1 e21 (Binop b1 e22 e12))) \/
      rewriteExp [ASSOCRULE_GEN b2] (Binop b1 e11 e12) = Binop b1 e11 e12`
  (rpt strip_tac \\ Cases_on `e11` \\ Cases_on `b1 = b2` \\ EVAL_TAC \\ fs[]
  \\ TOP_CASE_TAC \\ fs[] \\ rveq \\ EVAL_TAC);

(** Correctness proofs for above patterns **)
Theorem ASSOCRULE_GEN_correct
  `! b e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp [ASSOCRULE_GEN b] e) v ==>
      evalExpNonDet (cfg with opts:= (ASSOCRULE_GEN b)::cfg.opts) E e v`
  (rpt strip_tac \\ Cases_on `e` \\ solve_trivial_rw_tac
  \\ rename1 `Binop b2 e1 e2`
  \\ qspecl_then [`b2`, `b`, `e1`,`e2`] assume_tac ASSOCRULE_GEN_cases
  \\ fs[] \\ rveq
  >- (qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ rename1 `evalExpNonDet cfg E e2 (Val v2i)`
      \\ rename1 `evalExpNonDet cfg E e21 (Val v21)`
      \\ rename1 `evalExpNonDet cfg E e22 (Val v22)`
      \\ qexistsl_tac [`BinVal b v21 v22`, `v2i`]
      \\ rpt conj_tac
      >- (qexistsl_tac [`v21`, `v22`] \\ fs[rewritesTo_def]
          \\ rpt conj_tac \\ fs[]
          \\ drop_sem_rw_tac)
      >- drop_sem_rw_tac
      \\ fs[rewritesTo_def]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ rename1 `rwAllValTree insts2 T cfg.opts (BinVal b v211 v2) = SOME r`
      \\ last_assum (mp_then Any assume_tac rwAllValTree_comp_right)
      \\ first_x_assum (qspecl_then [`b`,`v211`] assume_tac) \\ fs[]
      \\ first_x_assum (mp_then Any assume_tac rwAllValTree_up)
      \\ first_x_assum
          (qspec_then `(ASSOCRULE_GEN b)::cfg.opts` destruct)
      \\ fs[SUBSET_DEF]
      \\ qpat_x_assum `rwAllValTree _ _ _ _ = SOME r`
          (mp_then Any assume_tac rwAllValTree_up)
      \\ first_x_assum (qspec_then `ASSOCRULE_GEN b ::cfg.opts` destruct)
      \\ fs[SUBSET_DEF]
      \\ qpat_x_assum `rwAllValTree _ _ _ _ = SOME (BinVal b v211 _)`
          (mp_then Any assume_tac rwAllValTree_chaining)
      \\ first_x_assum (qspecl_then [`r`, `insts2`] destruct) \\ fs[]
      \\ irule rwAllValTree_chaining
      \\ qexists_tac `[RewriteApp Here 0]`
      \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac \\ fs[]
      \\ EVAL_TAC)
  \\ fs[evalExpNonDet_cases, rewritesTo_def] \\ fs[]
  \\ qexistsl_tac [`v1`, `v2`] \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac);

(** Associativity theorems by instantiation **)
val assoc_thms =
  List.map
  (fn (s,b) =>
    curry save_thm s
    (ASSOCRULE_GEN_correct |> (SPEC b)
     |> ONCE_REWRITE_RULE (List.map GSYM [PLUSASSOCRULE_def, MULTASSOCRULE_def])))
  [("PLUSASSOCRULE_correct", ``Plus``), ("MULTASSOCRULE_correct", ``Mult``)];

Theorem COMMRULE_GEN_cases
  `!b1 b2 e1 e2.
        (b1 = b2 /\
         rewriteExp [COMMRULE_GEN b2] (Binop b1 e1 e2) =
        Binop b1 e2 e1) \/
      rewriteExp [COMMRULE_GEN b2] (Binop b1 e1 e2) = Binop b1 e1 e2`
  (rpt strip_tac \\ Cases_on `b1 = b2` \\ EVAL_TAC \\ fs[]
  \\ rveq \\ EVAL_TAC);

Theorem COMMRULE_GEN_correct
  `!b e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp [COMMRULE_GEN b] e) v ==>
      evalExpNonDet (cfg with opts := (COMMRULE_GEN b)::cfg.opts) E e v`
  (rpt strip_tac \\ Cases_on `e` \\ solve_trivial_rw_tac
  \\ rename1 `Binop b2 e1 e2`
  \\ qspecl_then [`b2`, `b`, `e1`,`e2`] assume_tac COMMRULE_GEN_cases
  \\ fs[] \\ rveq
  >- (qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ rename1 `evalExpNonDet cfg E e1 (Val v1i)`
      \\ rename1 `evalExpNonDet cfg E e2 (Val v2)`
      \\ rename1 `evalExpNonDet cfg E e1 (Val v1)`
      \\ qexistsl_tac [`v1`, `v2`]
      \\ rpt conj_tac
      >- drop_sem_rw_tac
      >- drop_sem_rw_tac
      \\ fs[rewritesTo_def]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllValTree_chaining
      \\ qexists_tac `[RewriteApp Here 0]`
      \\ first_x_assum (mp_then Any assume_tac rwAllValTree_up)
      \\ first_x_assum (qspec_then `COMMRULE_GEN b :: cfg.opts` destruct)
      \\ fs[SUBSET_DEF]
      \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac
      \\ fs[rwAllValTree_def] \\ EVAL_TAC)
  \\ qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
  \\ fs[evalExpNonDet_cases, rewritesTo_def] \\ fs[]
  \\ qexistsl_tac [`v1`, `v2`] \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac);

(** Commutativity theorems by instantiation **)
val comm_thms =
  List.map
  (fn (s,b) =>
    curry save_thm s
    (COMMRULE_GEN_correct |> (SPEC b)
     |> ONCE_REWRITE_RULE (List.map GSYM [PLUSCOMMRULE_def, MULTCOMMRULE_def])))
  [("PLUSCOMMRULE_correct", ``Plus``), ("MULTCOMMRULE_correct", ``Mult``)];

Theorem PMDISTRIBRULE_cases
  `!b e11 e12.
      (?e21 e22.
        (e12 = Binop Plus e21 e22 /\ b = Mult /\
        rewriteExp [PMDISTRIBRULE] (Binop b e11 e12) =
        Binop Plus (Binop Mult e11 e21) (Binop Mult e11 e22))) \/
      rewriteExp [PMDISTRIBRULE] (Binop b e11 e12) = Binop b e11 e12`
  (rpt strip_tac \\ Cases_on `e12` \\ EVAL_TAC \\ fs[]
  \\ TOP_CASE_TAC \\ fs[] \\ rveq \\ EVAL_TAC);

(** Syntactic criterion to check that distributivity can be applied,
  see section 4.3 of our CAV 2019 paper for a discussion *)
val isVarDistr_def = Define `
  isVarDistr (e:expr) =
  case e of
  | (Binop Mult (Var _) (Binop Plus (Var _) (Var _))) => T
  | _ => F `;

Theorem PMDISTRIBRULE_correct2
  `!e E v cfg.
    isVarDistr e /\
    cfg.canOpt /\
    evalExpNonDet cfg E (rewriteExp [PMDISTRIBRULE] e) v ==>
    evalExpNonDet (cfg with opts := PMDISTRIBRULE :: cfg.opts) E e v`
  (rpt strip_tac
  \\ Cases_on `e` \\ solve_trivial_rw_tac
  \\ rename1 `Binop b e1 e2`
  \\ qspecl_then [`b`, `e1`,`e2`] assume_tac PMDISTRIBRULE_cases
  \\ fs[isVarDistr_def] \\ rveq
  >- (fs[] \\ Cases_on `e21` \\ fs[] \\ Cases_on `e22` \\ fs[]
      \\ Cases_on `e1` \\ fs[]
      \\ rename1 `Binop Mult (Var x1) (Binop Plus (Var x2) (Var x3))`
      \\ qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ rveq
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ rename1 `rwAllValTree insts1 T cfg.opts (BinVal Mult v21 v22) = SOME v1`
      \\ rename1 `rwAllValTree insts2 T cfg.opts (BinVal Mult v21 v23) = SOME v2`
      \\ qexistsl_tac [`BinVal Plus v22 v23`]
      \\ rpt conj_tac \\ fs[rewritesTo_def]
      \\ last_x_assum (mp_then Any assume_tac rwAllValTree_comp_left)
      \\ last_x_assum (mp_then Any assume_tac rwAllValTree_comp_right)
      \\ first_x_assum (qspecl_then [`Plus`, `v1`] assume_tac)
      \\ first_x_assum (qspecl_then [`Plus`, `BinVal Mult v21 v23`] assume_tac) \\ fs[]
      \\ first_x_assum (mp_then Any assume_tac rwAllValTree_chaining)
      \\ res_tac
      \\ first_x_assum (mp_then Any assume_tac rwAllValTree_chaining)
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ res_tac
      \\ pop_assum kall_tac
      \\ first_x_assum (mp_then Any assume_tac rwAllValTree_up)
      \\ first_x_assum (qspecl_then [`PMDISTRIBRULE::cfg.opts`] destruct)
      \\ fs[SUBSET_DEF]
      \\ qexists_tac `RewriteApp Here 0 :: insts2` \\ fs[PMDISTRIBRULE_def] \\ EVAL_TAC \\ fs[])
  \\ qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
  \\ fs[evalExpNonDet_cases, rewritesTo_def] \\ fs[]
  \\ qexistsl_tac [`v1`, `v2`] \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac);

Theorem FMARULE_cases
  `!b e1 e2.
      (?e11 e12. e1 = Binop Mult e11 e12 /\ b = Plus /\
        (rewriteExp [FMARULE] (Binop b (Binop Mult e11 e12) e2) =
          Terop Fma e11 e12 e2))
      \/ rewriteExp [FMARULE] (Binop b e1 e2) = Binop b e1 e2`
  (rpt strip_tac
  \\ Cases_on `e1` \\ Cases_on `b`
  \\ fs[rewriteExp_def, FMARULE_def, matchesExpr_def, appExpr_def]
  \\ Cases_on `b'` \\ EVAL_TAC);

Theorem FMARULE_correct
  `!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp [FMARULE] e) v ==>
      evalExpNonDet (cfg with opts := FMARULE :: cfg.opts) E e v`
  (rpt strip_tac
  \\ Cases_on `e` \\ solve_trivial_rw_tac
  \\ rename1 `Binop b e1 e2`
  \\ qspecl_then [`b`,`e1`,`e2`] assume_tac FMARULE_cases \\ fs[]
  >- (rveq \\ qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qexistsl_tac [`BinVal Mult v1 v2`, `v3`]
      \\ conj_tac
      >- (qexistsl_tac [`v1`, `v2`] \\ fs[rewritesTo_def]
          \\ rpt conj_tac
          >- (drop_sem_rw_tac)
          >- (drop_sem_rw_tac)
          \\ qexists_tac `[]` \\ EVAL_TAC)
      \\ conj_tac >- drop_sem_rw_tac
      \\ fs[rewritesTo_def]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ `?insts.
            rwAllValTree insts T (FMARULE::cfg.opts) (TerVal Fma v1 v2 v3) = SOME r`
          by (ignore_sem_rws_tac)
      \\ irule rwAllValTree_chaining
      \\ qexists_tac `[RewriteApp Here 0]`
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac \\ fs[]
      \\ EVAL_TAC)
  \\ qpat_x_assum `rewriteExp _ _ = _` (fn thm => fs[thm])
  \\ fs[evalExpNonDet_cases, rewritesTo_def] \\ fs[]
  \\ qexistsl_tac [`v1`, `v2`] \\ rpt conj_tac
  >- (drop_sem_rw_tac)
  >- (drop_sem_rw_tac)
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac);

Theorem DNEG_cases
  `! op e.
      (?e1. e = Unop Neg e1 /\ op = Neg /\
        rewriteExp [DNEGRULE] (Unop Neg (Unop Neg e1)) = e1) \/
      rewriteExp [DNEGRULE] (Unop op e) = Unop op e`
  (rpt strip_tac \\ Cases_on `e`
  \\ fs[DNEGRULE_def, rewriteExp_def, matchesExpr_def, appExpr_def]
  \\ Cases_on `op` \\ fs[matchesExpr_def]
  \\ Cases_on `u` \\ fs[]
  \\ EVAL_TAC);

Theorem DNEG_correct
  `! e E v cfg.
    cfg.canOpt /\
    evalExpNonDet cfg E (rewriteExp [DNEGRULE] e) (Val v) ==>
    evalExpNonDet (cfg with opts := DNEGRULE::cfg.opts) E e (Val v)`
  (rpt strip_tac \\ Cases_on `e` \\ solve_trivial_rw_tac
  \\ qspecl_then [`u`,`e'`] assume_tac DNEG_cases
  \\ fs[] \\ rveq \\ fs[]
  >- (fs[evalExpNonDet_cases] \\ fs[]
      \\ qexists_tac `UnVal Neg v` \\ conj_tac \\ fs[]
      >- (qexists_tac `v` \\ conj_tac
          >- drop_sem_rw_tac
          \\ fs[rewritesTo_def] \\ qexists_tac `[]` \\ EVAL_TAC)
      \\ fs[rewritesTo_def]
      \\ qexists_tac `[RewriteApp Here 0]` \\ EVAL_TAC)
  \\ drop_sem_rw_tac);

Theorem NONANCHECKRULE_cases
  `!p e.
       (p = NaN /\ rewriteCexp [NONANCHECKRULE] (Pred p e) = Bconst F) \/
       rewriteCexp [NONANCHECKRULE] (Pred p e) = Pred p e`
  (rpt strip_tac \\ Cases_on `p`
  \\ fs[NONANCHECKRULE_def, rewriteCexp_def, rewriteExp_def,
        matchesExpr_def, matchesCexpr_def, appExpr_def]);

Theorem NONANCHECKRULE_correct
  `! c E v cfg.
      cfg.canOpt /\
      (? vRep. evalCexpNonDet cfg E c vRep) /\
      evalCexpNonDet cfg E (rewriteCexp [NONANCHECKRULE] c) v ==>
      evalCexpNonDet (cfg with opts := NONANCHECKRULE::cfg.opts) E c v`
  (rpt strip_tac \\ Cases_on `c` \\ solve_trivial_rw_tac
  \\ rename1 `Pred p e`
  \\ qspecl_then [`p`, `e`] assume_tac NONANCHECKRULE_cases
  \\ fs[]
  >- (rveq \\ fs[evalExpNonDet_cases] \\ rveq
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ qexists_tac `v'` \\ conj_tac
      >- (drop_sem_rw_tac)
      \\ qexists_tac `[RewriteApp Here 0]`
      \\ EVAL_TAC)
  \\ qpat_x_assum `rewriteCexp _ _ = _` (fn thm => fs[thm])
  \\ drop_sem_rw_tac);

val _ = export_theory ();
