structure CompilerTacticsLib =
struct

open patternTheory patternProofsTheory SemanticsTheory;
open preamble;

val simp_no_rw_tac =
  (qpat_x_assum `evalExpNonDet _ _ (rewriteExp _  _) _` (fn thm => assume_tac (EVAL_RULE thm))
  ORELSE
  qpat_x_assum `evalCexpNonDet _ _ (rewriteCexp _  _) _` (fn thm => assume_tac (EVAL_RULE thm)))
  \\ fs[evalExpNonDet_cases, rewritesTo_def]
  \\ IMP_RES_TAC rwAllValTree_empty_rewrites \\ rveq;

val trivial_rewrite_tac =
  simp_no_rw_tac \\ qexists_tac `[]` \\ EVAL_TAC;

fun ttac_to_ltac t ls =
  case ls of
   [] => all_tac
  | t1 :: rem => t t1 THEN ttac_to_ltac t rem;

fun qexistsl_tac ls = ttac_to_ltac qexists_tac ls;

(* Ignore a subset of semantic rewrites for an application of rwAllValTree,
  reusing an already proven hypothesis *)
val ignore_sem_rws_tac =
  (irule rwAllValTree_up ORELSE irule rwAllCvalTree_up)
  \\ (asm_exists_tac ORELSE
     (once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac))
  \\ fs[SUBSET_DEF, UNION_DEF];

(* Drop a set of rewrites from the evaluation as the are ignored,
  some hypothesis must prove a subset of the rewrites to evaluate to a result *)
val drop_sem_rw_tac =
  (irule (hd (CONJ_LIST 2 evalNonDet_add_rewrite)) ORELSE
  irule (hd (tl (CONJ_LIST 2 evalNonDet_add_rewrite))))
  \\ fs[]
  \\ (asm_exists_tac ORELSE
     (rewrite_tac [CONJ_ASSOC] \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac))
  \\ fs[SUBSET_DEF, UNION_DEF]
  \\ rpt strip_tac \\ fs[];

fun drop_sem_conf_tac cfg_new =
  (irule (Q.SPEC cfg_new (hd (CONJ_LIST 2 evalNonDet_add_rewrite))) ORELSE
  irule (Q.SPEC cfg_new (hd (tl (CONJ_LIST 2 evalNonDet_add_rewrite)))))
  \\ fs[]
  \\ (asm_exists_tac ORELSE
     (rewrite_tac [CONJ_ASSOC] \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac))
  \\ fs[SUBSET_DEF, UNION_DEF]
  \\ rpt strip_tac \\ fs[];

(* specialized tactics to prove that rewriteexp does nothing *)
val no_list_rw_tac =
  rename1 `List l`
  \\ simp_no_rw_tac \\ fs[]
  \\ drop_sem_rw_tac;

val no_ith_rw_tac =
  rename1 `Ith n e`
  \\ simp_no_rw_tac \\ fs[]
  \\ qexists_tac `vl` \\ fs[]
  \\ drop_sem_rw_tac;

val no_unop_rw_tac =
  rename1 `Unop u e`
  \\ simp_no_rw_tac \\ fs[]
  \\ rename1 `evalExpNonDet cfg E e (Val v1)`
  \\ qexists_tac `v1` \\ conj_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac;

val no_binop_rw_tac =
  rename1 `Binop b e1 e2` \\ simp_no_rw_tac \\ fs[]
  \\ rename1 `evalExpNonDet rws_init E e1 (Val v1)`
  \\ rename1 `evalExpNonDet rws_init E e2 (Val v2)`
  \\ qexistsl_tac [`v1`, `v2`] \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `rws_init.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac;

val no_terop_rw_tac =
  rename1 `Terop top e2 e3 e4` \\ simp_no_rw_tac \\ fs[]
  \\ rename1 `evalExpNonDet _ _ e2 (Val ve2)`
  \\ rename1 `evalExpNonDet _ _ e3 (Val ve3)`
  \\ rename1 `evalExpNonDet _ _ e4 (Val ve4)`
  \\ qexistsl_tac [`ve2`, `ve3`, `ve4`] \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac;

val no_let_rw_tac =
  rename1 `Let x e1 e2` \\ simp_no_rw_tac
  \\ rename1 `evalExpNonDet rws_init E e1 v1`
  \\ rename1 `evalExpNonDet rws_init _ e2 v2`
  \\ qexists_tac `v1` \\ conj_tac \\ drop_sem_rw_tac;

val no_cond_rw_tac =
  rename1 `Cond c1 e1 e2` \\ simp_no_rw_tac
  \\ rename1 `evalExpNonDet rws_init E _ vres`
  THENL[ DISJ1_TAC, DISJ2_TAC ]
  \\ rewrite_tac [CONJ_ASSOC] \\ once_rewrite_tac [CONJ_COMM]
  \\ asm_exists_tac \\ fs[] \\ conj_tac \\ drop_sem_rw_tac;

val no_scope_rw_tac =
  rename1 `Scope sc e` \\ simp_no_rw_tac
  \\ rename1 `evalExpNonDet _ E _ vres`
  \\ qexists_tac `vres` \\ fs[]
  \\ drop_sem_rw_tac
  \\ `cfg with canOpt := T = cfg`
    by (fs[Icing_config_component_equality])
  \\ pop_assum (fn thm => fs[thm]);

val no_map_rw_tac =
  rename1 `Map x f l` \\ simp_no_rw_tac
  \\ qexists_tac `vl` \\ conj_tac
  \\ drop_sem_rw_tac;

val no_mapvl_rw_tac =
  rename1 `Mapvl x f l` \\ simp_no_rw_tac
  \\ drop_sem_rw_tac;

val no_fold_rw_tac =
  rename1 `Fold p f s l` \\ simp_no_rw_tac
  \\ drop_sem_rw_tac;

val no_foldvl_rw_tac =
  rename1 `Foldvl p f s l` \\ simp_no_rw_tac
  \\ drop_sem_rw_tac;

val no_cmp_rw_tac =
  simp_no_rw_tac \\ fs[]
  \\ qexistsl_tac [`v1'`, `v2'`]
  \\ rpt conj_tac
  >- drop_sem_rw_tac
  >- drop_sem_rw_tac
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ ignore_sem_rws_tac;

val no_cscope_rw_tac =
  simp_no_rw_tac
  \\ Cases_on `cfg` \\ drop_sem_rw_tac;

val solve_trivial_rw_tac =
  (no_list_rw_tac \\ FAIL_TAC "")
  ORELSE (no_ith_rw_tac \\ FAIL_TAC "")
  ORELSE (no_unop_rw_tac \\ FAIL_TAC "")
  ORELSE (no_binop_rw_tac \\ FAIL_TAC "")
  ORELSE (no_terop_rw_tac \\ FAIL_TAC "")
  ORELSE (no_let_rw_tac \\ FAIL_TAC "")
  ORELSE (no_cond_rw_tac \\ FAIL_TAC "")
  ORELSE (no_scope_rw_tac \\ FAIL_TAC "")
  ORELSE (no_map_rw_tac \\ FAIL_TAC "")
  ORELSE (no_mapvl_rw_tac \\ FAIL_TAC "")
  ORELSE (no_fold_rw_tac \\ FAIL_TAC "")
  ORELSE (no_foldvl_rw_tac \\ FAIL_TAC "")
  ORELSE (no_cmp_rw_tac \\ FAIL_TAC "")
  ORELSE (no_cscope_rw_tac \\ FAIL_TAC "")
  ORELSE (trivial_rewrite_tac \\ FAIL_TAC "")
  ORELSE ALL_TAC;

end
