structure patternTacticsLib =
struct

open patternProofsTheory;
open preamble;

val optimize_tac =
  rpt (asm_exists_tac \\ fs[])
  \\ irule rwAllValTree_up ORELSE irule rwAllCvalTree_up
  \\ rpt (asm_exists_tac \\ fs[]);

end
