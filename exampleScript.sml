open lexerParserTheory SyntaxTheory CompilerTheory;

open icingPreamble;

val _ = new_theory "example";

val getOrDie_def = Define `
  getOrDie x = case parseExp (lex x) of (SOME (e, ls) => e)`;

(**
  Example from section 3.2/Figure 3
**)
val exampleSec3_def =
  Define `
  exampleSec2 =
    getOrDie "let v1 = Map fn x. opt:(x + 0x3) vi in let vsum = (Fold fn x y. opt:((x * x) + y) (0x0) el) in sqrt vsum"`

(* Introducing an FMA but no commutativity on the Fold *)
val exEval1 =
EVAL ``optimizeGreedy
  [PLUSCOMMRULE; MULTCOMMRULE; FMARULE; PLUSASSOCRULE; MULTASSOCRULE]
  exampleSec2``;

(* No FMA but commutativity in both *)
val exEval2 =
EVAL ``optimizeGreedy
  [PLUSASSOCRULE; MULTASSOCRULE; FMARULE; PLUSCOMMRULE; MULTCOMMRULE]
  exampleSec2``;

val dotProduct_3d_def = Define `
  dotProduct_3d =
    parseExp (lex "opt: ((a1 * b1) + (a2 * b2) + (a3 * b3))")`

val dotProduct_parsed = EVAL ``case dotProduct_3d of |SOME (e, ls) => e``

val dotProduct_ast = rhs (concl dotProduct_parsed);

val dotProduct_opt = rhs (concl (EVAL ``optimizeGreedy [FMARULE] ^dotProduct_ast``));

val euclideanNorm_def = Define `
  euclideanNorm =
    getOrDie "let x = (Fold fn x y. opt:(x * x + y) (0x0) el) in sqrt x"`;

val invertedPendulum_def = Define `
  invertedPendulum =
  getOrDie "opt:((0x1 * s1) + (0x1 * s2) + (-(Ox18) * s3) + (- (0x3) * s4))"`;

val rotateClockWise_def = Define `
  rotateClockWise =
  getOrDie "[[y * - 0x1, x * 0x1]]"`;

val rotateClockWise_def = Define `
  rotateCounterClockWise =
  getOrDie "[[y * 0x1, x * - 0x1]]"`;

val crossProduct_3D_def = Define `
  crossProduct_3D =
  getOrDie (CONCAT [
    "opt: ( ";
    "let cx = ((al[1]) * (bl[2])) - ((al[2]) * (bl[1])) in ";
    "let cy = ((al[2]) * (bl[0])) - ((al[0]) * (bl[2])) in ";
    "let cz = ((al[0]) * (bl[1])) - ((al[1]) * (bl[0])) in ";
    "[[cx, cy, cz]])"])`;

(* Some Map examples: *)
val vectorShift_def = Define `
  vectorShift =
    getOrDie "Map fn x. opt:(x + 0x3) vec"`;

val vectorScale_def = Define `
  vectorScale =
    getOrDie "Map fn x. opt:(x * 0x2) vec"`;

val vectorShiftAndScale_def = Define `
  vectorShiftAndScale =
    getOrDie "Map fn x. opt:(x * 0x2) (Map fn x. opt:(x + 0x1 *0x5) vec)"`;

(** arcLength benchmark originallt from Precimonious paper, adapted from
https://gitlab.mpi-sws.org/AVA/daisy/blob/master/testcases/precimonious/ArcLength.scala **)
(**
(* require (100 <= z && z <= 103) *)
(* TODO: Takes too long to parse *)
val arcLength_def = Define `
  arcLength =
  getOrDie (CONCAT [
    "let maini = 0x1 in ";
    (* Precimonious paper used mainn as 1000000, but this leads to a divide-by-zero exception in daisy *)
    "let mainn = 0x1000 in ";
    (* super rough approx of Pi... *)
    "let maindppi = 0x3 in ";
    "let mains10 = 0x0 in ";
    "let maint10 = 0x0 in ";
    "let mainh = maindppi / mainn in ";
    (* Iteration maini of main loop *)
    (* One call to fun(i*h) *)
    (*
    "let x = maini * mainh in ";
    "let t10 = x in ";
    "let d10 = 0x1 in ";
    (* Iteration 1 *)
    "let d11 = 0x2 * d10 in ";
    (* Note: Uses small angle approximation. Originally sin(d11 * x), no0x(d11 * x) *)
    "let t11 = t10 + (d11 * x) / d11 in ";
    (* Iteration 2 *)
    "let d12 = 0x2 * d11 in ";
    "let t12 = t11 + (d12 * x) / d12 in ";
    (* Iteration 3 *)
    "let d13 = 0x2 * d12 in ";
    "let t13 = t12 + (d13 * x) / d13 in ";
    (* Iteration 4 *)
    "let d14 = 0x2 * d13 in ";
    "let t14 = t13 + (d14 * x) / d14 in ";
    (* Iteration 5 *)
    "let d15 = 0x2 * d14 in ";
    "let t15 = t14 + (d15 * x) / d15 in ";
    (* Storing result of fun(i*h) *)
    "let maint2 = (t15) in ";
    "let mains11 = mains10 + sqrt(mainh*mainh + (maint2 - maint10)*(maint2 - maint10)) in ";
    "let maint11 = maint2 in "; *)
    (* End of Iteration maini of main loop *)
    "mains11"
  ])
 `;
(* ensuring (res => res +/- 1e-14) *)
  **)

(* TODO Proof-read for parse failures *)
val arcLength_AST_def = Define `
  arcLength_AST =
    Let "maini" (Wconst 1w)
    (Let "mainn" (Wconst 1000w)
    (Let "maindppi" (Wconst 3w)
    (Let "mains10" (Wconst 0w)
    (Let "maint10" (Wconst 0w)
    (Let "mainh" (Binop Div (Var "maindppi") (Var "mainn"))
    (Let "x" (Binop Mult (Var "maini") (Var "mainh"))
    (Let "t10" (Var "x")
    (Let "d10" (Wconst 1w)
    (Let "d11" (Binop Mult (Wconst 2w) (Var "d10"))
    (Let "t11"
       (Binop Plus (Var "t10")
          (Binop Div (Binop Mult (Var "d11") (Var "x"))
             (Var "d11")))
    (Let "d12" (Binop Mult (Wconst 2w) (Var "d11"))
    (Let "t12"
       (Binop Plus (Var "t11")
          (Binop Div (Binop Mult (Var "d12") (Var "x"))
             (Var "d12")))
    (Let "d13" (Binop Mult (Wconst 2w) (Var "d12"))
    (Let "t13"
       (Binop Plus (Var "t12")
          (Binop Div
             (Binop Mult (Var "d13") (Var "x"))
             (Var "d13")))
    (Let "d14"
       (Binop Mult (Wconst 2w) (Var "d13"))
    (Let "t14"
       (Binop Plus (Var "t13")
          (Binop Div
             (Binop Mult (Var "d14") (Var "x"))
             (Var "d14")))
    (Let "d15"
       (Binop Mult (Wconst 2w) (Var "d14"))
    (Let "t15"
       (Binop Plus (Var "t14")
          (Binop Div
             (Binop Mult (Var "d15")
                (Var "x")) (Var "d15")))
    (Let "maint2" (Var "t15")
    (Let "mains11"
       (Binop Plus (Var "mains10")
          (Unop Sqrt
             (Binop Mult
                (Var "mainh")
                (Binop Plus
                   (Var "mainh")
                   (Binop Mult
                      (Binop Minus (Var "maint2") (Var "maint10"))
                      (Binop Minus (Var "maint2") (Var "maint10")))))))
    (Let "maint11" (Var "maint2")
       (Var "mains11"))))))))))))))))))))))`;

val _ = export_theory();
