(**
  This file defines the rewrite rules used in the Icing compiler.
  In the CAV 2019 paper the rewrites are defined in Table 1
**)

open SemanticsTheory patternTheory valueTreesTheory;

open icingPreamble;

val _ = new_theory "rewriteRules";

(** Real-valued, algebraic laws as rewrites **)

(** A generator for associativity rewrites **)
val ASSOCRULE_GEN_def = Define `
  ASSOCRULE_GEN (b:binop) =
    (Binop b (Binop b (Var 0) (Var 1)) (Var 2),
     Binop b (Var 0) (Binop b (Var 1) (Var 2)))`;

(**
  a + (b + c) -> (a + b) + c
**)
val PLUSASSOCRULE_def = Define `
  PLUSASSOCRULE = ASSOCRULE_GEN Plus`;

(**
  a * (b * c) -> (a * b) * c
**)
val MULTASSOCRULE_def = Define `
  MULTASSOCRULE = ASSOCRULE_GEN Mult`;

(** A generator for commutativity rewrites **)
val COMMRULE_GEN_def = Define `
  COMMRULE_GEN (b:binop) =
  (Binop b (Var 0) (Var 1), Binop b (Var 1) (Var 0))`;

(**
  a + b -> b + a
**)
val PLUSCOMMRULE_def = Define `
  PLUSCOMMRULE = COMMRULE_GEN Plus`;

(**
  a * b -> b * a
**)
val MULTCOMMRULE_def = Define `
  MULTCOMMRULE = COMMRULE_GEN Mult`;

(** Distributivity of +,*:
  a * (b + c) -> a * b + a * c
**)
val PMDISTRIBRULE_def = Define `
  PMDISTRIBRULE =
    (Binop Mult (Var 0) (Binop Plus (Var 1) (Var 2)),
     Binop Plus (Binop Mult (Var 0) (Var 1)) (Binop Mult (Var 0) (Var 2)))`;

(** Double negation:
  - - a -> a
**)
val DNEGRULE_def = Define `
  DNEGRULE =
    (Unop Neg (Unop Neg (Var 0)), Var 0) `;

(**  floating-point specific rewrites **)

(** FMA introduction
  a * b + c -> FMA (a,b,c)
**)
val FMARULE_def = Define `
  FMARULE =
    (Binop Plus (Binop Mult (pattern$Var 0) (Var 1)) (Var 2),
     Terop Fma (Var 0) (Var 1) (Var 2))`;

(** Rewrites assuming real-valued behavior for floating-point *)

(** Removing NaN checks, this rewrite should only be applied with a rewrite
    precondition **)
val NONANCHECKRULE_def = Define `
  NONANCHECKRULE =
  ((Pred NaN (Var 0)), Bool F)`;

val _ = export_theory ();
