(* Copied code from CakeML preamble for niceness of reading *)
structure icingPreamble =
struct
local open intLib wordsLib in end;
open set_relationTheory; (* comes first so relationTheory takes precedence *)
open BasicProvers Defn HolKernel Parse SatisfySimps Tactic monadsyntax
     alistTheory arithmeticTheory bagTheory boolLib boolSimps bossLib
     combinTheory dep_rewrite finite_mapTheory indexedListsTheory lcsymtacs
     listTheory llistTheory markerLib mp_then optionTheory pairLib pairTheory
     pred_setTheory quantHeuristicsLib relationTheory res_quanTheory
     rich_listTheory sortingTheory sptreeTheory stringTheory sumTheory
     wordsTheory;
(* TOOD: move? *)
val wf_rel_tac = WF_REL_TAC
val induct_on = Induct_on
val cases_on = Cases_on;
val every_case_tac = BasicProvers.EVERY_CASE_TAC;
val full_case_tac = BasicProvers.FULL_CASE_TAC;
val sym_sub_tac = SUBST_ALL_TAC o SYM;
fun asm_match q = Q.MATCH_ASSUM_RENAME_TAC q
val match_exists_tac = part_match_exists_tac (hd o strip_conj)
val asm_exists_tac = first_assum(match_exists_tac o concl)
val has_pair_type = can dest_prod o type_of

val rveq = rpt BasicProvers.VAR_EQ_TAC

val _ = set_trace"Goalstack.print_goal_at_top"0 handle HOL_ERR _ => set_trace"goalstack print goal at top"0

val _ = ParseExtras.tight_equality();

fun destruct th =
  let
      val hyp_to_prove = lhand (concl th)
  in
      SUBGOAL_THEN hyp_to_prove (fn thm => STRIP_ASSUME_TAC (MP th thm))
  end;

fun ttac_to_ltac t ls =
  case ls of
   [] => all_tac
  | t1 :: rem => t t1 THEN ttac_to_ltac t rem;

fun qexistsl_tac ls = ttac_to_ltac qexists_tac ls;

fun mk_lambda_tm (concl:term) (fVars:term list) =
  case fVars of
   [] => concl
  | v :: fVarRem =>
      mk_lambda_tm (``\ ^v. ^concl``) fVarRem;

fun spec_for_thms thm thms =
  case thms of
   [] => thm
  | thm1 :: thms =>
    let
      val term1 = mk_lambda_tm thm1 (free_vars thm1);
    in
      spec_for_thms (Q.SPEC `^term1` thm) thms
    end;

(* Some test...
  spec_for_thms expr_induction [``! e2:expr. e1 = e2``]
  (mk_lambda_tm ``! e2:expr. e1 = e2`` (free_vars ``! e2:expr. e1 = e2``))
*)

fun mut_induct_tac thm thms =
  let
    val tm1 = hd thms;
    val tmrm = tl thms;
    (* val numfVars = length (free_vars tm1); *)
    (* val allfVars_same_count  = *)
    (*   foldl (fn (t, b) => b andalso (length (free_vars t) = numfVars)) true tmrm; *)
  in
    if (true) (* allfVars_same_count) *)
    then
      reverse (destruct (spec_for_thms thm thms))
    else
      raise Exn.ERROR "theorem list does not have same amount of free variables"
  end;

val fth = fn thm => hd (CONJ_LIST 2 thm)
val sth = fn thm => hd (tl (CONJ_LIST 2 thm))

fun iter_exists_tac ind n =
  fn tm =>
    if ind < n
    then
      (part_match_exists_tac
        (fn concl => List.nth (strip_conj concl, ind)) tm)
        ORELSE (iter_exists_tac (ind+1) n tm)
    else
      FAIL_TAC (concat ["No matching clause found for ", term_to_string tm]) ;

val try_all:term -> tactic =
  fn tm =>
    fn (asl, g) =>
      let
        val len = length (strip_conj (snd (dest_exists g))) in
        iter_exists_tac 0 len tm (asl, g)
     end;

val find_exists_tac =
  first_assum (try_all o concl);

infix 8 specWith;
fun thmtac specWith list =
  (fn thm => thmtac (Q.SPECL list thm));

fun resolve_tac thm =
  first_x_assum (fn ithm => mp_then Any assume_tac ithm thm);

fun first_res_tac t = fn thm => first_x_assum (fn ithm => mp_then Any t ithm thm);
fun last_res_tac t = fn thm => last_x_assum (fn ithm => mp_then Any t ithm thm);

end
