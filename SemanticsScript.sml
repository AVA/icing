(**
  Formalization of Icing semantics from section 3
  and proof of theorem 5.5 (Icing semantics are deterministic
**)
open SyntaxTheory valueTreesTheory patternTheory patternTacticsLib;
open icingPreamble;

val _ = new_theory "Semantics";

val bind_def = Define `
  bind E x v = \y. if y = x then SOME v else E y`;

val Icing_config_def = Datatype `
  (* Icing configuration for the semantics *)
  Icing_config =
    <| opts : rw_rule list (* List of rewrite rules for expressions *)
     ; canOpt : bool (* Flag to denote whether the semantics can optimize or not *)
    |>`;

val rewritesTo_def = new_infixl_definition(
  "rewritesTo_def",
  ``$rewritesTo (v, cfg) v_opt =
      if (cfg.canOpt)
      then ?insts. rwAllValTree insts cfg.canOpt cfg.opts v = SOME v_opt
      else v = v_opt``,480);

Theorem no_opt_thm[simp]
  `~ cfg.canOpt ==> ((v,cfg) rewritesTo v_opt <=> v = v_opt)`
  (rw[rewritesTo_def]);

val crewritesTo_def = new_infixl_definition(
  "crewritesTo_def",
  ``$crewritesTo (cv, cfg) v_opt =
      if (cfg.canOpt)
      then ?insts. rwAllCvalTree insts cfg.canOpt cfg.opts cv = SOME v_opt
      else cv = v_opt``,481);

Theorem no_copt_thm[simp]
  `~ cfg.canOpt ==> ((cv, cfg) crewritesTo cv_opt <=> cv = cv_opt)`
  (rw[crewritesTo_def]);

val _ = export_rewrites ["rewritesTo_def", "crewritesTo_def"];

val wrapAll_def = Define `
  wrapAll [] = [] /\
  wrapAll (v::vl) = (valueTrees$Scope Opt v)::(wrapAll vl) `;

val wrapScope_def = Define `
  wrapScope (Val v) = Val (valueTrees$Scope Opt v) /\
  wrapScope (Cval v) = Cval (valueTrees$Cscope Opt v) /\
  wrapScope (Lval vl) = (Lval (wrapAll vl))`;

(**
  Icing semantics from the paper.
  Expression evaluation semantics
  function eval takes 3 parameters.
  rws, a list of rewrites that can be used to optimize value trees
  E, the environment for the evaluation
  e, the expression to be evaluated
**)
val (evalExpNonDet_rules, evalExpNonDet_ind, evalExpNonDet_cases) = Hol_reln `
  (* Constants *)
  (! cfg E v.
    evalExpNonDet cfg E (Syntax$Wconst v) (Val (valueTrees$Wconst v))) /\
  (* Variables *)
  (! cfg E n r.
    E n = SOME r ==>
    evalExpNonDet cfg E (Var n) r) /\
  (* List projection *)
  (! cfg E e n v vl.
    evalExpNonDet cfg E e (Lval vl) /\
    n < LENGTH vl /\
    oEL n vl = SOME v ==>
    evalExpNonDet cfg E (Ith n e) (Val v)) /\
  (* Unary operations *)
  (! cfg E e v r u.
    evalExpNonDet cfg E e (Val v) /\
    (UnVal u v, cfg) rewritesTo r ==>
    evalExpNonDet cfg E (Unop u e) (Val r)) /\
  (* Binary operations *)
  (!cfg E op e1 e2 v1 v2 r.
    evalExpNonDet cfg E e1 (Val v1) /\
    evalExpNonDet cfg E e2 (Val v2) /\
    (BinVal op v1 v2, cfg) rewritesTo r ==>
    evalExpNonDet cfg E (Binop op e1 e2) (Val r)) /\
  (* Ternary operations *)
  (!cfg E op e1 e2 e3 v1 v2 v3 r.
    evalExpNonDet cfg E e1 (Val v1) /\
    evalExpNonDet cfg E e2 (Val v2) /\
    evalExpNonDet cfg E e3 (Val v3) /\
    (TerVal op v1 v2 v3, cfg) rewritesTo r ==>
    evalExpNonDet cfg E (Terop op e1 e2 e3) (Val r)) /\
  (* Let bindings
    -> Do not restrict value as we want to be able to let bind map results *)
  (!cfg E e1 e2 x v1 v2.
    evalExpNonDet cfg E e1 v1 /\
    evalExpNonDet cfg (bind E x v1) e2 v2 ==>
    evalExpNonDet cfg E (Let x e1 e2) v2) /\
  (* Conditionals *)
  (! cfg E e1 e2 ce cv v1.
    evalExpNonDet cfg E e1 v1 /\
    evalCexpNonDet cfg E ce cv /\
    cTree2IEEE cv ==>
    evalExpNonDet cfg E (Cond ce e1 e2) v1) /\
  (! cfg E e1 e2 ce cv v2.
    evalExpNonDet cfg E e2 v2 /\
    evalCexpNonDet cfg E ce cv /\
    ~cTree2IEEE cv ==>
    evalExpNonDet cfg E (Cond ce e1 e2) v2) /\
  (! cfg E e v.
    evalExpNonDet (cfg with canOpt := T) E e v ==>
    evalExpNonDet cfg E (Syntax$Scope Opt e) (wrapScope v)) /\
  (* Map applications;
    first go from Syntax to semantic in arg *)
  (! cfg E x f e vl vres.
    evalExpNonDet cfg E e (Lval vl) /\
    evalExpNonDet cfg E (Mapvl x f vl) vres ==>
    evalExpNonDet cfg E (Map x f e) vres) /\
  (* empty list case *)
  (! cfg E x f.
    evalExpNonDet cfg E (Mapvl x f []) (Lval [])) /\
  (* Recursive map application + optimization *)
  (! cfg E x f v vl vrl v_res.
    evalExpNonDet cfg (bind E x (Val v)) f (Val v_res) /\ (* apply function to result *)
    evalExpNonDet cfg E (Mapvl x f vl) (Lval vrl) ==> (* recursive eval *)
    evalExpNonDet cfg E (Mapvl x f (v::vl)) (Lval (v_res::vrl))) /\
  (* Foldl applications;
    first fo from Syntax to semantic arg *)
  (! cfg E x y e i f vl vres.
    evalExpNonDet cfg E e (Lval vl) /\
    evalExpNonDet cfg E (Foldvl (x,y) f i vl) vres ==>
    evalExpNonDet cfg E (Fold (x,y) f i e) vres) /\
  (* empty list *)
  (! cfg E x y e f v.
    evalExpNonDet cfg E e v ==>
    evalExpNonDet cfg E (Foldvl (x,y) f e []) v) /\
  (! cfg E x y f es v vl v_rec v_res.
    evalExpNonDet cfg E (Foldvl (x,y) f es vl) v_rec /\ (* recursive result, needed for foldl *)
    evalExpNonDet cfg (bind (bind E y (Val v)) x v_rec) f v_res ==> (* eval final *)
    evalExpNonDet cfg E (Foldvl (x,y) f es (v::vl)) v_res)
  /\
  (! cfg E.
    evalExpNonDet cfg E (List []) (Lval [])) /\
  (! cfg E e1 els v1 vl.
    evalExpNonDet cfg E (List els) (Lval vl) /\
    evalExpNonDet cfg E e1 (Val v1) ==>
    evalExpNonDet cfg E (List (e1::els)) (Lval (v1::vl)))
  /\
  (* Boolean constants *)
  (! cfg E b. evalCexpNonDet cfg E (Bconst b) (Bconst b)) /\
  (* Predicates *)
  (! cfg E e v vopt.
    evalExpNonDet cfg E e (Val v) /\
    (Pred NaN v, cfg) crewritesTo vopt ==>
    evalCexpNonDet cfg E (Pred NaN e) vopt) /\
  (* Boolean comparisons *)
  (! cfg E cmp e1 e2 v1 v2 vopt.
    evalExpNonDet cfg E e1 (Val v1) /\
    evalExpNonDet cfg E e2 (Val v2) /\
    (Cmp cmp v1 v2, cfg) crewritesTo vopt ==>
    evalCexpNonDet cfg E (Cmp cmp e1 e2) vopt) /\
  (* Boolean scopes *)
  (! cfg E e v.
    evalCexpNonDet (cfg with canOpt := T) E e v ==>
    evalCexpNonDet cfg E (Cscope Opt e) (Cscope Opt v))`;

val evalExpNonDet_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once evalExpNonDet_cases])
    [``evalExpNonDet cfg E (Wconst n) res``,
     ``evalExpNonDet cfg E (Var x) res``,
     ``evalExpNonDet cfg E (List []) res``,
     ``evalExpNonDet cfg E (List (e::els)) res``,
     ``evalExpNonDet cfg E (Ith n e) res``,
     ``evalExpNonDet cfg E (Unop u e) res``,
     ``evalExpNonDet cfg E (Binop b e1 e2) res``,
     ``evalExpNonDet cfg E (Terop op e1 e2 e3) res``,
     ``evalExpNonDet cfg E (Let x e1 e2) res``,
     ``evalExpNonDet cfg E (Cond c e1 e2) res``,
     ``evalExpNonDet cfg E (Scope sc e) res``,
     ``evalExpNonDet cfg E (Map x f e) res``,
     ``evalExpNonDet cfg E (Mapvl x f []) res``,
     ``evalExpNonDet cfg E (Mapvl x f (e::expl)) res``,
     ``evalExpNonDet cfg E (Fold (x,y) f i e) res``,
     ``evalExpNonDet cfg E (Foldvl (x,y) f e []) res``,
     ``evalExpNonDet cfg E (Foldvl (x,y) f e (es::expl)) res``,
     ``evalCexpNonDet cfg E (Bconst b) res``,
     ``evalCexpNonDet cfg E (Pred pr e) res``,
     ``evalCexpNonDet cfg E (Cmp cmp e1 e2) res``,
     ``evalCexpNonDet cfg E (Cscope sc e) res``
     ]
  |> LIST_CONJ |> curry save_thm "evalExpNonDet_cases";

val [Const_nonDet, Var_nonDet, Ith_nonDet, Unop_nonDet, Binop_nonDet,
     Terop_nonDet, Let_nonDet, Cond_True_nonDet, Cond_False_nonDet,
     Scope_nonDet, Map_nonDet, Mapvl_empty_nonDet, Mapvl_rec_nonDet,
     Fold_nonDet, Foldvl_empty_nonDet, Foldvl_rec_nonDet,
     Bconst_nonDet, Pred_nonDet, Cmp_nonDet, Cscope_nonDet] =
      CONJ_LIST 20 evalExpNonDet_rules;
save_thm ("Const_nonDet", Const_nonDet);
save_thm ("Var_nonDet", Var_nonDet);
save_thm ("Ith_nonDet", Ith_nonDet);
save_thm ("Unop_nonDet", Unop_nonDet);
save_thm ("Binop_nonDet", Binop_nonDet);
save_thm ("Terop_nonDet", Terop_nonDet);
save_thm ("Let_nonDet", Let_nonDet);
save_thm ("Cond_True_nonDet", Cond_True_nonDet);
save_thm ("Cond_False_nonDet", Cond_False_nonDet);
save_thm ("Scope_nonDet", Scope_nonDet);
save_thm ("Map_nonDet", Map_nonDet);
save_thm ("Mapvl_empty_nonDet", Mapvl_empty_nonDet);
save_thm ("Mapvl_rec_nonDet", Mapvl_rec_nonDet);
save_thm ("Fold_nonDet", Fold_nonDet);
save_thm ("Foldvl_empty_nonDet", Foldvl_empty_nonDet);
save_thm ("Foldvl_rec_nonDet", Foldvl_rec_nonDet);
save_thm ("Bconst_nonDet", Bconst_nonDet);
save_thm ("Pred_nonDet", Pred_nonDet);
save_thm ("Cmp_nonDet", Cmp_nonDet);
save_thm ("Cscope_nonDet", Cscope_nonDet);

(**
  This lemma should not break.
  Otherwise we would not mimic floating-point semantics
  Lemma 5.5 from the paper
**)
Theorem evalNonDet_deterministic
  `(! cfg E e v1.
    evalExpNonDet cfg E e v1 ==>
    ! v2.
    cfg.opts = [] ==>
    evalExpNonDet cfg E e v2 ==> v1 = v2)
  /\
  (! cfg E e v1.
    evalCexpNonDet cfg E e v1 ==>
    ! v2.
    cfg.opts = [] ==>
    evalCexpNonDet cfg E e v2 ==> v1 = v2)`
  (ho_match_mp_tac evalExpNonDet_ind
  \\ rpt conj_tac \\ rpt strip_tac \\ rveq
  \\ fs[Once evalExpNonDet_cases, tree2IEEE_def, rwAllValTree_def]
  \\ res_tac \\ fs[]
  \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ TRY (qpat_x_assum `cfg.opts = []` (fn thm => fs[thm]))
  \\ IMP_RES_TAC rwAllValTree_empty_rewrites
  \\ IMP_RES_TAC rwAllCvalTree_empty_rewrites
  \\ rveq \\ fs[]
  \\ res_tac \\ fs[] \\ rveq);

Theorem evalNonDet_cfg_det
  `(! cfg1 E e v1.
    evalExpNonDet cfg1 E e v1 ==>
    ! cfg2.
    cfg1.opts = [] /\
    cfg2.opts = [] ==>
    evalExpNonDet cfg2 E e v1)
  /\
  (! cfg1 E e v1.
    evalCexpNonDet cfg1 E e v1 ==>
    ! cfg2.
    cfg1.opts = [] /\
    cfg2.opts = [] ==>
    evalCexpNonDet cfg2 E e v1)`
  (ho_match_mp_tac evalExpNonDet_ind
  \\ rpt conj_tac \\ rpt strip_tac \\ rveq
  \\ fs[Once evalExpNonDet_cases, tree2IEEE_def, rwAllValTree_def]
  >- (res_tac \\ fs[] \\ find_exists_tac \\ fs[])
  \\ Cases_on `cfg1.canOpt` \\ fs[]
  \\ Cases_on `cfg2.canOpt` \\ fs[]
  \\ TRY (qpat_x_assum `cfg1.opts = []` (fn thm => fs[thm]))
  \\ IMP_RES_TAC rwAllValTree_empty_rewrites
  \\ IMP_RES_TAC rwAllCvalTree_empty_rewrites
  \\ rveq \\ fs[]
  \\ res_tac \\ rpt (find_exists_tac \\ fs[])
  \\ TRY (DISJ1_TAC \\ rpt (find_exists_tac \\ fs[]) \\ FAIL_TAC"")
  \\ TRY (DISJ2_TAC \\ rpt (find_exists_tac \\ fs[]) \\ FAIL_TAC"")
  \\ qexists_tac `v1` \\ fs[]);

Theorem evalNonDet_add_rewrite
  `(! cfg1 E e v.
    evalExpNonDet cfg1 E e v ==>
    !cfg2.
      set (cfg1.opts) SUBSET set (cfg2.opts) /\
      cfg1.canOpt = cfg2.canOpt ==>
      evalExpNonDet cfg2 E e v)
  /\
  (! cfg1 E e v.
    evalCexpNonDet cfg1 E e v ==>
      !cfg2.
        set (cfg1.opts) SUBSET set (cfg2.opts) /\
        cfg1.canOpt = cfg2.canOpt ==>
        evalCexpNonDet cfg2 E e v)`
  (ho_match_mp_tac evalExpNonDet_ind
  \\ rpt strip_tac \\ res_tac
  \\ fs[Once evalExpNonDet_cases, evalExpNonDet_rules, rewritesTo_def, crewritesTo_def,
        rwAllValTree_def, rwAllCvalTree_def]
  \\ Cases_on `cfg1.canOpt` \\ fs[]
  \\ TRY (optimize_tac)
  >- (DISJ1_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ1_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ2_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ2_TAC \\ asm_exists_tac \\ fs[])
  \\ rpt (asm_exists_tac \\ fs[]) \\ optimize_tac);

Theorem no_opt_gives_opt
  `(! cfg E e v.
     evalExpNonDet cfg E e v ==>
     evalExpNonDet (cfg with canOpt:= T) E e v)
  /\
  (!cfg E e v.
    evalCexpNonDet cfg E e v ==>
    evalCexpNonDet (cfg with canOpt:= T) E e v)`
  (ho_match_mp_tac evalExpNonDet_ind
  \\ rpt strip_tac \\ fs[evalExpNonDet_cases]
  \\ res_tac \\ rveq \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ rpt (asm_exists_tac \\ fs[])
  >- (DISJ1_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ1_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ2_TAC \\ asm_exists_tac \\ fs[])
  >- (DISJ2_TAC \\ asm_exists_tac \\ fs[])
  \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac \\ fs[]);

val empty_cfg_def = Define `
  empty_cfg = <| opts := []; canOpt := F |> `;

val (evalExpClocked_rules, evalExpClocked_ind, evalExpClocked_cases) = Hol_reln `
  (* Constants *)
  (! E v.
    evalExpClocked E (Syntax$Wconst v) 0n) /\
  (* Variables *)
  (! E n.
    (* E n = SOME r ==> *)
    evalExpClocked E (Syntax$Var n) 0) /\
  (* List projection *)
  (! E e n vl.
    evalExpNonDet empty_cfg E e (Lval vl) /\
    evalExpClocked E e ck1 ==>
    evalExpClocked E (Ith n e) (n * 3 + 3 + ck1)) /\
  (* Unary operations *)
  (! E e u ck.
    evalExpClocked E e ck ==>
    evalExpClocked E (Unop u e) ck) /\
  (* Binary operations *)
  (! E op e1 e2.
    evalExpClocked E e1 ck1 /\
    evalExpClocked E e2 ck2 ==>
    evalExpClocked E (Binop op e1 e2) (ck1 + ck2)) /\
  (* Ternary operations *)
  (! E op e1 e2 e3.
    evalExpClocked E e1 ck1 /\
    evalExpClocked E e2 ck2 /\
    evalExpClocked E e3 ck3 ==>
    evalExpClocked E (Terop op e1 e2 e3) (ck1 + ck2 + ck3)) /\
  (* Let bindings
    -> Do not restrict value as we want to be able to let bind map results *)
  (! E e1 e2 x ck1 ck2.
    evalExpClocked E e1 ck1 /\
    evalExpNonDet empty_cfg E e1 v1 /\
    evalExpClocked (bind E x v1) e2 ck2 ==>
    evalExpClocked E (Let x e1 e2) (ck1 + ck2)) /\
  (* Conditionals *)
  (! E e1 e2 ce ck1 ck3.
    evalExpClocked E e1 ck1 /\
    evalCexpNonDet empty_cfg E ce cv /\
    cTree2IEEE cv /\
    evalCexpClocked E ce ck3 ==>
    evalExpClocked E (Cond ce e1 e2) (ck3 + ck1)) /\
  (! E e1 e2 ce ck2 ck3.
    evalExpClocked E e2 ck2 /\
    evalCexpNonDet empty_cfg E ce cv /\
    ~ cTree2IEEE cv /\
    evalCexpClocked E ce ck3 ==>
    evalExpClocked E (Cond ce e1 e2) (ck3 + ck2)) /\
  (! E e ck.
    evalExpClocked E e ck ==>
    evalExpClocked E (Syntax$Scope Opt e) ck) /\
  (* Map applications;
    first go from Syntax to semantic in arg *)
  (! E x f e vl ck1 ck2.
    evalExpNonDet empty_cfg E e (Lval vl) /\
    evalExpClocked E e ck2 /\
    evalExpClockedIM E (Mapvl x f vl) ck1 ==>
    evalExpClocked E (Map x f e) (ck1 + ck2 + 2)) /\
  (! E x f vl ck1.
    evalExpClockedIM E (Mapvl x f vl) ck1 ==>
    evalExpClocked E (Mapvl x f vl) (ck1 + 2)) /\
  (* Foldl applications;
    first fo from Syntax to semantic arg *)
  (! E x y e i f vl.
    evalExpNonDet empty_cfg E e (Lval vl) /\
    evalExpClocked E e ck3 /\
    evalExpClocked E i ck2 /\
    evalExpClockedIM E (Foldvl (x,y) f i vl) ck1 ==>
    evalExpClocked E (Fold (x,y) f i e) (ck1 + ck2 + ck3 + 3)) /\
  (! E x y i f vl ck1 ck2.
    evalExpClocked E i ck2 /\
    evalExpClockedIM E (Foldvl (x,y) f i vl) ck1 ==>
    evalExpClocked E (Foldvl (x,y) f i vl) (ck1 + ck2 + 3))
  /\
  (! E.
    evalExpClocked E (List []) 0) /\
  (! E e1 els ck1 ck2.
    evalExpClocked E e1 ck1 /\
    evalExpClocked E (List els) ck2 ==>
    evalExpClocked E (List (e1::els)) (ck1 + ck2))
  /\
  (* Boolean constants *)
  (! E b. evalCexpClocked E (Bconst b) (0)) /\
  (* Predicates *)
  (! E e ck.
    evalExpClocked E e ck ==>
    evalCexpClocked E (Pred NaN e) (ck + ck)) /\
  (* Boolean comparisons *)
  (! E cmp e1 e2 .
    evalExpClocked E e1 ck1 /\
    evalExpClocked E e2 ck2 ==>
    evalCexpClocked E (Cmp cmp e1 e2) (ck1 + ck2)) /\
  (* Boolean scopes *)
  (! E e ck.
    evalCexpClocked E e ck ==>
    evalCexpClocked E (Cscope Opt e) ck)
  /\
  (* empty list case *)
  (! E x f.
    evalExpClockedIM E (Mapvl x f []) 3) /\
  (* Recursive map application + optimization *)
  (! E x f (ck1:num) v vl.
    evalExpClocked (bind E x (Val v)) f ck1 /\
    evalExpClockedIM E (Mapvl x f vl) ck2 ==> (* apply function to result *)
    evalExpClockedIM E (Mapvl x f (v::vl)) (ck1 + ck2 + 3)) /\
  (! E x y e f ck1.
    evalExpClocked E e ck1 ==>
    evalExpClockedIM E (Foldvl (x,y) f e []) (ck1 + 4)) /\
  (! E x y f es v vl ck1 ck2.
    evalExpClocked E es ck1 /\
    evalExpClockedIM E (Foldvl (x,y) f es vl) ck3 /\
    evalExpNonDet empty_cfg E (Foldvl (x,y) f es vl) v_rec /\
    evalExpClocked (bind (bind E y (Val v)) x v_rec) f ck2 ==>
    evalExpClockedIM E (Foldvl (x,y) f es (v::vl)) (ck1 + ck2 + ck3 + 5))`;

val evalExpClocked_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once evalExpClocked_cases])
    [``evalExpClocked E (Wconst n) res``,
     ``evalExpClocked E (Var x) res``,
     ``evalExpClocked E (List []) res``,
     ``evalExpClocked E (List (e::els)) res``,
     ``evalExpClocked E (Ith n e) res``,
     ``evalExpClocked E (Unop u e) res``,
     ``evalExpClocked E (Binop b e1 e2) res``,
     ``evalExpClocked E (Terop op e1 e2 e3) res``,
     ``evalExpClocked E (Let x e1 e2) res``,
     ``evalExpClocked E (Cond c e1 e2) res``,
     ``evalExpClocked E (Scope sc e) res``,
     ``evalExpClocked E (Map x f e) res``,
     ``evalExpClocked E (Mapvl x f vl) res``,
     ``evalExpClocked E (Fold (x,y) f i e) res``,
     ``evalExpClocked E (Foldvl (x,y) f e vl) res``,
     ``evalCexpClocked E (Bconst b) res``,
     ``evalCexpClocked E (Pred pr e) res``,
     ``evalCexpClocked E (Cmp cmp e1 e2) res``,
     ``evalCexpClocked E (Cscope sc e) res``,
     ``evalExpClockedIM E (Mapvl x f []) res``,
     ``evalExpClockedIM E (Mapvl x f (e::expl)) res``,
     ``evalExpClockedIM E (Foldvl (x,y) f e []) res``,
     ``evalExpClockedIM E (Foldvl (x,y) f e (es::expl)) res``
     ]
  |> LIST_CONJ |> curry save_thm "evalExpClocked_cases";

Theorem evalExpNonDet_is_clocked_map
  `! vl s e v.
    (! E v.
      evalExpNonDet <|opts := []; canOpt := F|> E e v ==>
      ? ck. evalExpClocked E e ck) /\
    evalExpNonDet empty_cfg E (Mapvl s e vl) v ==>
    ? ck1. evalExpClockedIM E (Mapvl s e vl) ck1`
  (Induct_on `vl`
  \\ rw[evalExpNonDet_cases, evalExpClocked_cases, empty_cfg_def] \\ res_tac
  \\ first_x_assum (qspecl_then [`Lval vrl`, `s`] destruct)
  \\ fs[empty_cfg_def]
  \\ rpt (find_exists_tac \\ fs[]));

Theorem evalExpNonDet_is_clocked_fold
  `! vl x y f i v E.
    (! E v.
      evalExpNonDet <|opts := []; canOpt := F|> E i v ==>
      ? ck. evalExpClocked E i ck) /\
    (! E v.
      evalExpNonDet <|opts := []; canOpt := F|> E f v ==>
      ? ck. evalExpClocked E f ck) /\
    evalExpNonDet empty_cfg E (Foldvl (x,y) f i vl) v ==>
    ? ck1. evalExpClockedIM E (Foldvl (x,y) f i vl) ck1`
  (Induct_on `vl`
  \\ rw[evalExpNonDet_cases, evalExpClocked_cases, empty_cfg_def] \\ res_tac
  >- (find_exists_tac \\ fs[])
  \\ pop_assum kall_tac
  \\ first_x_assum (qspecl_then [`y`, `x`, `v_rec`, `i`, `E`] destruct)
  \\ fs[empty_cfg_def]
  \\ first_x_assum destruct \\ fs[]
  \\ qexists_tac `ck1` \\ fs[]
  \\ qexists_tac `v_rec` \\ fs[]
  \\ `? v. evalExpNonDet empty_cfg E i v`
      by (qpat_x_assum `evalExpNonDet _ E (Foldvl _ _ _ _) _` mp_tac
          \\ rpt (pop_assum kall_tac)
          \\ qid_spec_tac `E` \\ qid_spec_tac `x` \\ qid_spec_tac `y`
          \\ qid_spec_tac `f` \\ qid_spec_tac `i` \\ qid_spec_tac `v_rec`
          \\ qid_spec_tac `vl`
          \\ Induct_on `vl` \\ rw[evalExpNonDet_cases] \\ res_tac
          \\ fs[empty_cfg_def] \\ find_exists_tac \\ fs[])
  \\ fs[empty_cfg_def]
  \\ res_tac \\ rpt (find_exists_tac \\ fs[]));

Theorem evalExpNonDet_is_clocked
  `(! e E v.
    evalExpNonDet empty_cfg E e v ==>
    ? ck. evalExpClocked E e ck) /\
  (! c E v.
    evalCexpNonDet empty_cfg E c v ==>
    ? ck. evalCexpClocked E c ck) /\
  (! (l:expr list) E v.
    evalExpNonDet empty_cfg E (List l) v ==>
    ? ck. evalExpClocked E (List l) ck)`
  (ho_match_mp_tac expr_induction
  \\ rw[evalExpNonDet_cases, evalExpClocked_cases, empty_cfg_def]
  \\ res_tac \\ rpt (find_exists_tac \\ fs[])
  >- (qexists_tac `ck + ck'` \\ DISJ1_TAC
      \\ qexistsl_tac [`cv`, `ck`, `ck'`] \\ fs[])
  >- (qexists_tac `ck + ck'` \\ DISJ2_TAC
      \\ qexistsl_tac [`cv`, `ck`, `ck'`] \\ fs[])
  >- (drule (CONJUNCT1 evalNonDet_cfg_det)
      \\ disch_then (qspec_then `<|opts :=[]; canOpt := F|>` destruct) \\ fs[]
      \\ res_tac \\ find_exists_tac \\ fs[])
  >- (irule evalExpNonDet_is_clocked_map
      \\ rpt conj_tac \\ fs[empty_cfg_def]
      \\ find_exists_tac \\ fs[])
  >- (irule evalExpNonDet_is_clocked_map
      \\ rpt conj_tac \\ fs[empty_cfg_def]
      \\ find_exists_tac \\ fs[])
  >- (Cases_on `p` \\ fs[evalExpNonDet_cases, evalExpClocked_cases]
      \\ res_tac
      \\ rename1 `Foldvl (x,y) f i vl`
      \\ `? v. evalExpNonDet empty_cfg E i v`
          by (qpat_x_assum `evalExpNonDet _ E (Foldvl _ _ _ _) _` mp_tac
              \\ rpt (pop_assum kall_tac)
              \\ qid_spec_tac `E` \\ qid_spec_tac `x` \\ qid_spec_tac `y`
              \\ qid_spec_tac `f` \\ qid_spec_tac `i` \\ qid_spec_tac `v`
              \\ qid_spec_tac `vl`
              \\ Induct_on `vl` \\ rw[evalExpNonDet_cases] \\ res_tac
              \\ fs[empty_cfg_def] \\ find_exists_tac \\ fs[])
      \\ assume_tac evalExpNonDet_is_clocked_fold \\ fs[empty_cfg_def]
      \\ qpat_x_assum `evalExpNonDet _ E (Foldvl _ _ _ _) _` resolve_tac
      \\ first_x_assum destruct
      >- (rpt conj_tac \\ fs[empty_cfg_def])
      \\ res_tac
      \\ rpt (find_exists_tac \\ fs[]))
  >- (Cases_on `p` \\ fs[evalExpNonDet_cases, evalExpClocked_cases]
      \\ rename1 `Foldvl (x,y) f i vl`
      \\ drule evalExpNonDet_is_clocked_fold
      \\ disch_then imp_res_tac
      \\ `? v. evalExpNonDet empty_cfg E i v`
          by (qpat_x_assum `evalExpNonDet _ E (Foldvl _ _ _ _) _` mp_tac
              \\ rpt (pop_assum kall_tac)
              \\ qid_spec_tac `E` \\ qid_spec_tac `x` \\ qid_spec_tac `y`
              \\ qid_spec_tac `f` \\ qid_spec_tac `i` \\ qid_spec_tac `v`
              \\ qid_spec_tac `vl`
              \\ Induct_on `vl` \\ rw[evalExpNonDet_cases] \\ res_tac
              \\ fs[empty_cfg_def] \\ find_exists_tac \\ fs[])
      \\ fs[empty_cfg_def] \\ res_tac
      \\ rpt (find_exists_tac \\ fs[]))
  \\ drule (CONJUNCT2 evalNonDet_cfg_det)
  \\ disch_then (qspec_then `<| opts:=[]; canOpt := F |>` destruct) \\ fs[]
  \\ res_tac \\ find_exists_tac \\ fs[]);

val _ = export_theory();
