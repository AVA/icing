(**
  This file contains the correctness proofs for optimizeGreedy, optimizeCond,
  and compileIEE754 defined in Section 3 and 4 of the paper and
  CompilerScript.sml.

  compileIEEE_no_rewrites, and compileIEEE754_det_map prove that IEEE754
  translator disallows optimizations and produces a deterministic expression

  compileGreedy_correct proves correctness of the greedy compiler for the
  rewrites described in the paper

  optimizeCond_correct_general gives a general correctness theorem for the
  conditional optimizer
**)
open SyntaxTheory SemanticsTheory simRelTheory patternTheory valueTreesTheory
    rewriteRulesTheory rewriteRulesProofsTheory CompilerTheory;
open patternTacticsLib CompilerTacticsLib;

open icingPreamble;

val _ = new_theory "CompilerProofs";

(**
  Greedy optimization is the same as using the conditional optimizer with only
  True as precondition
**)
Theorem greedy_is_optimize_uncond
  `! ls e. optimizeGreedy ls e = FST (optimizeCond (MAP (\x. CondExp (\ e. T) [x]) ls) e)`
  (Induct_on `ls`
  >- (fs[optimizeGreedy_def, optimizeCond_def])
  \\ rpt strip_tac
  \\ fs[optimizeCond_def, optimizeGreedy_def]
  \\ Cases_on `optimizeCond (MAP (\x. CondExp (\e. T) [x]) ls) e` \\ fs[]
  \\ PairCases_on `r` \\ fs[]
  \\ assume_tac (ETA_CONV ``\x. rewriteExp [h] x``) \\ fs[])

(**
  Infrastructural lemmas for correctness of compile greedy
**)
Theorem bottomUpApp_empty_id
  `(! e. bottomUpExp (rewriteExp []) (rewriteCexp []) e = e) /\
   (! e. bottomUpCexp (rewriteExp []) (rewriteCexp []) e = e) /\
   (! l. bottomUpExpL (rewriteExp []) (rewriteCexp []) l = l)`
  (Induct \\ rpt strip_tac \\ fs[bottomUpExp_def, rewriteExp_def, rewriteCexp_def]
  \\ (Cases_on `s` ORELSE Cases_on `p`)
  \\ fs[bottomUpExp_def, rewriteExp_def, rewriteCexp_def]);

Theorem rwAllValTree_noOpt[simp]
  `! v. ? r insts. rwAllValTree insts T [] v = SOME r`
  (rpt strip_tac \\ qexistsl_tac [`v`, `[]`] \\ EVAL_TAC);

val simp_tac =
  res_tac
  \\ fs[crewritesTo_def, rewritesTo_def, tree2IEEE_def, Once valEq_lval, Once valEq_val]
  \\ rveq \\ fs[];

Theorem simEnv_sim_val
  `(! cfg E1 e1 v1.
    evalExpNonDet cfg E1 e1 v1 ==>
    ! e2 E2. cfg.opts = [] /\ simEnv E1 E2 /\ simExp e1 e2 ==>
      (? v2. evalExpNonDet cfg E2 e2 v2) /\
      (! v2.
        evalExpNonDet cfg E2 e2 v2 ==>
        valEq v1 v2)) /\
  (! cfg E1 ce1 cv1.
    evalCexpNonDet cfg E1 ce1 cv1 ==>
      ! ce2 E2. cfg.opts = [] /\ simEnv E1 E2 /\ simCexp ce1 ce2 ==>
      (? cv2. evalCexpNonDet cfg E2 ce2 cv2) /\
      (! cv2.
        evalCexpNonDet cfg E2 ce2 cv2 ==>
        cTree2IEEE cv1  = cTree2IEEE cv2))`
  (ho_match_mp_tac evalExpNonDet_ind
  \\ rpt strip_tac \\ fs[simEnv_def, Once simExp_cases]
  \\ rveq \\ fs[evalExpNonDet_cases] \\ rveq
  >- (res_tac \\ fs[])
  >- (res_tac \\ fs[] \\ rveq \\ fs[])
  >- (simp_tac \\ asm_exists_tac \\ fs[]
      \\ imp_res_tac valEq_lval_val_eq
      \\ imp_res_tac valEq_lval_length \\ fs[])
  >- (simp_tac \\ fs[Once valEq_lval] \\ rveq
      \\ imp_res_tac valEq_lval_val_eq \\ fs[] \\ rveq \\ fs[])
  >- (simp_tac \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ asm_exists_tac \\ fs[])
  >- (simp_tac \\ qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `cfg.canOpt`
      \\ fs[]  \\ imp_res_tac rwAllValTree_empty_rewrites
      \\ rveq \\ fs[valEq_def, tree2IEEE_def])
  >- (simp_tac \\ ntac 2 (asm_exists_tac \\ fs[])
      \\ Cases_on `cfg.canOpt` \\ fs[])
  >- (simp_tac \\ qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `cfg.canOpt`
      \\ fs[]  \\ imp_res_tac rwAllValTree_empty_rewrites
      \\ rveq \\ fs[valEq_def, tree2IEEE_def])
  >- (simp_tac \\ ntac 3 (asm_exists_tac \\ fs[])
      \\ Cases_on `cfg.canOpt` \\ fs[])
  >- (simp_tac \\ qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `cfg.canOpt`
      \\ fs[]  \\ imp_res_tac rwAllValTree_empty_rewrites
      \\ rveq \\ fs[valEq_def, tree2IEEE_def])
  >- (simp_tac \\ asm_exists_tac \\ fs[]
      \\ first_x_assum (qspec_then `bind E2 x v2` destruct)
      >- (rpt strip_tac \\ fs[bind_def]
          \\ TOP_CASE_TAC \\ fs[])
      \\ first_x_assum (qspec_then `bind E2 x v2'` destruct)
      >- (rpt strip_tac \\ fs[bind_def]
          \\ TOP_CASE_TAC \\ fs[])
      \\ asm_exists_tac \\ fs[])
  >- (simp_tac \\ first_x_assum (qspec_then `bind E2 x v1''` destruct)
      >- (rpt strip_tac \\ fs[bind_def]
          \\ TOP_CASE_TAC \\ fs[])
      \\ res_tac)
  >- (simp_tac \\ qexists_tac `v2` \\ DISJ1_TAC
      \\ rpt (asm_exists_tac \\ fs[]))
  >- (simp_tac)
  >- (simp_tac)
  >- (simp_tac \\ qexists_tac `v2` \\ DISJ2_TAC
      \\ rpt (asm_exists_tac \\ fs[]))
  >- (simp_tac)
  >- (simp_tac)
  >- (simp_tac \\ irule valEq_wrap \\ fs[])
  >- (simp_tac \\ ntac 2 (asm_exists_tac \\ fs[]))
  >- (simp_tac \\ res_tac)
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases])
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases])
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases]
      \\ fs[GSYM PULL_EXISTS] \\ conj_tac
      >- (last_x_assum (qspecl_then [`g`, `bind E2 x (Val h)`] destruct)
          >- (fs[] \\ rpt strip_tac
              \\ fs[bind_def] \\ TOP_CASE_TAC \\ fs[]
              \\ rveq \\ fs[valEq_def])
          \\ first_x_assum (qspec_then `v2` destruct) \\ fs[Once valEq_val]
          \\ rveq \\ asm_exists_tac \\ fs[])
      \\ first_x_assum (qspecl_then [`Mapvl x g t`, `E2`] destruct)
      \\ fs[]
      \\ first_x_assum (qspec_then `v2` destruct) \\ fs[Once valEq_lval]
      \\ rveq \\ asm_exists_tac \\ fs[])
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases]
      \\ rveq \\ fs[]
      \\ first_x_assum (qspecl_then [`Mapvl x g t`, `E2`] destruct)
      \\ fs[]
      \\ first_x_assum (qspecl_then [`g`, `bind E2 x (Val h)`] destruct)
      >- (fs[] \\ rpt strip_tac \\ fs[bind_def] \\ TOP_CASE_TAC \\ fs[]
          \\ rveq \\ fs[valEq_def])
      \\ first_x_assum (qspec_then `Val v_res'` destruct)
      \\ fs[Once valEq_val] \\ rveq \\ fs[valEq_def])
  >- (simp_tac \\ ntac 2 (asm_exists_tac \\ fs[]))
  >- (simp_tac \\ res_tac)
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases])
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases]
      \\ res_tac)
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases]
      \\ last_x_assum (qspecl_then [`Foldvl (x,y) g i2 t`, `E2`] destruct)
      \\ fs[] \\ asm_exists_tac \\ fs[]
      \\ last_x_assum (qspecl_then [`g`, `bind (bind E2 y (Val h)) x v2`] destruct)
      >- (fs[] \\ rpt strip_tac
          \\ fs[bind_def] \\ TOP_CASE_TAC \\ fs[]
          \\ TOP_CASE_TAC \\ fs[valEq_def] \\ rveq \\ fs[valEq_def])
      \\ first_x_assum (qspec_then `v2'` destruct) \\ fs[Once valEq_val]
      \\ asm_exists_tac \\ fs[])
  >- (Cases_on `vl2` \\ fs[valEq_def, evalExpNonDet_cases]
      \\ rveq \\ fs[]
      \\ last_x_assum (qspecl_then [`Foldvl (x,y) g i2 t`, `E2`] destruct)
      \\ fs[]
      \\ last_x_assum (qspecl_then [`g`, `bind (bind E2 y (Val h)) x v_rec`] destruct)
      >- (fs[] \\ rpt strip_tac
          \\ fs[bind_def] \\ TOP_CASE_TAC \\ fs[]
          \\ TOP_CASE_TAC \\ fs[] \\ rveq \\ fs[valEq_def])
      \\ first_x_assum (qspec_then `v2` destruct)
      \\ fs[Once valEq_val] \\ rveq \\ fs[valEq_def])
  >- (Cases_on `l2` \\ fs[simExp_cases] \\ fs[evalExpNonDet_cases])
  >- (Cases_on `l2` \\ fs[simExp_cases] \\ fs[evalExpNonDet_cases])
  >- (Cases_on `l2` \\ fs[simExp_cases] \\ res_tac
      \\ fs[evalExpNonDet_cases]
      \\ first_x_assum (qspec_then `t` destruct) \\ fs[]
      \\ first_x_assum (qspec_then `v2''` destruct) \\ fs[]
      \\ fs[Once valEq_lval, Once valEq_val] \\ rveq
      \\ rpt (find_exists_tac \\ fs[]))
  >- (qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `l2` \\ fs[simExp_cases]
      \\ qpat_x_assum `simExp _ _` resolve_tac
      \\ first_x_assum (qspec_then `E2` destruct) \\ fs[]
      \\ fs[evalExpNonDet_cases] \\ rveq
      \\ qpat_x_assum `evalExpNonDet cfg E2 h (Val _)` resolve_tac
      \\ fs[valEq_def]
      \\ first_x_assum (qspecl_then [`List t`, `E2`] destruct)
      \\ fs[])
  >- (simp_tac \\ asm_exists_tac \\ fs[]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ qexistsl_tac [`Pred NaN v2'''`, `[]`] \\ EVAL_TAC)
  >- (qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ imp_res_tac rwAllCvalTree_empty_rewrites \\ rveq
      \\ res_tac \\ fs[Once valEq_val, tree2IEEE_def] \\ rveq \\ fs[valEq_def])
  >- (simp_tac \\ ntac 2 (asm_exists_tac \\ fs[])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ qexistsl_tac [`Cmp cmp v2'''''''' v2''''''`, `[]`] \\ EVAL_TAC)
  >- (qpat_x_assum `cfg.opts = []` (fn thm => fs[thm])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ imp_res_tac rwAllCvalTree_empty_rewrites \\ rveq
      \\ res_tac \\ fs[Once valEq_val, tree2IEEE_def] \\ rveq \\ fs[valEq_def])
  \\ simp_tac);

val solve_tac =
  fs[tree2IEEE_def] \\ Cases_on `cfg.canOpt` \\ fs[]
  \\ rpt (asm_exists_tac \\ fs[crewritesTo_def, rewritesTo_def]);

(**
  Infrastructural lemmas for compileIEEE754
**)
Theorem compileIEEE754_no_rewrites_map
  `! cfg E s e vl v vl2.
    evalExpNonDet cfg E (Mapvl s (compileIEEE754 e) vl) v /\
    ~ cfg.canOpt /\
    valEq (Lval vl) (Lval vl2) /\
    (! cfg E v.
      evalExpNonDet cfg E (compileIEEE754 e) v ⇒
      ~ cfg.canOpt ⇒
      ? v1.
        evalExpNonDet (cfg with opts := []) E e v1 ∧ valEq v1 v) ==>
    ? v2.
      evalExpNonDet (cfg with opts := []) E (Mapvl s e vl2) v2 /\ valEq v v2`
  (Induct_on `vl` \\ rpt strip_tac
  \\ imp_res_tac valEq_lval_length \\ Cases_on `vl2` \\ fs[valEq_def]
  \\ fs[evalExpNonDet_cases] \\ rveq
  \\ qpat_x_assum `evalExpNonDet cfg E _ (Lval _)`
      (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
  \\ qpat_assum `valEq (Lval _) (Lval _)`
      (fn thm => first_x_assum (fn ithm => mp_then Any destruct ithm thm))
  \\ fs[]
  \\ qpat_x_assum `evalExpNonDet cfg (bind _ _ _) _ _`
      (fn thm => first_x_assum (fn ithm => mp_then Any destruct ithm thm))
  \\ fs[Once valEq_sym, Once valEq_lval, Once valEq_val] \\ rveq
  \\ rename1 `valEq (Val v_res) (Val v_res2)`
  \\ qspecl_then [`cfg with opts := []`, `bind E s (Val h)`, `e`, `Val v_res2`]
      destruct (fth simEnv_sim_val) \\ fs[]
  \\ first_x_assum (qspecl_then [`e`, `bind E s (Val h')`] destruct)
  >- (fs[simEnv_def, simExp_refl, bind_def] \\ rpt strip_tac
      \\ Cases_on `x = s` \\ fs[] \\ rveq \\ fs[valEq_def])
  \\ first_x_assum (qspec_then `v2` destruct)
  \\ fs[Once valEq_val] \\ rveq
  \\ qexists_tac `Lval (v2'::vl2)` \\ fs[valEq_def]);

Theorem compileIEEE754_no_rewrites_fold
  `! cfg E vl v vl2.
    evalExpNonDet cfg E (Foldvl (x,y) (compileIEEE754 f) (compileIEEE754 i) vl) v /\
    ~ cfg.canOpt /\
    valEq (Lval vl) (Lval vl2) /\
    (! cfg E v.
      evalExpNonDet cfg E (compileIEEE754 i) v ⇒
      ~ cfg.canOpt ⇒
      ? v1.
        evalExpNonDet (cfg with opts := []) E i v1 ∧ valEq v1 v) ==>
    (! cfg E v.
      evalExpNonDet cfg E (compileIEEE754 f) v ⇒
      ~ cfg.canOpt ⇒
      ? v1.
        evalExpNonDet (cfg with opts := []) E f v1 ∧ valEq v1 v) ==>
    ? v2.
      evalExpNonDet (cfg with opts := []) E (Foldvl (x,y) f i vl2) v2 /\ valEq v v2`
  (Induct_on `vl` \\ rpt strip_tac
  \\ imp_res_tac valEq_lval_length \\ Cases_on `vl2` \\ fs[valEq_def]
  \\ fs[evalExpNonDet_cases] \\ rveq
  >- (res_tac \\ find_exists_tac \\ fs[Once valEq_sym])
  \\ qpat_x_assum `evalExpNonDet cfg E _ v_rec`
      (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
  \\ qpat_assum `valEq (Lval _) (Lval _)`
      (fn thm => first_x_assum (fn ithm => mp_then Any destruct ithm thm))
  \\ fs[]
  \\ qpat_x_assum `evalExpNonDet cfg (bind _ _ _) _ _`
      (fn thm => first_x_assum (fn ithm => mp_then Any destruct ithm thm))
  \\ fs[Once valEq_sym, Once valEq_lval, Once valEq_val] \\ rveq
  \\ qspecl_then [`cfg with opts := []`, `bind (bind E y (Val h)) x v_rec`, `f`, `v1`]
      destruct (fth simEnv_sim_val) \\ fs[]
  \\ first_x_assum (qspecl_then [`f`, `bind (bind E y (Val h')) x v2`] destruct)
  >- (fs[simEnv_def, simExp_refl, bind_def] \\ rpt strip_tac
      \\ Cases_on `x' = x` \\ fs[] \\ rveq \\ fs[valEq_def, Once valEq_sym]
      \\ Cases_on `x' = y` \\ fs[] \\ rveq \\ fs[valEq_def, Once valEq_sym])
  \\ first_x_assum (qspec_then `v2'` destruct)
  \\ fs[Once valEq_val] \\ rveq
  \\ qexists_tac `v2'` \\ fs[valEq_def] \\ conj_tac
  >- (find_exists_tac \\ fs[])
  \\ irule valEq_trans \\ qexists_tac `v1` \\ fs[Once valEq_sym]);

(**
  Simulation proofs for compileIEEE754

  First show that compileIEEE754 disallows optimizations for an arbitrary
  context
  Assumption ~ cfg.canOpt is necessary because otherwise we would be operating
  below an Opt node, meaning that compileIEEE754 was not applied to the top AST
  node but somewhere in between
**)
Theorem compileIEEE_no_rewrites
  `(! e cfg E v.
    evalExpNonDet cfg E (compileIEEE754 e) v ==>
    ~ cfg.canOpt ==>
    ?v1.
      evalExpNonDet (cfg with opts := []) E e v1 /\
      valEq v1 v) /\
  (! ce cfg E cv.
    evalCexpNonDet cfg E (compileIEEE754c ce) cv ==>
    ~ cfg.canOpt ==>
    ? cv1.
      evalCexpNonDet (cfg with opts := []) E ce cv1 /\
      valEq (Cval cv1) (Cval cv)) /\
  (! l cfg E v.
    evalExpNonDet cfg E (compileIEEE754 (List l)) v ==>
    ~ cfg.canOpt ==>
    ? v1. evalExpNonDet (cfg with opts := []) E (List l) v1 /\
      valEq v1 v)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac \\ fs[compileIEEE754_def, evalExpNonDet_cases] \\ rpt strip_tac
  \\ fs[tree2IEEE_def] \\ rveq \\ res_tac \\ fs[]
  >- (pop_assum (fn thm => assume_tac (ONCE_REWRITE_RULE [valEq_sym]thm ))
      \\ fs[Once valEq_lval] \\ rveq \\ imp_res_tac valEq_lval_length
      \\ imp_res_tac valEq_lval_val_eq \\ once_rewrite_tac [valEq_sym]
      \\ once_rewrite_tac [CONJ_COMM] \\ asm_exists_tac \\ fs[]
      \\ asm_exists_tac \\ fs[])
  >- (fs[Once valEq_sym, Once valEq_val] \\ rveq
      \\ qexists_tac `Val (UnVal u v2)` \\ conj_tac >- solve_tac
      \\ fs[valEq_def, tree2IEEE_def])
  >- (fs[Once valEq_sym, Once valEq_val] \\ rveq
      \\ rename1 `valEq (Val v1) (Val v3)` \\ rename1 `valEq (Val v2) (Val v4)`
      \\ qexists_tac `Val (BinVal b v3 v4)` \\ fs[] \\ solve_tac
      \\ fs[valEq_def, tree2IEEE_def])
  >- (fs[Once valEq_sym, Once valEq_val] \\ rveq
      \\ rename1 `valEq (Val v1) (Val v4)` \\ rename1 `valEq (Val v2) (Val v5)`
      \\ rename1 `valEq (Val v3) (Val v6)`
      \\ qexists_tac `Val (TerVal t v4 v5 v6)` \\ solve_tac
      \\ fs[valEq_def, tree2IEEE_def])
  >- (fs[PULL_EXISTS, tree2IEEE_def]
      \\ rename1 `valEq vBound1 v1`
      \\ qspecl_then [`cfg with opts:=[]`,`bind E s v1`, `e'`, `v1'`]
          destruct (hd (CONJ_LIST 2 simEnv_sim_val))
      \\ fs[]
      \\ first_x_assum (qspecl_then [`e'`, `bind E s vBound1`] destruct)
      >- (fs[simEnv_def, bind_def] \\ rpt strip_tac
          \\ Cases_on `x = s` \\ fs[]
          \\ rveq \\ fs[Once valEq_sym])
      \\ first_x_assum (qspec_then `v2` destruct) \\ fs[]
      \\ rpt (find_exists_tac \\ fs[])
      \\ irule valEq_trans
      \\ qexists_tac `v1'` \\ fs[Once valEq_sym])
  >- (qexists_tac `v1` \\ fs[tree2IEEE_def] \\ DISJ1_TAC
      \\ asm_exists_tac \\ fs[valEq_def])
  >- (qexists_tac `v1` \\ fs[tree2IEEE_def] \\ DISJ2_TAC
      \\ asm_exists_tac \\ fs[valEq_def])
  >- (Cases_on `s` \\ fs[compileIEEE754_def] \\ res_tac
      \\ imp_res_tac no_opt_gives_opt
      \\ qexists_tac `wrapScope v1` \\ conj_tac
      >- (Cases_on `cfg` \\ fs[] \\ find_exists_tac \\ fs[])
      \\ irule valEq_wrap_left \\ fs[])
  >- (fs[PULL_EXISTS, Once valEq_sym, Once valEq_lval]
      \\ rveq \\ fs[Once valEq_sym]
      \\ qpat_x_assum `evalExpNonDet _ _ (Mapvl _ _ _) _`
          (mp_then Any assume_tac compileIEEE754_no_rewrites_map)
      \\ first_x_assum (qspec_then `vl2` destruct) \\ fs[Once valEq_sym]
      \\ rpt (find_exists_tac \\ fs[Once valEq_sym]))
  >- (qpat_x_assum `evalExpNonDet _ _ (Mapvl _ _ _) _`
        (mp_then Any assume_tac compileIEEE754_no_rewrites_map)
      \\ first_x_assum (qspec_then `l` destruct) \\ fs[]
      \\ find_exists_tac \\ fs[Once valEq_sym])
  >- (Cases_on `p` \\ fs[evalExpNonDet_cases]
      \\ fs[PULL_EXISTS] \\ res_tac
      \\ fs[Once valEq_sym, Once valEq_lval] \\ rveq
      \\ qpat_x_assum `evalExpNonDet _ _ (Foldvl _ _ _ _) _`
          (mp_then Any assume_tac compileIEEE754_no_rewrites_fold)
      \\ first_x_assum (qspec_then `vl2` destruct) \\ fs[Once valEq_sym]
      \\ rpt (find_exists_tac \\ fs[Once valEq_sym]))
  >- (PairCases_on `p` \\ qpat_x_assum `evalExpNonDet _ _ (Foldvl _ _ _ _) _`
        (mp_then Any assume_tac compileIEEE754_no_rewrites_fold)
      \\ first_x_assum (qspec_then `l` destruct) \\ fs[]
      \\ find_exists_tac \\ fs[Once valEq_sym])
  >- (fs[Once valEq_sym, Once valEq_val] \\ rveq
      \\ qexists_tac `Pred NaN v2` \\ solve_tac
      \\ fs[valEq_def, tree2IEEE_def])
  >- (fs[Once valEq_sym, Once valEq_val] \\ rveq
      \\ rename1 `valEq (Val v1) (Val v1o)`
      \\ rename1 `valEq (Val v2) (Val v2o)`
      \\ qexists_tac `Cmp ce v1o v2o` \\ solve_tac
      \\ fs[valEq_def, tree2IEEE_def])
  >- (Cases_on `s` \\ fs[compileIEEE754_def]
      \\ res_tac
      \\ qexists_tac `Cscope Opt cv1` \\ fs[tree2IEEE_def]
      \\ imp_res_tac no_opt_gives_opt \\ Cases_on `cfg` \\ fs[valEq_def, tree2IEEE_def])
  \\ fs[Once valEq_sym, Once valEq_lval, Once valEq_val] \\ rveq
  \\ qexists_tac `Lval (v2::vl2)` \\ fs[valEq_def]);

(**
  More infrastructural lemmas
**)
Theorem compileIEEE754_det_map
  `! cfg1 E s e vl v.
    evalExpNonDet cfg1 E (Mapvl s (compileIEEE754 e) vl) v /\
    ~ cfg1.canOpt /\
    (! v E.
      evalExpNonDet cfg1 E (compileIEEE754 e) v ==>
      ! cfg2.
        ~ cfg2.canOpt ==>
        evalExpNonDet cfg2 E (compileIEEE754 e) v) ==>
  ! cfg2.
    ~ cfg2.canOpt ==>
    evalExpNonDet cfg2 E (Mapvl s (compileIEEE754 e) vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ fs[evalExpNonDet_cases] \\ res_tac \\ fs[]);

Theorem compileIEEE754_det_fold
  `! cfg1 E s e vl v.
    evalExpNonDet cfg1 E (Foldvl (x,y) (compileIEEE754 f) (compileIEEE754 i) vl) v /\
    ~ cfg1.canOpt /\
    (! v E.
      evalExpNonDet cfg1 E (compileIEEE754 f) v ==>
      ! cfg2.
        ~ cfg2.canOpt ==>
        evalExpNonDet cfg2 E (compileIEEE754 f) v) /\
    (! v E.
      evalExpNonDet cfg1 E (compileIEEE754 i) v ==>
      ! cfg2.
        ~ cfg2.canOpt ==>
        evalExpNonDet cfg2 E (compileIEEE754 i) v) ==>
  ! cfg2.
    ~ cfg2.canOpt ==>
    evalExpNonDet cfg2 E (Foldvl (x,y) (compileIEEE754 f) (compileIEEE754 i) vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ fs[evalExpNonDet_cases] \\ res_tac \\ fs[]
  \\ find_exists_tac \\ fs[]);

(**
  Theorem showing that compileIEEE754 is deterministic
**)
local
  val solve_tac =
  (fs[compileIEEE754_def, evalExpNonDet_cases, rewritesTo_def, crewritesTo_def]
  \\ rveq \\ res_tac \\ fs[]
  \\ Cases_on `cfg1.canOpt` \\ fs[] \\ rveq \\ fs[]
  \\ res_tac
  \\ rpt (find_exists_tac \\ fs[]));
in
Theorem compileIEEE754_det
  `(! e cfg1 E v.
    ~ cfg1.canOpt /\
    evalExpNonDet cfg1 E (compileIEEE754 e) v ==>
    ! cfg2.
      ~ cfg2.canOpt ==>
      evalExpNonDet cfg2 E (compileIEEE754 e) v)
  /\
  (! ce cfg1 E cv.
    ~ cfg1.canOpt /\
    evalCexpNonDet cfg1 E (compileIEEE754c ce) cv ==>
    ! cfg2.
      ~ cfg2.canOpt ==>
      evalCexpNonDet cfg2 E (compileIEEE754c ce) cv)
  /\
  (! l cfg1 E v.
    ~ cfg1.canOpt /\
    evalExpNonDet cfg1 E (compileIEEE754 (List l)) v ==>
    ! cfg2.
      ~ cfg2.canOpt ==>
      evalExpNonDet cfg2 E (compileIEEE754 (List l)) v)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac
  \\ TRY (Cases_on `p`)
  \\ solve_tac
  >- (DISJ1_TAC \\ once_rewrite_tac[CONJ_COMM] \\ asm_exists_tac \\ fs[]
      \\ res_tac \\ fs[] \\ first_x_assum (qspec_then `cfg2` assume_tac) \\ fs[])
  >- (DISJ2_TAC \\ once_rewrite_tac[CONJ_COMM] \\ asm_exists_tac \\ fs[]
      \\ res_tac \\ fs[] \\ first_x_assum (qspec_then `cfg2` assume_tac) \\ fs[])
  >- (Cases_on `s` \\ solve_tac)
  >- (irule compileIEEE754_det_map \\ fs[] \\ find_exists_tac \\ fs[])
  >- (irule compileIEEE754_det_map \\ fs[] \\ find_exists_tac \\ fs[])
  >- (irule compileIEEE754_det_fold \\ fs[] \\ find_exists_tac \\ fs[])
  >- (irule compileIEEE754_det_fold \\ fs[] \\ find_exists_tac \\ fs[])
  \\ Cases_on `s` \\ solve_tac)
end;

(**
  Key lemma for correctness of the greedy optimizer.
  We can reduce correctness of rewriting for a list of optimizations
  to correctness of a single optimization
**)
Theorem rewriteExp_compositional
  `!e E v rw rws cfg.
    (* Correctness of single rewrite *)
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp [rw] e) v ==>
      evalExpNonDet (cfg with opts := rw :: cfg.opts) E e v) /\
    (* recursive correctness *)
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp rws e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v) /\
    cfg.canOpt /\
    evalExpNonDet cfg E (rewriteExp (rw::rws) e) v ==>
    evalExpNonDet (cfg with opts := APPEND cfg.opts (rw::rws)) E e v`
  (rpt strip_tac \\ PairCases_on `rw` \\ fs[rewriteExp_def]
  \\ Cases_on `matchesExpr rw0 e FEMPTY` \\ fs[]
  >- (res_tac
      \\ irule (hd (CONJ_LIST 2 evalNonDet_add_rewrite))
      \\ fs[]
      \\ qexists_tac `cfg with opts := cfg.opts ++ rws` \\ fs[SUBSET_DEF]
      \\ rpt strip_tac \\ fs[])
  \\ Cases_on `appExpr rw1 x` \\ fs[]
  >- (res_tac
        \\ irule (hd (CONJ_LIST 2 evalNonDet_add_rewrite))
        \\ fs[]
        \\ qexists_tac `cfg with opts := cfg.opts ++ rws` \\ fs[SUBSET_DEF]
        \\ rpt strip_tac \\ fs[])
  \\ rename1 `rewriteExp rws eopt`
  \\ res_tac
  \\ last_x_assum (qspecl_then [`e`, `E`, `v`, `cfg with opts := cfg.opts ++ rws`] assume_tac)
  \\ qpat_x_assum `matchesExpr _ _ _ = SOME _` (fn thm => fs[thm] \\ assume_tac thm)
  \\ qpat_x_assum `appExpr _ _ = SOME _` (fn thm => fs[thm] \\ assume_tac thm)
  \\ res_tac
  \\ irule (hd (CONJ_LIST 2 evalNonDet_add_rewrite))
  \\ fs[]
  \\ qexists_tac `cfg with opts := (rw0,rw1) :: (cfg.opts ++ rws)` \\ fs[SUBSET_DEF]
  \\ rpt strip_tac \\ fs[]);

Theorem cfg_component_equality
  `! cfg1 cfg2.
    cfg1 = cfg2 <=> ((cfg1.canOpt <=> cfg2.canOpt) /\ cfg1.opts = cfg2.opts)`
  (rpt strip_tac \\ Cases_on `cfg1` \\ Cases_on `cfg2` \\ fs[]
  \\ metis_tac []);

Theorem empty_rw_correct
  `!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (rewriteExp [] e) v ==>
      evalExpNonDet (cfg with opts := cfg.opts ++ []) E e v`
  (rpt strip_tac \\ fs[rewriteExp_def]
  \\ `cfg = cfg with opts := cfg.opts` by fs[cfg_component_equality]
  \\ first_x_assum (fn thm => fs[GSYM thm]));

Theorem bottomUpApp_map_correct
`! cfg rws1 E s f g e vl v rws2.
  (∀e E v cfg.
       cfg.canOpt ∧ evalExpNonDet cfg E (f e) v ⇒
       evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
  (∀e E v cfg.
       cfg.canOpt ∧ evalCexpNonDet cfg E (g e) v ⇒
       evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
  (! E v cfg rws1 rws2 f g.
    (∀e E v cfg.
         cfg.canOpt ∧ evalExpNonDet cfg E (f e) v ⇒
         evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
    (∀e E v cfg.
         cfg.canOpt ∧ evalCexpNonDet cfg E (g e) v ⇒
         evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
    cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
    evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1 ⧺ rws2) E e v) /\
    cfg.canOpt /\
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1) E (Mapvl s (bottomUpExp f g e) vl) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (Mapvl s e vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ fs[evalExpNonDet_cases] \\ res_tac \\ fs[]
  \\ rveq \\ first_x_assum destruct \\ fs[]
  \\ first_x_assum (qspec_then `rws2` destruct) \\ fs[]
  \\ first_x_assum (qspec_then `rws1` destruct) \\ fs[]
  \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
  \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[]);

Theorem bottomUpApp_fold_correct
`! cfg rws1 E s f g e vl v rws2 i.
  (∀e E v cfg.
       cfg.canOpt ∧ evalExpNonDet cfg E (f e) v ⇒
       evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
  (∀e E v cfg.
       cfg.canOpt ∧ evalCexpNonDet cfg E (g e) v ⇒
       evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
  (! E v cfg rws1 rws2 f g.
    (∀e E v cfg.
         cfg.canOpt ∧ evalExpNonDet cfg E (f e) v ⇒
         evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
    (∀e E v cfg.
         cfg.canOpt ∧ evalCexpNonDet cfg E (g e) v ⇒
         evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
    cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g i) v ⇒
    evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1 ⧺ rws2) E i v) /\
  (! E v cfg rws1 rws2 f g.
    (∀e E v cfg.
         cfg.canOpt ∧ evalExpNonDet cfg E (f e) v ⇒
         evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
    (∀e E v cfg.
         cfg.canOpt ∧ evalCexpNonDet cfg E (g e) v ⇒
         evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
    cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
    evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1 ⧺ rws2) E e v) /\
    cfg.canOpt /\
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1) E (Foldvl s (bottomUpExp f g e) (bottomUpExp f g i) vl) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (Foldvl s e i vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ PairCases_on `s`
  \\ fs[evalExpNonDet_cases]
  >- (qpat_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  \\ qpat_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
      (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
  \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
  \\ qpat_assum `evalExpNonDet _ _ (Foldvl _ _ _ _) _`
      (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
  \\ first_x_assum (qspec_then `rws2` destruct) \\ fs[]
  >- (conj_tac \\ fs[])
  \\ find_exists_tac \\ conj_tac \\ fs[]
  \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
  \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[]);

Theorem bottomUpApp_correct
  `(! e E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (f e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    cfg.canOpt /\
    evalExpNonDet cfg E (bottomUpExp f g e) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v) /\
  (! e E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (f e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    cfg.canOpt /\
    evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
    evalCexpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! (l:expr list) E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (f e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    cfg.canOpt /\
    evalExpNonDet cfg E (Syntax$List (bottomUpExpL f g l)) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (Syntax$List l) v)`
  (ho_match_mp_tac expr_induction  \\ rpt strip_tac \\ fs[bottomUpExp_def]
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspec_then `rws` destruct) \\ fs[]
      \\ qexists_tac `v'` \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspec_then `rws` destruct) \\ fs[]
      \\ qexists_tac `v'` \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (List (bottomUpExpL _ _ _)) _` resolve_tac
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct)
      \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ qexists_tac `vl` \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ qexists_tac `v'` \\ fs[]
      \\ Cases_on `cfg.canOpt` \\ fs[rewritesTo_def] \\ conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ ntac 2
          (qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
            (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm)))
      \\ rpt (first_x_assum (qspec_then `rws1` destruct) \\ fs[])
      \\ qexistsl_tac [`v1`, `v2`] \\ fs[]
      \\ Cases_on `cfg.canOpt` \\ fs[rewritesTo_def] \\ rpt conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ ntac 3
          (qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
            (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm)))
      \\ rpt (first_x_assum (qspec_then `rws` destruct) \\ fs[])
      \\ qexistsl_tac [`v1`, `v2`, `v3`] \\ fs[]
      \\ Cases_on `cfg.canOpt` \\ fs[rewritesTo_def] \\ rpt conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ ntac 2 (qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm)))
      \\ rpt (first_x_assum (qspec_then `rws` destruct) \\ fs[])
      \\ qexistsl_tac [`v1`] \\ fs[]
      \\ Cases_on `cfg.canOpt` \\ fs[rewritesTo_def] \\ rpt conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      THENL [DISJ1_TAC, DISJ2_TAC]
      \\ qexists_tac `cv` \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ qpat_x_assum `evalCexpNonDet _ _ (bottomUpCexp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ rpt (first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[])
      \\ Cases_on `cfg.canOpt` \\ fs[rewritesTo_def] \\ rpt conj_tac
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (Cases_on `s` \\ fs[bottomUpExp_def]
      \\ qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ rveq \\ first_x_assum (qspecl_then [`rws1`,`rws2`] destruct)
      \\ fs[] \\ qexists_tac `v'` \\ fs[]
      \\ drop_sem_conf_tac `<| opts:=cfg.opts ++ rws1 ++ rws1 ++ rws2; canOpt := T|>`)
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ imp_res_tac bottomUpApp_map_correct
      \\ find_exists_tac \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ imp_res_tac bottomUpApp_map_correct)
  >- (PairCases_on `p` \\ qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ first_x_assum (mp_then Any assume_tac bottomUpApp_fold_correct)
      \\ first_x_assum (qspec_then `rws2` destruct)
      >- (rpt conj_tac \\ fs[])
      \\ find_exists_tac \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`
      \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
  >- (qpat_assum `evalExpNonDet _ _ (f _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ imp_res_tac bottomUpApp_fold_correct)
  >- (qpat_assum `evalCexpNonDet _ _ (g _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ ntac 2
          (qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
            (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm)))
      \\ rpt (first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]))
  >- (qpat_assum `evalCexpNonDet _ _ (g _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ qexists_tac `v'` \\ fs[crewritesTo_def]
      \\ conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws2 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllCvalTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF]
      \\ rpt strip_tac \\ fs[])
  >- (qpat_assum `evalCexpNonDet _ _ (g _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[]
      \\ ntac 2
          (qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
            (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm)))
      \\ rpt (first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[])
      \\ qexistsl_tac [`v1`, `v2`]
      \\ Cases_on `cfg.canOpt` \\ fs[crewritesTo_def] \\ rpt conj_tac
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws2 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      >- (drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws2 ++ rws1 ++ rws2`
          \\ qexistsl_tac [`rws1`, `rws2`] \\ fs[])
      \\ irule rwAllCvalTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF]
      \\ rpt strip_tac \\ fs[])
  >- (Cases_on `s` \\ fs[bottomUpExp_def]
      \\ qpat_assum `evalCexpNonDet _ _ (g _) _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases] \\ fs[] \\ rveq
      \\ qpat_x_assum `evalCexpNonDet _ _ (bottomUpCexp _ _ _) _`
          (fn thm => first_x_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`,`rws2`] destruct)
      \\ fs[]
      \\ drop_sem_conf_tac `<| opts:=cfg.opts ++ rws2 ++ rws1 ++ rws2; canOpt := T|>`)
  >- (drop_sem_rw_tac)
  >- (fs[evalExpNonDet_cases]
      \\ qpat_x_assum `evalExpNonDet _ _ (bottomUpExp _ _ _) _`
          (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]
      \\ rveq
      \\ qpat_x_assum `evalExpNonDet _ _ (List (bottomUpExpL _ _ _)) _`
          (fn thm => first_assum (fn ithm => mp_then Any assume_tac ithm thm))
      \\ first_x_assum (qspecl_then [`rws1`, `rws2`] destruct) \\ fs[]));

Theorem appAtOpt_map_correct
`! cfg rws1 E s f g e vl v rws2.
  (∀e E v cfg.
       cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
       evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
  (∀e E v cfg.
       cfg.canOpt ∧ evalCexpNonDet cfg E (bottomUpCexp f g e) v ⇒
       evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
  (! E v cfg rws1 rws2 f g.
    (∀e E v cfg.
         cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
         evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
    (∀e E v cfg.
         cfg.canOpt ∧ evalCexpNonDet cfg E (bottomUpCexp f g e) v ⇒
         evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
    evalExpNonDet cfg E (appAtOpt f g e) v ⇒
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v) /\
  evalExpNonDet (cfg with opts := cfg.opts) E (Mapvl s (appAtOpt f g e) vl) v ==>
  evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (Mapvl s e vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ fs[evalExpNonDet_cases] \\ res_tac \\ fs[])

Theorem appAtOpt_fold_correct
  `! cfg rws1 E s f g e vl v rws2 i.
    (∀e E v cfg.
         cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
         evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
    (∀e E v cfg.
         cfg.canOpt ∧ evalCexpNonDet cfg E (bottomUpCexp f g e) v ⇒
         evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
    (! E v cfg rws1 rws2 f g.
      (∀e E v cfg.
           cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
           evalExpNonDet (cfg with opts := cfg.opts ⧺ rws1) E e v) ∧
      (∀e E v cfg.
           cfg.canOpt ∧ evalCexpNonDet cfg E (bottomUpCexp f g e) v ⇒
           evalCexpNonDet (cfg with opts := cfg.opts ⧺ rws2) E e v) ∧
      evalExpNonDet cfg E (appAtOpt f g i) v ⇒
      evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E i v) /\
    (! E v cfg rws1 rws2 f g.
      (∀e E v cfg.
           cfg.canOpt ∧ evalExpNonDet cfg E (bottomUpExp f g e) v ⇒
           evalExpNonDet (cfg with opts := cfg.opts ++ rws1) E e v) ∧
      (∀e E v cfg.
           cfg.canOpt ∧ evalCexpNonDet cfg E (bottomUpCexp f g e) v ⇒
           evalCexpNonDet (cfg with opts := cfg.opts ++ rws2) E e v) ∧
      evalExpNonDet cfg E (appAtOpt f g e) v ⇒
      evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v) /\
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1) E (Foldvl s (appAtOpt f g e) (appAtOpt f g i) vl) v ==>
  evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (Foldvl s e i vl) v`
  (Induct_on `vl` \\ rpt strip_tac \\ PairCases_on `s`
  \\ fs[evalExpNonDet_cases]
  >- (res_tac \\ fs[]
      \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`)
  \\ ntac 2 (pop_assum (fn thm => first_assum (fn ithm => mp_then Any mp_tac ithm thm)))
  \\ disch_then (qspec_then `rws2` destruct)
  >- (rpt conj_tac \\ fs[])
  \\ disch_then (qspecl_then [`rws1`, `rws2`] destruct)
  >- (rpt conj_tac \\ fs[])
  \\ find_exists_tac \\ fs[]
  \\ drop_sem_conf_tac `cfg with opts := cfg.opts ++ rws1 ++ rws1 ++ rws2`);

Theorem appAtOpt_correct
  `(! e E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (bottomUpExp f g e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    evalExpNonDet cfg E (appAtOpt f g e) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! e E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (bottomUpExp f g e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    evalCexpNonDet cfg E (appAtCOpt f g e) v ==>
    evalCexpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! l E v cfg rws1 rws2 f g.
    (!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E (bottomUpExp f g e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    evalExpNonDet cfg E (List (appAtOptL f g l)) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E (List l) v)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac \\ fs[appAtOpt_def, evalExpNonDet_cases]
  >- (res_tac \\ asm_exists_tac \\ fs[rewritesTo_def])
  >- (res_tac \\ asm_exists_tac \\ fs[rewritesTo_def]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def]))
  >- (DISJ1_TAC \\ res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def])
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (DISJ2_TAC \\ res_tac \\ rpt (asm_exists_tac \\ fs[rewritesTo_def])
      \\ irule rwAllValTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (Cases_on `s` \\ fs[appAtOpt_def, evalExpNonDet_cases]
      \\ qpat_assum `evalExpNonDet _ _ _ _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases]
      \\ qexists_tac `v'` \\ fs[]
      \\ drop_sem_conf_tac `<| opts := cfg.opts ++ rws1; canOpt := T |>`)
  >- (res_tac \\ asm_exists_tac \\ fs[]
      \\ irule appAtOpt_map_correct \\ rpt conj_tac \\ fs[]
      \\ qexistsl_tac [`f`, `g`] \\ fs[]
      \\ drop_sem_conf_tac `cfg`)
  >- (irule appAtOpt_map_correct \\ rpt conj_tac \\ fs[]
      \\ qexistsl_tac [`f`, `g`] \\ fs[]
      \\ drop_sem_conf_tac `cfg`)
  >- (Cases_on `p` \\ fs[evalExpNonDet_cases]
      \\ res_tac \\ find_exists_tac \\ fs[]
      \\ irule appAtOpt_fold_correct \\ rpt conj_tac \\ fs[]
      \\ qexistsl_tac [`f`, `g`] \\ fs[]
      \\ drop_sem_conf_tac `cfg`)
  >- (irule appAtOpt_fold_correct \\ rpt conj_tac \\ fs[]
      \\ qexistsl_tac [`f`, `g`] \\ fs[]
      \\ drop_sem_conf_tac `cfg`)
  >- (res_tac \\ asm_exists_tac \\ fs[crewritesTo_def]
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllCvalTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF])
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[crewritesTo_def, rewritesTo_def])
      \\ Cases_on `cfg.canOpt` \\ fs[]
      \\ irule rwAllCvalTree_up \\ asm_exists_tac \\ fs[SUBSET_DEF, UNION_DEF])
  >- (Cases_on `s` \\ fs[appAtOpt_def, evalExpNonDet_cases]
      \\ qpat_assum `evalCexpNonDet _ _ _ _`
          (fn thm => first_assum (fn ithm => mp_then Any destruct ithm thm))
      \\ fs[evalExpNonDet_cases]
      \\ drop_sem_conf_tac `<| opts := cfg.opts ++ rws2; canOpt := T |>`)
  >- (res_tac \\ rpt (asm_exists_tac \\ fs[])));

Theorem id_rw_correct
  `!e E v cfg.
      cfg.canOpt /\
      evalExpNonDet cfg E ((\x.x) e) v ==>
      evalExpNonDet (cfg with opts := cfg.opts ++ []) E e v`
  (rpt strip_tac \\ fs[] \\ drop_sem_rw_tac);

Theorem id_crw_correct
  `!e E v cfg.
      cfg.canOpt /\
      evalCexpNonDet cfg E ((\x.x) e) v ==>
      evalCexpNonDet (cfg with opts := cfg.opts ++ []) E e v`
  (rpt strip_tac \\ fs[] \\ drop_sem_rw_tac);

(**
  The correctness of the greedy compiler can be nearly fully automated with the
  above infrastructural lemmas.
  We reduce it to showing each rewrite correct separately and then use
  HOL4's modus ponens matching (MATCH_MP) to generate the correctness theorems
  below
**)
fun curry thm = REWRITE_RULE[satTheory.AND_IMP] thm;
fun uncurry thm = REWRITE_RULE[GSYM satTheory.AND_IMP] thm;

fun mk_rwExp_corr_thm thms =
let
  val rwExp_comp_imp = curry rewriteExp_compositional
in
  case thms of
   [] => rewriteExp_compositional
  | thm::[] => MATCH_MP (MATCH_MP rwExp_comp_imp (curry thm)) (curry empty_rw_correct)
  | thm::thms =>
    let val rec_res = mk_rwExp_corr_thm thms in
      MATCH_MP (MATCH_MP rwExp_comp_imp (curry thm)) rec_res
  end
end;

fun mk_bottomUp_corr_thm thmExp thmCexp =
let
  val bottomUp_corr_imp = curry (fth bottomUpApp_correct)
in
  MATCH_MP (MATCH_MP bottomUp_corr_imp (curry thmExp)) (curry thmCexp)
end;

fun mk_bottomUpc_corr_thm thmExp thmCexp =
let
  val bottomUp_corr_imp = curry (fth (sth bottomUpApp_correct))
in
  MATCH_MP (MATCH_MP bottomUp_corr_imp (curry thmExp)) (curry thmCexp)
end;

fun mk_appAtOpt_corr_thm thmA thmB =
let
  val appAtOpt_corr_imp = curry (fth appAtOpt_correct)
in
  MATCH_MP (MATCH_MP appAtOpt_corr_imp (curry thmA)) (curry thmB)
end;

val greedyRws_rewrite_correct =
  map mk_rwExp_corr_thm
  [
    [FMARULE_correct],
    [PLUSASSOCRULE_correct],
    [PLUSCOMMRULE_correct],
    [MULTASSOCRULE_correct],
    [MULTCOMMRULE_correct]
  ];

val greedyRws_bottomUp_correct =
  map
    (fn thm =>
      REWRITE_RULE [APPEND_NIL] (mk_bottomUp_corr_thm thm id_crw_correct))
  greedyRws_rewrite_correct;

val greedyRws_bottomUpC_correct =
  map
    (fn thm =>
      REWRITE_RULE [APPEND_NIL] (mk_bottomUpc_corr_thm thm id_crw_correct))
  greedyRws_rewrite_correct;

val appAtOpt_corr_thms =
  map
    (fn (thm1, thm2) =>
      mk_appAtOpt_corr_thm thm1 thm2)
    (zip greedyRws_bottomUp_correct greedyRws_bottomUpC_correct);

val FMARULE_app_corr =
  save_thm ("FMARULE_app_corr",(List.nth (appAtOpt_corr_thms,0)));

val PLUSASSOC_app_corr =
  save_thm ("PLUSASSOC_app_corr",(List.nth (appAtOpt_corr_thms,1)));

val PLUSCOMM_app_corr =
  save_thm ("PLUSCOMM_app_corr",(List.nth (appAtOpt_corr_thms,2)));

val MULTASSOC_app_corr =
  save_thm ("MULTASSOC_app_corr",(List.nth (appAtOpt_corr_thms,3)));

val MULTCOMM_app_corr =
  save_thm ("MULTCOMM_app_corr",(List.nth (appAtOpt_corr_thms,4)));

(*
val DNEG_app_corr =
  save_thm ("DNEG_app_corr",(List.nth (appAtOpt_corr_thms,5))); *)

(**
  Compiler correctness for compileGreedy with the Greedy rewrites from
  section 3.2
**)
Theorem compileGreedy_correct
  `! e E v cfg.
    evalExpNonDet (cfg with opts := []) E
      (optimizeGreedy
        [PLUSASSOCRULE; PLUSCOMMRULE ; MULTASSOCRULE; MULTCOMMRULE; FMARULE]
        e) v ==>
    evalExpNonDet
      (cfg with opts :=
        [PLUSASSOCRULE; PLUSCOMMRULE ; MULTASSOCRULE; MULTCOMMRULE; FMARULE])
      E e v`
  (rpt strip_tac
  \\ fs [optimizeGreedy_def]
  \\ imp_res_tac (PLUSASSOC_app_corr) \\ fs[]
  \\ imp_res_tac (PLUSCOMM_app_corr) \\ fs[]
  \\ imp_res_tac (MULTASSOC_app_corr) \\ fs[]
  \\ imp_res_tac (MULTCOMM_app_corr) \\ fs[]
  \\ imp_res_tac (FMARULE_app_corr) \\ fs[]
  \\ drop_sem_rw_tac);

(**
  Soundness proof of removeNaNcheck_correctness from CompilerScript.sml.
**)
val removeNaNcheck_correctness = store_thm (
  "removeNaNcheck_correctness",
  ``! e eOpt cfg E v log.
      cfg.canOpt /\
      removeNaNcheck e = SOME (log) /\
      (! p. MEM p log ==> p) /\
      evalCexpNonDet cfg E (rewriteCexp [NONANCHECKRULE] e) v ==>
      evalCexpNonDet (cfg with opts := NONANCHECKRULE :: cfg.opts) E e v``,
  rpt strip_tac \\ Cases_on `e` \\ fs[removeNaNcheck_def]
  \\ Cases_on `p` \\ fs[removeNaNcheck_def] \\ rveq
  \\ fs[NONANCHECKRULE_def, rewriteCexp_def, matchesCexpr_def, matchesExpr_def, appExpr_def]
  \\ first_x_assum (qspecl_then [`cfg`, `E`] assume_tac) \\ fs[]
  \\ res_tac \\ rveq
  \\ fs[evalExpNonDet_cases]
  \\ qexists_tac `v'` \\ conj_tac
  >- (drop_sem_rw_tac)
  \\ qexists_tac `[RewriteApp Here 0]` \\ EVAL_TAC);

val cond_rw_correct = store_thm (
  "cond_rw_correct",
  ``!P e E v cfg rws.
    (!e E v cfg.
       cfg.canOpt /\
       evalExpNonDet cfg E (rewriteExp rws e) v ==>
       evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v) /\
    cfg.canOpt /\
    evalExpNonDet cfg E ((\e. if P e then rewriteExp rws e else e) e) v ==>
    evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v``,
  rpt strip_tac \\ fs[]
  \\ Cases_on `P e`
  >- (first_x_assum irule \\fs[])
  \\ fs[] \\ drop_sem_rw_tac);

val cond_rw_correct_pre = store_thm (
  "cond_rw_correct_pre",
  ``!P e E v cfg rws.
      (!e E v cfg.
         P e /\
         cfg.canOpt /\
         evalExpNonDet cfg E (rewriteExp rws e) v ==>
         evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v) /\
      cfg.canOpt /\
      evalExpNonDet cfg E ((\e. if P e then rewriteExp rws e else e) e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v``,
  rpt strip_tac \\ fs[]
  \\ Cases_on `P e` \\ fs[]
  \\ drop_sem_rw_tac);

val getList_def = Define `
  getList [] = [] /\
  getList ((CondExp P rws)::crws) = APPEND rws (getList crws) /\
  getList ((CondCexp P rws)::crws) = APPEND rws (getList crws) /\
  getList ((AsmExp A rws)::crws) = APPEND rws (getList crws) /\
  getList ((AsmCexp A rws)::crws) = APPEND rws (getList crws)`;

Theorem bottomUpApp_correct_cond
  `(! e E v cfg rws1 rws2 f g Pf Pg.
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\
      evalExpNonDet cfg E (f e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      Pg /\
      evalCexpNonDet cfg E (g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    cfg.canOpt /\
    Pf /\ Pg /\
    evalExpNonDet cfg E (bottomUpExp f g e) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v) /\
  (! e E v cfg rws1 rws2 f g Pf Pg.
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\
      evalExpNonDet cfg E (f e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      Pg /\
      evalCexpNonDet cfg E (g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    cfg.canOpt /\
    Pf /\ Pg /\
    evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
    evalCexpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! (l:expr list). l = l ==> T)`
  (rpt conj_tac \\ fs[] \\ rpt strip_tac
  >- (irule (hd (CONJ_LIST 2 bottomUpApp_correct))
      \\ fs[] \\ rpt find_exists_tac \\ fs[])
  \\ irule (List.nth (CONJ_LIST 3 bottomUpApp_correct, 1)) \\ fs[]
  \\ rpt find_exists_tac \\ fs[])

Theorem appAtOpt_correct_cond
  `(! e E v cfg rws1 rws2 f g Pf Pg.
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\ Pg /\
      evalExpNonDet cfg E (bottomUpExp f g e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\ Pg /\
      evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    Pf /\ Pg /\
    evalExpNonDet cfg E (appAtOpt f g e) v ==>
    evalExpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! e E v cfg rws1 rws2 f g Pf Pg.
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\ Pg /\
      evalExpNonDet cfg E (bottomUpExp f g e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws1) E e v) /\
    (!e E v cfg.
      cfg.canOpt /\
      Pf /\ Pg /\
      evalCexpNonDet cfg E (bottomUpCexp f g e) v ==>
      evalCexpNonDet (cfg with opts := APPEND cfg.opts rws2) E e v) /\
    Pf /\ Pg /\
    evalCexpNonDet cfg E (appAtCOpt f g e) v ==>
    evalCexpNonDet (cfg with opts := cfg.opts ++ rws1 ++ rws2) E e v)
  /\
  (! (l:expr list). l = l ==> T)`
  (rpt conj_tac \\ rpt strip_tac \\ fs[] \\ imp_res_tac appAtOpt_correct)

Theorem rewriteExp_compositional_cond
  `!e E v rw rws cfg Prw Prws.
    (* Correctness of single rewrite *)
    (!e E v cfg.
      cfg.canOpt /\
      Prw /\
      evalExpNonDet cfg E (rewriteExp [rw] e) v ==>
      evalExpNonDet (cfg with opts := rw :: cfg.opts) E e v) /\
    (* recursive correctness *)
    (!e E v cfg.
      cfg.canOpt /\
      (! P. MEM P Prws ==> P) /\
      evalExpNonDet cfg E (rewriteExp rws e) v ==>
      evalExpNonDet (cfg with opts := APPEND cfg.opts rws) E e v) /\
    cfg.canOpt /\
    (! P. MEM P (Prw::Prws) ==> P) /\
    evalExpNonDet cfg E (rewriteExp (rw::rws) e) v ==>
    evalExpNonDet (cfg with opts := APPEND cfg.opts (rw::rws)) E e v`
  (rpt strip_tac \\ irule rewriteExp_compositional
  \\ conj_tac \\ rpt strip_tac \\ fs[]
  >- (last_x_assum irule \\ fs[] \\ rpt strip_tac \\ first_x_assum irule \\ fs[])
  \\ last_x_assum irule \\ fs[] \\ first_x_assum (qspec_then `Prw` irule) \\ fs[])

(**
  General correctness theorem for the conditional optimizer.
  We prove a general version of the theorem instead of a specialized one since
  we do not yet implement connections to external tools, as pointed out in
  section 4.
  The theorem is similar to the one for optimizeGreedy. The main difference is
  that the correctness theorems for rewrites using `AsmExp` or `AsmCexp` can
  additionally rely on correctness of the assumption returned by the function
**)
local
  fun resolve_last_pat_then p t =
    qpat_x_assum p (fn thm => last_assum (fn ithm => mp_then Any t ithm thm));
  fun resolve_first_pat_then p t =
    qpat_x_assum p (fn thm => first_assum (fn ithm => mp_then Any t ithm thm));
in
Theorem optimizeCond_correct_general
  `! e E crws v eopt logA logB cfg.
    (* Conditional expression optimization correctness *)
    (! f l. MEM (CondExp f l) crws ==>
      ! e cfg E v.
        evalExpNonDet cfg E
          (appAtOpt (\e. if f e then rewriteExp l e else e) (\x.x) e) v ==>
        evalExpNonDet (cfg with opts := l ++ cfg.opts) E e v) /\
    (* Conditional cexpression optimization correctness *)
    (! f l. MEM (CondCexp f l) crws ==>
      ! e cfg E v.
        evalExpNonDet cfg E
          (appAtOpt (\x.x) (\e. if f e then rewriteCexp l e else e) e) v ==>
        evalExpNonDet (cfg with opts := l ++ cfg.opts) E e v) /\
    (* Assumption expression optimization correctness,
      note that the function call here is different and allows to "assume"
      correctness of the log when proving the rewrite correct locally *)
    (! f l. MEM (AsmExp f l) crws ==>
      ! e eopt cfg E v logA logB.
        (appAndLogOpt
          (appAndLogExp f l)
          (\x. (x,[]:bool list)) e) = (eopt, logA, logB) /\
        evalExpNonDet cfg E eopt v ==>
        evalExpNonDet (cfg with opts := l ++ cfg.opts) E e v) /\
    (* Assumption cexpression optimization correctness,
      note that the function call here is different and allows to "assume"
      correctness of the log when proving the rewrite correct locally *)
    (! f l. MEM (AsmCexp f l) crws ==>
      ! e eopt cfg E v logA logB.
        (appAndLogOpt
          (\x. (x,[]:bool list))
          (appAndLogCexp f l) e) = (eopt, logA, logB) /\
        evalExpNonDet cfg E eopt v ==>
        evalExpNonDet (cfg with opts := l ++ cfg.opts) E e v) /\
    optimizeCond crws e = (eopt, logA, logB) /\
    evalExpNonDet cfg E eopt v ==>
    evalExpNonDet (cfg with opts := getList crws ++ cfg.opts) E e v`
  (Induct_on `crws` \\ rw[optimizeCond_def, getList_def, cfg_component_equality]
  >- (drop_sem_conf_tac `cfg`)
  \\ Cases_on `h` \\ fs[optimizeCond_def]
  >- (Cases_on `optimizeCond crws e` \\ Cases_on `r` \\ fs[] \\ rveq
      \\ resolve_first_pat_then `evalExpNonDet _ _ (appAtOpt _ _ _) _` destruct
      \\ fs[]
      \\ resolve_last_pat_then `evalExpNonDet _ _ q v` assume_tac
      \\ resolve_first_pat_then `optimizeCond _ _ = _` destruct
      >- (rw[] \\ res_tac)
      \\ fs[getList_def]
      \\ drop_sem_conf_tac `cfg with opts := getList crws ++ l ++ cfg.opts`)
  >- (Cases_on `optimizeCond crws e` \\ Cases_on `r` \\ fs[] \\ rveq
      \\ resolve_first_pat_then `evalExpNonDet _ _ (appAtOpt _ _ _) _` destruct
      \\ fs[]
      \\ resolve_last_pat_then `evalExpNonDet _ _ q v` assume_tac
      \\ resolve_first_pat_then `optimizeCond _ _ = _` destruct
      >- (rw[] \\ res_tac)
      \\ fs[getList_def]
      \\ drop_sem_conf_tac `cfg with opts := getList crws ++ l ++ cfg.opts`)
  >- (Cases_on `optimizeCond crws e` \\ Cases_on `r` \\ fs[] \\ rveq
      \\ Cases_on `appAndLogOpt (appAndLogExp f l) (\x. (x,[]:bool list)) q`
      \\ Cases_on `r` \\ fs[] \\ rveq
      \\ resolve_first_pat_then `appAndLogOpt _ _ _ = _` assume_tac
      \\ resolve_first_pat_then `evalExpNonDet _ _ eopt _` destruct
      \\ fs[]
      \\ resolve_last_pat_then `evalExpNonDet _ _ q v` assume_tac
      \\ resolve_first_pat_then `optimizeCond _ _ = _` destruct
      >- (rw[] \\ res_tac)
      \\ fs[getList_def]
      \\ drop_sem_conf_tac `cfg with opts := getList crws ++ l ++ cfg.opts`)
  \\ Cases_on `optimizeCond crws e` \\ Cases_on `r` \\ fs[] \\ rveq
  \\ Cases_on `appAndLogOpt (\x. (x,[]:bool list)) (appAndLogCexp f l) q`
  \\ Cases_on `r` \\ fs[] \\ rveq
  \\ resolve_first_pat_then `appAndLogOpt _ _ _ = _` assume_tac
  \\ resolve_first_pat_then `evalExpNonDet _ _ eopt _` destruct
  \\ fs[]
  \\ resolve_last_pat_then `evalExpNonDet _ _ q v` assume_tac
  \\ resolve_first_pat_then `optimizeCond _ _ = _` destruct
  >- (rw[] \\ res_tac)
  \\ fs[getList_def]
  \\ drop_sem_conf_tac `cfg with opts := getList crws ++ l ++ cfg.opts`)
end

val _ = export_theory();
