(**
  This file defines the CakeML connection function "toCML" from section 5.
  Semantic equivalence is proven in file CakeMLconnProofsScript.sml.
**)

open patternTheory valueTreesTheory SyntaxTheory
     SemanticsTheory CompilerTacticsLib CompilerProofsTheory;
open astTheory fpSemTheory evaluateTheory semanticPrimitivesTheory
     namespaceTheory namespacePropsTheory terminationTheory miscTheory;
open machine_ieeeTheory binary_ieeeTheory;
open icingPreamble;

val _ = new_theory "CakeMLconn";

open ListProgTheory ml_translatorLib ml_progLib;

val _ = translation_extends "ListProg";

Globals.max_print_depth := (~1);
(* val _ = translation_extends "basisProg"; *)

val state = get_env (ml_translatorLib.get_ml_prog_state());
val ffi_state = get_state (ml_translatorLib.get_ml_prog_state());

(* Move to HOL4 libs? *)
val float_eq_not_id_nan = store_thm (
  "float_eq_not_id_nan",
  ``!x. ~ float_equal x x <=> float_is_nan x``,
  rpt strip_tac \\ fs[float_equal_def, float_compare_def, float_is_nan_def]
  \\ Cases_on `float_value x` \\ fs[]);

val fp64_eq_not_id_nan = store_thm (
  "fp64_eq_not_id_nan",
  ``!x. fp64_equal x x = F <=> fp64_isNan x``,
  rpt strip_tac \\ fs[fp64_equal_def, fp64_isNan_def]
  \\ irule float_eq_not_id_nan \\ fs[]);

(* Operator translations *)
val toUop_def = Define `
  toUop (valueTrees$Neg) = FP_Neg /\
  toUop (valueTrees$Sqrt) = FP_Sqrt`;

val toBop_def = Define `
  toBop (valueTrees$Plus) = fpSem$FP_Add /\
  toBop (valueTrees$Minus) = FP_Sub /\
  toBop (valueTrees$Mult) = FP_Mul /\
  toBop (valueTrees$Div) = FP_Div `;

val toTop_def = Define `
  toTop (Fma) = FP_Fma `;

val toCmp_def = Define `
  toCmp (valueTrees$Less) = FP_Less /\
  toCmp (valueTrees$Leq) = FP_LessEqual /\
  toCmp (valueTrees$Eq) =  FP_Equal `;

(* Expression translations for true, false, nil and cons *)
val true_exp_def = Define `
  true_exp = ast$App (Opb Leq) [ast$Lit (IntLit 0); ast$Lit (IntLit 0)]`;

val false_exp_def = Define `
  false_exp = ast$App (Opb Leq) [ast$Lit (IntLit 1); ast$Lit (IntLit 0)]`;

val nil_exp_def = Define
  `nil_exp = ast$Con (SOME (Short"[]")) []`;

val cons_exp_def = Define `
  cons_exp e1 el = ast$Con (SOME (Short "::")) [e1; el]`;

(* EVAL ``evaluate ^ffi_state ^state [cons_exp (Lit (Word64 0w)) nil_exp]`` *)

val map_name_def = Define `
  map_name =  Var (Long "List" (Short "map"))`;

(* EVAL ``nsLookup (^state).v (Long"List"(Short"map"))`` *)

(*
  REWRITE_RULE [ml_progTheory.eval_rel_def, ml_translatorTheory.Eval_def]
  (hol2deep ``(MAP:(num -> num) -> num list -> num list) (\x.x) []``);
*)

(* EVAL_RULE
  (ONCE_REWRITE_RULE [map_1_v_def]
  (EVAL
  ``evaluate (^ffi_state with clock:=100) ^state
  [ast$App Opapp [ast$App Opapp [map_name; Fun "x" (ast$Var (Short "x"))]; nil_exp]]``)) *)

val fold_name_def = Define `
  fold_name =  Var (Long "List" (Short "foldr"))`;

(* EVAL ``nsLookup (^state).v (Long"List"(Short"foldr"))`` *)

val ith_name_def = Define `
  ith_name = Var (Long "List" (Short "nth"))`;

val vTree2CML_def = Define `
  vTree2CML (Wconst w) = (ast$Lit (Word64 w)) /\
  vTree2CML (UnVal uop e) =
    (ast$App (FP_uop (toUop uop)) [vTree2CML e]) /\
  vTree2CML (BinVal b e1 e2) =
    (ast$App (FP_bop (toBop b)) [vTree2CML e1; vTree2CML e2]) /\
  vTree2CML (TerVal top e1 e2 e3) =
    (ast$App (FP_top (toTop top)) [vTree2CML e1; vTree2CML e2; vTree2CML e3]) /\
  vTree2CML (Scope sc e) = vTree2CML e`;

val valTrees2CML_def = Define `
  valTrees2CML [] =  nil_exp /\
  valTrees2CML (v::vl) = cons_exp (vTree2CML v) (valTrees2CML vl)`;

(**
  Main definition of the translation function. In the paper this function is
  called `toCML`.
  Icing ASTs are translated into CakeML source ASTs by structural recursion.
**)
val expToCML_def = Define `
  expToCML (Var v) = (ast$Var (mk_id [] v)) /\
  expToCML (Wconst w) = (ast$Lit (Word64 w)) /\
  expToCML (Ith n e) = (ast$App Opapp [ast$App Opapp [ith_name; expToCML e]; ast$Lit (IntLit (&n))]) /\
  expToCML (List []) = nil_exp /\
  expToCML (List (e::el)) = cons_exp (expToCML e) (expToCML (List el)) /\
  expToCML (Unop uop e) =
    (ast$App (FP_uop (toUop uop)) [expToCML e]) /\
  expToCML (Binop b e1 e2) =
    (ast$App (FP_bop (toBop b)) [expToCML e1; expToCML e2]) /\
  expToCML (Terop top e1 e2 e3) =
    (ast$App (FP_top (toTop top)) [expToCML e1; expToCML e2; expToCML e3]) /\
  expToCML (Syntax$Let x e1 e2) =
    (ast$Let (SOME x) (expToCML e1) (expToCML e2)) /\
  expToCML (Cond c e1 e2) =
    ast$If (cexpToCML c)
      (expToCML e1)
      (expToCML e2)  /\
  expToCML (Map x f e) =
    ast$App Opapp
      [ast$App Opapp [map_name; (Fun x (expToCML f))];
      expToCML e] /\
  expToCML (Fold (x,y) f i e) =
    ast$App Opapp
      [ast$App Opapp
        [ast$App Opapp
          [fold_name; (Fun y (Fun x (expToCML f)))];
        expToCML i];
      (expToCML e)] /\
  expToCML (Mapvl x f vl) =
    ast$App Opapp
      [ast$App (Opapp)
        [map_name; (Fun x (expToCML f))];
      (valTrees2CML vl)] /\
  expToCML (Foldvl (x,y) f i vl) =
    ast$App Opapp
      [ast$App Opapp
        [ast$App Opapp
          [fold_name; (Fun y (Fun x (expToCML f)))];
        expToCML i];
      (valTrees2CML vl)] /\
  expToCML (Scope sc e) = expToCML e
  /\
  cexpToCML (Bconst b) = (if b then true_exp else false_exp) /\
  cexpToCML (Pred NaN e) =
    (ast$If (ast$App (FP_cmp FP_Equal) [expToCML e; expToCML e]) (false_exp) (true_exp)) /\
  cexpToCML (Cmp cmp e1 e2) =
    (ast$App (FP_cmp (toCmp cmp) ) [expToCML e1; expToCML e2]) /\
  cexpToCML (Cscope sc c) = cexpToCML c`;

(**
  Translation of CakeML values into Icing value trees
**)
val toIcingVal_def = Define `
  toIcingVal (Litv l) =
    (case l of
    | Word64 w => SOME (Val (valueTrees$Wconst w))
    | _ => NONE) /\
  toIcingVal (Conv (SOME ts) vl) =
    (case ts, vl of
    | (TypeStamp s 0), [] =>
      if (s = "True")
      then SOME (Cval (valueTrees$Bconst T))
      else if (s = "False")
      then SOME (Cval (valueTrees$Bconst F))
      else NONE
    | (TypeStamp "[]" 1), [] => SOME (Lval [])
    | (TypeStamp "::" 1), vl =>
        (case vl of
        | [] => NONE
        | Litv (Word64 w)::[Conv ts vl] =>
          (case toIcingVal (Conv ts vl) of
          |SOME (Lval vres) => SOME (Lval ((valueTrees$Wconst w)::vres))
          | _ => NONE)
        | _ => NONE)
    | _, _ => NONE) /\
  toIcingVal _ = NONE`;

(**
  Translation from Icing value trees into CakeML values
**)
val fromIcingVal_def = Define `
  fromIcingVal (Val v) = Litv (Word64 (tree2IEEE v)) /\
  fromIcingVal (Cval b) = Boolv (cTree2IEEE b) /\
  fromIcingVal (Lval []) =  Conv (SOME (TypeStamp "[]" 1)) [] /\
  fromIcingVal (Lval (v::vl)) =
    Conv (SOME (TypeStamp "::" 1))
      ((fromIcingVal (Val v))::[fromIcingVal (Lval vl)])`;

val tofML_env_def = Define `
  tofML_env (env:v sem_env) =
  \x. (case nsLookup env.v (mk_id [] x) of
    | NONE => NONE
    | SOME v => toIcingVal v)`;

val isIcingVal_def = Define `
  isIcingVal (Litv (Word64 w)) = T /\
  isIcingVal (Conv ts vl) =
    (case vl of
    | [] => ts = SOME (TypeStamp "[]" 1)
    | v1::vl =>
      LENGTH (v1::vl) = 2 /\
      ts = SOME (TypeStamp "::" 1) /\
      (case v1, vl of
      | Litv (Word64 w), [Conv ts vl] => isIcingVal (Conv ts vl)
      | _, _ => F)) /\
  isIcingVal _ = F`;

(**
  Translation function that produces a CakeML environment from
  the Icing environment E.
  The resulting CakeML environment binds every free variable
  which is contained in the second argument.
**)
val expToCML_env_def = Define `
  expToCML_env E (Wconst w):(tvarN, tvarN, v) namespace = nsEmpty /\
  expToCML_env E (Syntax$Var w) =
    (case E w of
    |SOME v => nsBind w (fromIcingVal v) nsEmpty
    |NONE => nsEmpty) /\
  expToCML_env E (Ith _ e) = expToCML_env E e /\
  expToCML_env E (List []) = nsEmpty /\
  expToCML_env E (List (e::el)) = nsAppend (expToCML_env E e) (expToCML_env E (List el)) /\
  expToCML_env E (Unop _ e) = expToCML_env E e /\
  expToCML_env E (Binop b e1 e2) = nsAppend (expToCML_env E e1) (expToCML_env E e2) /\
  expToCML_env E (Terop _ e1 e2 e3) =
    nsAppend (nsAppend (expToCML_env E e1) (expToCML_env E e2)) (expToCML_env E e3) /\
  expToCML_env E (Let x e1 e2) = nsAppend (expToCML_env E e1) (expToCML_env E e2) /\
  expToCML_env E (Cond c e1 e2) =
    (nsAppend (nsAppend (cexpToCML_env E c) (expToCML_env E e1)) (expToCML_env E e2)) /\
  expToCML_env E (Map x f e) =
    (nsAppend (expToCML_env E f) (expToCML_env E e)) /\
  expToCML_env E (Mapvl x f vl) = expToCML_env E f /\
  expToCML_env E (Fold p f s e) =
    (nsAppend (nsAppend (expToCML_env E f) (expToCML_env E s)) (expToCML_env E e)) /\
  expToCML_env E (Foldvl p f s vl) =
    (nsAppend (expToCML_env E f) (expToCML_env E s)) /\
  expToCML_env E (Scope _ e) = expToCML_env E e
  /\
  cexpToCML_env E (Syntax$Bconst b) = nsEmpty /\
  cexpToCML_env E (Pred p e) = expToCML_env E e /\
  cexpToCML_env E (Cmp _ e1 e2) = nsAppend (expToCML_env E e1) (expToCML_env E e2) /\
  cexpToCML_env E (Cscope _ ce) = cexpToCML_env E ce`;


val _ = export_theory();
