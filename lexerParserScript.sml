open SyntaxTheory;

open icingPreamble;

val _ = new_theory "lexerParser";

val _ = Datatype `
  Token =
    (* Control Flow *)
    | LET | IN | EQ | COND | THEN | ELSE | MAP | ABS | FOLD | OPT | SCOPE
    (* Variables and constants *)
    | VAR string | WCONST word64 | BCONST bool | NUM num
    (* Operators *)
    | SQRT | NEG | PLUS | MUL | DIV | LESS | NAN | FMA
    (* Brackets *)
    | LRBRAC | RRBRAC | LBRAC | RBRAC
    (* Syntax for lists, functions *)
    | SEP | DOT`;

val getConst_def = Define `
  getConst (c:char) = ORD c - 48`;

val DEST_STRING_LEN_decreasing = store_thm (
  "DEST_STR_LEN_decreasing",
  ``!s c st. DEST_STRING s = SOME (c, st) ==>
    STRLEN st < STRLEN s``,
  Induct \\ fs[DEST_STRING_def]);

val lexDigit_def = tDefine "lexDigit" `
  lexDigit (s:string) (akk:num) =
  case (DEST_STRING s) of
  | NONE => (akk, "")
  | SOME (c, strc) =>
    if (isDigit c)
    then lexDigit strc (akk * 10 + getConst(c))
    else (akk, s) `
  (WF_REL_TAC `measure (STRLEN o FST)` \\ rw[] \\ fs[DEST_STRING_LEN_decreasing]);

val lexDigit_decreasing = store_thm (
  "lexDigit_decreasing",
  ``!s akk c rm.
      lexDigit s akk = (c, rm)  ==> STRLEN rm <= STRLEN s``,
   Induct \\ simp[Once lexDigit_def]
   \\ rw[] \\res_tac \\ fs[DEST_STRING_LEN_decreasing]);

val lexHexString_def = Define `
  lexHexString "" = ("", "") /\
  lexHexString (STRING c str) =
    if (isHexDigit c)
    then
      (case lexHexString str of
      |(str, strRem) => (STRING c str, strRem))
    else ("", STRING c str)`;

Theorem lexHexString_decreasing
  `! s hexStr rm.
    lexHexString s = (hexStr, rm) ==> STRLEN rm <= STRLEN s`
  (Induct \\ simp [Once lexHexString_def] \\ rw[]
  \\ fs[] \\ Cases_on `lexHexString s` \\ fs[]);

val strAppend_def = Define `
  strAppend (s1:string) (s2:string) =
  case s1 of
  | STRING c s1rm => STRING c (strAppend s1rm s2)
  | "" => s2`;

val lexAlphaNum_def = tDefine "lexAlphaNum" `
  lexAlphaNum (s:string) (akk:string) : string # string =
  case (DEST_STRING s) of
  | NONE => (akk, "")
  | SOME (c, strc) =>
    if (isAlphaNum c)
    then lexAlphaNum strc (strAppend akk (STR c))
    else (akk, s) `
  (WF_REL_TAC `measure (STRLEN o FST)` \\ rw[] \\ fs[DEST_STRING_LEN_decreasing]);

val lexAlphaNum_decreasing = store_thm (
  "lexAlphaNum_decreasing",
  ``!s akk name rm.
    lexAlphaNum s akk = (name, rm) ==> STRLEN rm <= STRLEN s``,
  Induct \\ simp[Once lexAlphaNum_def] \\ rw[] \\res_tac \\ fs[DEST_STRING_LEN_decreasing]);

val lex_def = tDefine "lex" `
  lex ("":string) :Token list = [] /\
  lex (STRING c str) :Token list =
    if (isSpace c)
    then lex str
    else
      if (isDigit c)
      then
        (if (c = #"0")
        then
          (case str of
          | STRING (#"x") strRem =>
            (case lexHexString strRem of
            | (hexStr, strRem2) => WCONST (word_from_hex_string hexStr) :: lex strRem2
            | _ => [])
          | _ =>
            let (n, strc) = lexDigit str (getConst c) in
            NUM n :: lex strc)
        else
          let (n, strc) = lexDigit str (getConst c) in
            NUM n :: lex strc)
      else
        if (isAlpha c)
        then
          let (name, strc) = lexAlphaNum str (STR c) in
            case name of
            | "let" => LET :: lex strc
            | "in" => $IN :: lex strc
            | "FMA" => FMA :: lex strc
            | "sqrt" => SQRT :: lex strc
            | "if" => COND :: lex strc
            | "then" => THEN :: lex strc
            | "else" => ELSE :: lex strc
            | "opt" => OPT :: lex strc
            | "Map" => MAP :: lex strc
            | "Fold" => FOLD :: lex strc
            | "fn" => ABS :: lex strc
            | "T" => BCONST T :: lex strc
            | "F" => BCONST F :: lex strc
            | "isNaN" => NAN :: lex strc
            | _ =>  VAR name :: lex strc
        else
          case c of
            | #"," => SEP :: lex str
            | #"." => DOT :: lex str
            | #":" =>  SCOPE :: lex str
            | #"=" => EQ :: lex str
            | #"<" => LESS :: lex str
            | #"+" => PLUS :: lex str
            | #"-" => NEG :: lex str
            | #"*" => MUL :: lex str
            | #"/" => $DIV :: lex str
            | #"~" => OPT :: lex str
            | #"(" => LBRAC :: lex str
            | #")" => RBRAC :: lex str
            | #"[" => LRBRAC :: lex str
            | #"]" => RRBRAC :: lex str`
  (WF_REL_TAC `measure STRLEN` \\ rw[] \\ fs[DEST_STRING_LEN_decreasing]
   \\ imp_res_tac (GSYM lexDigit_decreasing) \\ fs []
   \\ imp_res_tac (GSYM lexAlphaNum_decreasing) \\ fs []
   \\ imp_res_tac (lexHexString_decreasing) \\ fs[]
   \\ irule arithmeticTheory.LESS_EQ_LESS_TRANS \\ once_rewrite_tac [CONJ_COMM]
   \\ asm_exists_tac \\ fs[DEST_STRING_LEN_decreasing]);

val ppNum_def = Define `
  ppNum (n:num) =
    if n < 9 then STR (CHR n)
    else
      let rm = n MOD 10 in
      strAppend (ppNum (n DIV 10)) (STR (CHR rm)) `;

val ppTok_def = Define `
  (ppTok (NIL:Token list) = "") /\
  (ppTok (WCONST n :: tokList) =
    strAppend (strAppend "0x" (word_to_hex_string n)) (ppTok tokList)) /\
  (ppTok (NUM n :: tokList) =
    strAppend (strAppend (ppNum n) " ") (ppTok tokList)) /\
  (ppTok (BCONST b :: tokList) =
    strAppend (if b then "T" else "F") (ppTok tokList)) /\
  (ppTok (VAR name :: tokList) = strAppend (strAppend (name) " ") (ppTok tokList)) /\
  (ppTok (LET :: tokList) = strAppend "let " (ppTok tokList)) /\
  (ppTok ($IN :: tokList) = strAppend "in " (ppTok tokList)) /\
  (ppTok (COND :: tokList) = strAppend "if " (ppTok tokList)) /\
  (ppTok (THEN :: tokList) = strAppend "then " (ppTok tokList)) /\
  (ppTok (ELSE :: tokList) = strAppend "else " (ppTok tokList)) /\
  (ppTok (OPT :: tokList) = strAppend "opt " (ppTok tokList)) /\
  (ppTok (SCOPE :: tokList) = strAppend ": " (ppTok tokList)) /\
  (ppTok (MAP :: tokList) = strAppend "Map " (ppTok tokList)) /\
  (ppTok (FOLD :: tokList) = strAppend "Fold " (ppTok tokList)) /\
  (ppTok (ABS :: tokList) = strAppend "fn " (ppTok tokList)) /\
  (ppTok (EQ :: tokList) = strAppend "= " (ppTok tokList)) /\
  (ppTok (LESS :: tokList) = strAppend "< " (ppTok tokList)) /\
  (ppTok (FMA :: tokList) = strAppend "FMA " (ppTok tokList)) /\
  (ppTok (PLUS :: tokList) = strAppend "+ " (ppTok tokList)) /\
  (ppTok (NEG :: tokList) = strAppend "- " (ppTok tokList)) /\
  (ppTok (MUL :: tokList) = strAppend "* " (ppTok tokList)) /\
  (ppTok ($DIV :: tokList) = strAppend "/ " (ppTok tokList)) /\
  (ppTok (NAN :: tokList) = strAppend "isNaN " (ppTok tokList)) /\
  (ppTok (OPT :: tokList) = strAppend "~ " (ppTok tokList)) /\
  (ppTok (NANCHECK :: tokList) = strAppend "nonan " (ppTok tokList)) /\
  (ppTok (LBRAC :: tokList) = strAppend "( " (ppTok tokList)) /\
  (ppTok (RBRAC :: tokList) = strAppend ") " (ppTok tokList)) /\
  (ppTok (LRBRAC :: tokList) = strAppend "[ " (ppTok tokList)) /\
  (ppTok (RRBRAC :: tokList) = strAppend "] " (ppTok tokList)) /\
  (ppTok (SEP :: tokList) = strAppend ", " (ppTok tokList)) /\
  (ppTok (DOT :: tokList) = strAppend ". " (ppTok tokList))`;

val fix_res_def = Define `
  fix_res xs NONE = NONE /\
  fix_res xs (SOME (x,y)) =
    if LENGTH xs < LENGTH y then SOME (x,xs) else SOME (x,y)`

val fix_res_imp = prove(
  ``fix_res xs a = SOME (x,y) ==> LENGTH y <= LENGTH xs``,
  Cases_on `a` \\ rw [fix_res_def]
  \\ Cases_on `x'` \\ fs [fix_res_def]
  \\ every_case_tac \\ fs [] \\ rveq \\ fs []);

val isBinop_def = Define `
  isBinop (PLUS :: tokList) = SOME (Plus, tokList) /\
  isBinop (NEG :: tokList) = SOME (Minus, tokList) /\
  isBinop (MUL :: tokList) = SOME (Mult, tokList) /\
  isBinop ($DIV :: tokList) = SOME (Div, tokList) /\
  isBinop _ = NONE`;

val isBinop_dec = store_thm (
  "isBinop_dec",
  ``isBinop ls = SOME (op, lsRem) ==> LENGTH lsRem < LENGTH ls``,
  rpt strip_tac \\ Cases_on `ls` \\ fs[isBinop_def]
  \\ Cases_on `h` \\ fs[isBinop_def]
  \\ Cases_on `t` \\ fs[isBinop_def]
  \\ Cases_on `h` \\ fs[isBinop_def]);

val isCmp_def = Define `
  isCmp (EQ :: tokList) = SOME (Eq, tokList) /\
  isCmp (LESS :: EQ :: tokList) = SOME (Leq, tokList) /\
  isCmp (LESS :: tokList) = SOME (Less, tokList) /\
  isCmp _ = NONE`;

val isCmp_dec = store_thm (
  "isCmp_dec",
  ``isCmp ls = SOME (op, lsRem) ==> LENGTH lsRem < LENGTH ls``,
  rpt strip_tac \\ Cases_on `ls` \\ fs[isCmp_def]
  \\ Cases_on `h` \\ fs[isCmp_def]
  \\ Cases_on `t` \\ fs[isCmp_def] \\ rveq \\ fs[]
  \\ Cases_on `h` \\ fs[isCmp_def] \\ rveq \\ fs[]);

val parseExp_def = tDefine
  "parseExp"
  `parseExp tokList =
    (let resP =
      (case tokList of
      | [] => NONE
      | (WCONST n) :: tokList => SOME (Syntax$Wconst n, tokList)
      | (VAR name) :: tokList => SOME (Syntax$Var name, tokList)
      | LET :: tokList =>
        (case tokList of
        |VAR name :: EQ :: tokRem =>
          (case fix_res tokRem (parseExp tokRem) of
          |NONE => NONE
          |SOME (e1, tokRem1) =>
            (case tokRem1 of
            | $IN :: tokRem2 =>
              (case fix_res tokRem2 (parseExp tokRem2) of
              |NONE => NONE
              |SOME (e2, tokRem3) => SOME (Syntax$Let name e1 e2, tokRem3))
            | _ => NONE))
        | _ => NONE)
      | COND :: tokList =>
        (case fix_res tokList (parseCexp tokList) of
        |NONE => NONE
        |SOME (c1, tokRem1) =>
          (case tokRem1 of
           | THEN :: tokRem1 =>
            (case fix_res tokRem1 (parseExp tokRem1) of
            |NONE => NONE
            |SOME (e1, tokRem2) =>
              (case tokRem2 of
               | ELSE :: tokRem2 =>
                 (case fix_res tokRem2 (parseExp tokRem2) of
                 | NONE => NONE
                 | SOME (e2, tokRem3) =>
                    SOME (Syntax$Cond c1 e1 e2, tokRem3))
               | _ => NONE))
           | _ => NONE))
      | FMA :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        |NONE => NONE
        |SOME (e1, tokRem1) =>
          (case fix_res tokRem1 (parseExp tokRem1) of
          |NONE => NONE
          |SOME (e2, tokRem2) =>
            (case fix_res tokRem2 (parseExp tokRem2) of
            |NONE => NONE
            |SOME (e3, tokRem3) =>
              SOME (Syntax$Terop Fma e1 e2 e3, tokRem3))))
      | SQRT :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        | NONE => NONE
        | SOME (e1, tokRem1) => SOME (Syntax$Unop Sqrt e1, tokRem1))
      | NEG :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        | NONE => NONE
        | SOME (e, tokRem) => SOME (Syntax$Unop Neg e, tokRem))
      | OPT :: tokList =>
        (case tokList of
         | SCOPE::tokList2 =>
           (case fix_res tokList2 (parseExp tokList2) of
           | NONE => NONE
           | SOME (e, tokRem) => SOME (Scope Opt e, tokRem))
         | _ => NONE)
      | MAP :: ABS :: (VAR s) :: DOT :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        | NONE => NONE
        | SOME (f, tokRem1) =>
          (case fix_res tokRem1 (parseExp tokRem1) of
          | NONE => NONE
          |SOME (el, tokRem2) => SOME (Map s f el, tokRem2)))
      | FOLD :: ABS :: (VAR x) :: (VAR y) :: DOT :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        | NONE => NONE
        | SOME (f, tokRem1) =>
          (case fix_res tokRem1 (parseExp tokRem1) of
          | NONE => NONE
          | SOME (i, tokRem2) =>
            (case fix_res tokRem2 (parseExp tokRem2) of
            | NONE => NONE
            | SOME (el, tokRem3) => SOME (Fold (x,y) f i el, tokRem3))))
      | LBRAC :: tokList =>
        (case fix_res tokList (parseExp tokList) of
        |SOME (e, RBRAC::tokRem) => SOME (e, tokRem)
        | _ => NONE)
      | LRBRAC :: tokList =>
        (case fix_res tokList (parseExpL tokList)  of
        |SOME (el, RRBRAC :: tokRem) => SOME (List el, tokRem)
        | _ => NONE)
      | _ => NONE)
    in
      case resP of
        | NONE => NONE
        | SOME (e1, tokRem) =>
          (case isBinop tokRem of
          |NONE =>
            (case tokRem of
            | LRBRAC :: NUM n :: RRBRAC :: tokRem =>
              SOME (Ith n e1, tokRem)
            | _ => SOME (e1, tokRem))
          |SOME (op, tokRem2) =>
            (case fix_res tokRem2 (parseExp tokRem2) of
            |NONE => SOME (e1, tokRem)
            |SOME (e2, tokRem3) => SOME (Syntax$Binop op e1 e2, tokRem3))))
  /\
  parseCexp tokList =
   (case tokList of
    | [] => NONE
    | (BCONST b) :: tokRem => SOME (Bconst b, tokRem)
    | NAN :: tokRem =>
      (case fix_res tokRem (parseExp tokRem) of
      | SOME (e, tokList) => SOME (Pred NaN e, tokList)
      | NONE => NONE)
    | (OPT :: SCOPE :: tokRem) =>
      (case fix_res tokRem (parseCexp tokRem) of
      | SOME (c1, tokList) => SOME (Cscope Opt c1, tokList)
      | NONE => NONE)
    | LBRAC :: tokList =>
      (case fix_res tokList (parseExp tokList) of
      | NONE => NONE
      | SOME (e1, tokRem1) =>
        (case (isCmp tokRem1) of
        | NONE => NONE
        | SOME (cmp, tokRem2) =>
          (case fix_res tokRem2 (parseExp tokRem2) of
          | NONE => NONE
          | SOME (e2, RBRAC :: tokRem3) => SOME (Cmp cmp e1 e2, tokRem3)
          | SOME _ => NONE)))
    | _ => NONE)
  /\
  parseExpL (LRBRAC ::tokRem) =
    (case fix_res tokRem (parseExp tokRem) of
    |SOME (e, tokL) =>
      (case tokL of
      | SEP:: tokL =>
        (case parseExpL (LRBRAC :: tokL) of
        |SOME (el, tokRem2) => SOME (e::el, tokRem2)
        | _ => NONE)
      | RRBRAC :: tokRem => SOME ([e], tokRem)
      | _ => NONE)
    | NONE =>
      (case tokRem of
      | RRBRAC :: tokRem => SOME ([], tokRem)
      | _ => NONE)) /\
  parseExpL (RRBRAC::tokRem) = SOME ([],tokRem) /\
  parseExpL _ = NONE`
  (WF_REL_TAC
    `measure
      (\x:Token list + Token list + Token list.
        case x of |INL x => LENGTH x |INR (INL x) => LENGTH x | INR (INR x) => LENGTH x)`
  \\ rw[] \\ fs[LENGTH]
  \\ imp_res_tac fix_res_imp \\ fs []
  \\ imp_res_tac isBinop_dec
  >- (imp_res_tac isCmp_dec \\ fs[])
  \\ Cases_on `tokList` \\ fs[]
  \\ Cases_on `h` \\ fs[option_case_eq, pair_case_eq]
  >- (Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ fs[] \\ Cases_on `t'` \\ fs[]
      \\ Cases_on `h` \\ fs[option_case_eq]
      \\ Cases_on `v1` \\ fs[] \\ Cases_on `r` \\ fs[]
      \\ Cases_on `h` \\ fs[option_case_eq]
      \\ Cases_on `v` \\ fs[]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  >- (rveq \\ Cases_on `v8` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `v4` \\ fs[option_case_eq, pair_case_eq]
      \\ Cases_on `h` \\ fs[option_case_eq, pair_case_eq]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  >- (Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `t'` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  >- (Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `t'` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ Cases_on `t'` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  >- (Cases_on `t` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  >- (rveq \\ imp_res_tac fix_res_imp \\ fs[])
  >- (rveq \\ imp_res_tac fix_res_imp \\ fs[])
  >- (rveq \\ imp_res_tac fix_res_imp \\ fs[])
  >- (Cases_on `v5` \\ fs[]
      \\ Cases_on `h` \\ rveq \\ fs[option_case_eq,pair_case_eq]
      \\ rveq
      \\ imp_res_tac fix_res_imp \\ fs[])
  \\ rveq
  \\ Cases_on `v5` \\ fs[]
  \\ Cases_on `h` \\ fs[]
  \\ rveq
  \\ imp_res_tac fix_res_imp \\ fs[]);

val lexParse_test1 = prove (
  ``parseExp(lex "(a + b)") = SOME (Binop Plus (Var "a") (Var "b"),[])``,
  EVAL_TAC);

val lexParse_test2 = prove (
  ``parseExp (lex "opt:(a + b )") = SOME (Scope Opt (Binop Plus (Var "a") (Var "b")),[])``,
  EVAL_TAC);

val lexParse_test3 = prove (
  ``parseExp (lex "let x = (a + b) in opt:(x + 0x1)") =
    SOME
      (Let "x" (Binop Plus (Var "a") (Var "b"))
        (Scope Opt (Binop Plus (Var "x") (Wconst 1w))),[])``,
  EVAL_TAC);

val lexParse_test4 = prove (
  ``parseExp (lex "if (x = x) then x else y") =
      SOME
        (Cond (Cmp Eq (Var "x") (Var "x")) (Var "x") (Var "y"),[])``,
  EVAL_TAC);

val lexParse_test5 = store_thm (
  "lexParse_test5",
  ``parseExp (lex "Map fn x. x [[]]") = SOME (Map "x" (Var "x") (List []), [])``,
  EVAL_TAC);

val _ = export_theory();
