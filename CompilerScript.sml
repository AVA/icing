(**
  This file contains the definitions of
  the IEEE754 preserving translator from section 3.1 (function compileIEEE754),
  the greedy optimizer from section 32. (function optimizeGreedy),
  and the conditional optimizer from section 4 (here: optimizeCond)
**)
open patternTheory valueTreesTheory SemanticsTheory rewriteRulesTheory;
open machine_ieeeTheory binary_ieeeLib optionTheory;

open icingPreamble;

val _ = new_theory "Compiler";

(**
  function bottomUpExp f g e applies function f in a bottom up walk to all nodes
  of e  and function g to all "boolean" expressions encountered in between
  The distinction between `f` and `g` is only done because the functions need
  to return different types of expressions.
**)
val bottomUpExp_def = tDefine
  "bottomUpExp"
 `bottomUpExp (f:expr -> expr) (g:cexpr->cexpr) (Wconst w) = f (Wconst w) /\
  bottomUpExp f g (Var v) = f (Var v) /\
  bottomUpExp f g (List l) = f (List (bottomUpExpL f g l)) /\
  bottomUpExp f g (Ith n e) = f (Ith n (bottomUpExp f g e)) /\
  bottomUpExp f g (Unop uop e) = f (Unop uop (bottomUpExp f g e)) /\
  bottomUpExp f g (Binop bop e1 e2) =
    f (Binop bop (bottomUpExp f g e1) (bottomUpExp f g e2)) /\
  bottomUpExp f g (Terop top e1 e2 e3) =
    f (Terop top (bottomUpExp f g e1) (bottomUpExp f g e2) (bottomUpExp f g e3)) /\
  bottomUpExp f g (Let x e1 e2) =
    f (Let x (bottomUpExp f g e1) (bottomUpExp f g e2)) /\
  bottomUpExp f g (Cond c e1 e2) =
    f (Cond (bottomUpCexp f g c) (bottomUpExp f g e1) (bottomUpExp f g e2)) /\
  bottomUpExp f g (Scope Opt e) = f (Scope Opt (bottomUpExp f g e)) /\
  bottomUpExp f g (Map x h e) = f (Map x (bottomUpExp f g h) (bottomUpExp f g e)) /\
  bottomUpExp f g (Mapvl x h vl) = f (Mapvl x (bottomUpExp f g h) vl) /\
  bottomUpExp f g (Fold p h i e) =
    f (Fold p (bottomUpExp f g h) (bottomUpExp f g i) (bottomUpExp f g e)) /\
  bottomUpExp f g (Foldvl p h i vl) =
    f (Foldvl p (bottomUpExp f g h) (bottomUpExp f g i) vl)
  /\
  bottomUpCexp f g (Bconst b) = g (Bconst b) /\
  bottomUpCexp f g (Pred p e) = g (Pred p (bottomUpExp f g e)) /\
  bottomUpCexp f g (Cmp cmp e1 e2) =
    g (Cmp cmp (bottomUpExp f g e1) (bottomUpExp f g e2)) /\
  bottomUpCexp f g (Cscope Opt ce) = g (Cscope Opt (bottomUpCexp f g ce))
  /\
  bottomUpExpL f g [] = [] /\
  bottomUpExpL f g (e::el) = (bottomUpExp f g e) :: (bottomUpExpL f g el)`
  (WF_REL_TAC
    `measure
      (\x.
        case x of
          |INL (_, _, x) => expr_size x
          |INR (INL (_, _, x)) => cexpr_size x
          | INR (INR (_,_,x)) => expr1_size x)`
  \\ fs[]);

(*
  Same as bottomUpExp, but additionally returns a log
*)
val bottomUpLoge_def = tDefine
  "bottomUpLoge"
  `bottomUpLoge (f:expr -> expr#'a list) (g:cexpr->cexpr#'b list) (Wconst w)
    :expr#'a list # 'b list=
    (let (res, log) = f (Wconst w) in
      (res, log ,[])) /\
  bottomUpLoge f g (Var v) =
    (let (res, log) = f (Var v) in
      (res, log, [])) /\
  bottomUpLoge f g (List el) =
  (let
    (res, logA, logB) = bottomUpLogel f g el;
    (res_l, logL) = f (List res);
  in
    (res_l, logL ++ logA, logB)) /\
  bottomUpLoge f g (Ith n e) =
  (let
    (res, logA, logB) = bottomUpLoge f g e;
    (res_i, logiA) = f (Ith n res)
  in
    (res_i, logiA ++ logA, logB)) /\
  bottomUpLoge f g (Unop uop e) =
    (let
      (res_e, logA, logB) = bottomUpLoge f g e;
      (res_u, logU) = f (Unop uop res_e);
    in
      (res_u, logU ++ logA, logB)) /\
  bottomUpLoge f g (Binop bop e1 e2) =
    (let
      (res_e1, logA1, logB1) = bottomUpLoge f g e1;
      (res_e2, logA2, logB2) = bottomUpLoge f g e2;
      (res_b, logB) = f (Binop bop res_e1 res_e2);
    in
      (res_b, logB++ (logA1 ++ logA2), logB1 ++ logB2)) /\
  bottomUpLoge f g (Terop top e1 e2 e3) =
    (let
      (res_e1, logA1, logB1) = bottomUpLoge f g e1;
      (res_e2, logA2, logB2) = bottomUpLoge f g e2;
      (res_e3, logA3, logB3) = bottomUpLoge f g e3;
      (res_t, logT) = f (Terop top res_e1 res_e2 res_e3);
    in
      (res_t, logT++ (logA1 ++ logA2 ++ logA3), logB1 ++ logB2 ++ logB3)) /\
  bottomUpLoge f g (Let x e1 e2) =
    (let
      (res_e1, logA1, logB1) = bottomUpLoge f g e1;
      (res_e2, logA2, logB2) = bottomUpLoge f g e2;
      (res_b, logB) = f (Let x res_e1 res_e2);
    in
      (res_b, logB++ (logA1 ++ logA2), logB1 ++ logB2)) /\
  bottomUpLoge f g (Cond c e1 e2) =
    (let
      (res_c, logAc, logBc) = bottomUpLogc f g c;
      (res_e1, logA1, logB1) = bottomUpLoge f g e1;
      (res_e2, logA2, logB2) = bottomUpLoge f g e2;
      (res_t, logT) = f (Cond c res_e1 res_e2);
    in
      (res_t, logT++(logAc ++ logA1 ++ logA2), APPEND logBc (APPEND logB1 logB2))) /\
  bottomUpLoge f g (Scope Opt e) =
    (let
      (res_e, logA, logB) = bottomUpLoge f g e;
      (res_s, logS) = f (Scope Opt res_e)
    in
      (res_s, logS ++ logA, logB)) /\
  bottomUpLoge f g (Map x h e) =
    (let
      (res_h, log_hA, log_hB) = bottomUpLoge f g h;
      (res_e, log_eA, log_eB) = bottomUpLoge f g e;
      (res_m, log_mA) = f (Map x res_h res_e);
    in
      (res_m, (log_mA ++ log_eA ++ log_hA), log_eB ++ log_hB)) /\
  bottomUpLoge f g (Mapvl x h vl) =
    (let
      (res_h, log_hA, log_hB) = bottomUpLoge f g h;
      (res_m, log_mA) = f (Mapvl x res_h vl);
    in
      (res_m, log_mA ++ log_hA, log_hB)) /\
  bottomUpLoge f g (Fold x h i e) =
    (let
      (res_h, log_hA, log_hB) = bottomUpLoge f g h;
      (res_i, log_iA, log_iB) = bottomUpLoge f g i;
      (res_e, log_eA, log_eB) = bottomUpLoge f g e;
      (res_m, log_mA) = f (Fold x res_h res_i res_e);
    in
      (res_m, log_mA ++ log_eA ++ log_iA ++ log_hA,
        log_eB ++ log_iB ++ log_hB)) /\
  bottomUpLoge f g (Foldvl x h i vl) =
    (let
      (res_h, log_hA, log_hB) = bottomUpLoge f g h;
      (res_i, log_iA, log_iB) = bottomUpLoge f g i;
      (res_m, log_mA) = f (Foldvl x res_h res_i vl);
    in
      (res_m, log_mA ++ log_iA ++ log_hA,
        log_iB ++ log_hB))
  /\
  bottomUpLogc f g (Bconst b) :cexpr#'a list # 'b list =
    (let (res_b, logB) = g (Bconst b) in
      (res_b, [], logB)) /\
  bottomUpLogc f g (Pred p e) =
    (let (res_e, logA1, logB1) = bottomUpLoge f g e;
      (res_p, logB) = g (Pred p res_e);
    in
      (res_p, logA1, logB ++ logB1)) /\
  bottomUpLogc f g (Cmp cmp e1 e2) =
    (let
      (res_e1, logA1, logB1) = bottomUpLoge f g e1;
      (res_e2, logA2, logB2) = bottomUpLoge f g e2;
      (res_b, logB) = g (Cmp cmp res_e1 res_e2);
    in
      (res_b, (logA1 ++ logA2), logB++(logB1 ++ logB2))) /\
  bottomUpLogc f g (Cscope Opt ce) =
    (let
      (res_e, logA, logB) = bottomUpLogc f g ce;
      (res_s, logS) = g (Cscope Opt res_e)
    in
      (res_s, logA, logS++logB))
  /\
  bottomUpLogel f g [] = ([], [], []) /\
  bottomUpLogel f g (e::el) =
    (let
      (el_new, log_elA, log_elB) = bottomUpLogel f g el;
      (e_new, log_eA, log_eB) = bottomUpLoge f g e;
    in
      (e_new :: el_new, log_eA ++ log_elA, log_eB ++ log_elB))`
  (WF_REL_TAC
    `measure
      (\x.
        case x of
          |INL (_, _, x) => expr_size x
          |INR (INL (_, _, x)) => cexpr_size x
          | INR (INR (_,_,x)) => expr1_size x)`
  \\ fs[]);

(**
  appAtOpt f g e is used to find a position in which bottomUp application
  of functions f,g is possible in e, traversing the AST until a `opt`
  node is found
**)
val appAtOpt_def = tDefine
  "appAtOpt"
  `appAtOpt f g (List el) = List (appAtOptL f g el) /\
  appAtOpt f g (Ith n e) = Ith n (appAtOpt f g e) /\
  appAtOpt f g (Scope Opt e) = Scope Opt (bottomUpExp f g e) /\
  appAtOpt f g (Unop u e) = Unop u (appAtOpt f g e) /\
  appAtOpt f g (Binop b e1 e2) = Binop b (appAtOpt f g e1) (appAtOpt f g e2) /\
  appAtOpt f g (Terop t e1 e2 e3) =
    Terop t (appAtOpt f g e1) (appAtOpt f g e2) (appAtOpt f g e3) /\
  appAtOpt f g(Let x e1 e2) = Let x (appAtOpt f g e1) (appAtOpt f g e2) /\
  appAtOpt f g (Cond c e1 e2) =
    Cond (appAtCOpt f g c) (appAtOpt f g e1) (appAtOpt f g e2) /\
  appAtOpt f g (Map x h e) =  Map x (appAtOpt f g h) (appAtOpt f g e) /\
  appAtOpt f g (Mapvl x h vl) = Mapvl x (appAtOpt f g h) vl /\
  appAtOpt f g (Fold p h i e) =
    Fold p (appAtOpt f g h) (appAtOpt f g i) (appAtOpt f g e) /\
  appAtOpt f g (Foldvl p h i vl) =
    Foldvl p (appAtOpt f g h) (appAtOpt f g i) vl /\
  appAtOpt f g e = e
  /\
  appAtCOpt f g (Pred p e) = Pred p (appAtOpt f g e) /\
  appAtCOpt f g (Cmp cmp e1 e2) = Cmp cmp (appAtOpt f g e1) (appAtOpt f g e2) /\
  appAtCOpt f g (Cscope Opt e) = Cscope Opt (bottomUpCexp f g e) /\
  appAtCOpt f g e = e
  /\
  appAtOptL f g [] = [] /\
  appAtOptL f g (e::el) = (appAtOpt f g e) :: (appAtOptL f g el)`
  (WF_REL_TAC
    `measure
      (\x.
        case x of
          |INL (_, _, x) => expr_size x
          |INR (INL (_, _, x)) => cexpr_size x
          | INR (INR (_,_,x)) => expr1_size x)`
  \\ fs[]);

(**
  Same as appAtOpt f g e, but additionally returns the logbook from bottomUpLog
**)
val appAndLogOpt_def = tDefine
  "appAndLogOpt"
  `appAndLogOpt f g (List el) =
    (let
      (resl, logAl, logBl) = appAndLogOptL f g el
    in
      (List resl, logAl, logBl)) /\
  appAndLogOpt f g (Ith n e) =
    (let
      (res_e, logA, logB) = appAndLogOpt f g e
    in
      (Ith n res_e, logA, logB)) /\
  appAndLogOpt f g (Scope Opt e) =
    (let (res, logA, logB) = (bottomUpLoge f g e) in
    (Scope Opt res, logA, logB)) /\
  appAndLogOpt f g (Unop u e) =
    (let (res, logA, logB) = (appAndLogOpt f g e) in
    (Unop u res, logA, logB)) /\
  appAndLogOpt f g (Binop b e1 e2) =
    (let
      (res1, logA1, logB1) = (appAndLogOpt f g e1);
      (res2, logA2, logB2) = (appAndLogOpt f g e2)
    in
    (Binop b res1 res2, logA1 ++ logA2, logB1 ++ logB2)) /\
  appAndLogOpt f g (Terop t e1 e2 e3) =
    (let
      (res1, logA1, logB1) = (appAndLogOpt f g e1);
      (res2, logA2, logB2) = (appAndLogOpt f g e2);
      (res3, logA3, logB3) = (appAndLogOpt f g e3)
    in
    (Terop t res1 res2 res3, logA1 ++ logA2 ++ logA3, logB1 ++ logB2 ++ logB3)) /\
  appAndLogOpt f g(Let x e1 e2) =
    (let
      (res1, logA1, logB1) = (appAndLogOpt f g e1);
      (res2, logA2, logB2) = (appAndLogOpt f g e2)
    in
    (Let x res1 res2, logA1 ++ logA2, logB1 ++ logB2)) /\
  appAndLogOpt f g (Cond c e1 e2) =
    (let
      (c1, logAC1, logBC1) = (appAndLogCOpt f g c);
      (res1, logA1, logB1) = (appAndLogOpt f g e1);
      (res2, logA2, logB2) = (appAndLogOpt f g e2)
    in
    (Cond c1 res1 res2, logAC1 ++ logA1 ++ logA2, logBC1 ++ logB1 ++ logB2)) /\
  appAndLogOpt f g (Map x h e) =
    (let
      (res_h, log_hA, log_hB) = appAndLogOpt f g h;
      (res_e, log_eA, log_eB) = appAndLogOpt f g e;
    in
       (Map x res_h res_e, log_eA ++ log_hA, log_eB ++ log_hB)) /\
  appAndLogOpt f g (Mapvl x h vl) =
    (let
      (res_h, log_hA, log_hB) = appAndLogOpt f g h;
    in
      (Mapvl x res_h vl, log_hA, log_hB)) /\
  appAndLogOpt f g (Fold x h i e) =
    (let
      (res_h, log_hA, log_hB) = appAndLogOpt f g h;
      (res_i, log_iA, log_iB) = appAndLogOpt f g i;
      (res_e, log_eA, log_eB) = appAndLogOpt f g e;
    in
      (Fold x res_h res_i res_e,
        log_eA ++ log_iA ++ log_hA,
        log_eB ++ log_iB ++ log_hB)) /\
  appAndLogOpt f g (Foldvl x h i vl) =
    (let
      (res_h, log_hA, log_hB) = appAndLogOpt f g h;
      (res_i, log_iA, log_iB) = appAndLogOpt f g i;
    in
      (Foldvl x res_h res_i vl, log_iA ++ log_hA, log_iB ++ log_hB)) /\
  appAndLogOpt f g e = (e, [], [])
  /\
  appAndLogCOpt f g (Pred p e) =
    (let
      (res, logA, logB) = appAndLogOpt f g e
    in
      (Pred p res, logA, logB)) /\
  appAndLogCOpt f g (Cmp cmp e1 e2) =
    (let
      (res1, logA1, logB1) = (appAndLogOpt f g e1);
      (res2, logA2, logB2) = (appAndLogOpt f g e2)
    in
    (Cmp cmp res1 res2, logA1 ++ logA2, logB1 ++ logB2)) /\
  appAndLogCOpt f g (Cscope Opt e) =
    (let (res, logA, logB) = (bottomUpLogc f g e) in
    (Cscope Opt res, logA, logB)) /\
  appAndLogCOpt f g e = (e, [], [])
  /\
  appAndLogOptL f g [] = ([], [], []) /\
  appAndLogOptL f g (e::el) =
    (let
      (el_res, logAl, logBl) = appAndLogOptL f g el;
      (e_res, logA, logB) = appAndLogOpt f g e;
    in
    (e_res :: el_res, logA ++ logAl, logB ++ logBl))`
  (WF_REL_TAC
    `measure
      (\x.
        case x of
          |INL (_, _, x) => expr_size x
          |INR (INL (_, _, x)) => cexpr_size x
          | INR (INR (_,_,x)) => expr1_size x)`
  \\ fs[]);

(**
  The greedy optimizer is a simple call to appAtOpt for each rewrite provided
  We make the second function the identity function since all of the rewrites we
  support in the greedy optimizer operate on arithmetic expressions and not on
  conditional expressions
**)
val optimizeGreedy_def = Define `
  optimizeGreedy [] e = e /\
  optimizeGreedy (r::rws) e =
    appAtOpt (rewriteExp [r]) (\x.x) (optimizeGreedy rws e)`;

(**
  The list of rewrites supported by the greedy compiler
**)
val greedyRws_def = Define `
  greedyRws:rw_rule list = [
    PLUSASSOCRULE;
    PLUSCOMMRULE;
    MULTASSOCRULE;
    MULTCOMMRULE;
    FMARULE]`;

(* Simple testcases for optimizeGreedy*)
val test1 = prove (``
  optimizeGreedy greedyRws
    (Scope Opt (Binop Plus (Binop Plus (Wconst 3w) (Wconst 2w)) (Wconst 1w))) =
       (Scope Opt (Binop Plus (Wconst 1w) (Binop Plus (Wconst 2w) (Wconst 3w))))``,
  EVAL_TAC);

val test2 = prove (``
  optimizeGreedy greedyRws
       (Syntax$Binop Plus (Wconst 1w) (Binop Mult (Wconst 2w) (Wconst 3w))) =
     (Syntax$Binop Plus (Wconst 1w) (Binop Mult (Wconst 2w) (Wconst 3w)))``,
  EVAL_TAC);

val test3 = prove (``
  optimizeGreedy greedyRws
       (Scope Opt (Binop Plus (Binop Mult (Wconst 1w) (Wconst 2w)) (Wconst 3w))) =
     (Scope Opt (Terop Fma (Wconst 1w) (Wconst 2w) (Wconst 3w)))``,
  EVAL_TAC);

(**
  Datatype for rewrites used by conditional optimizer
**)
val _ = Datatype `
  info_rw =
    | CondExp (expr -> bool) (rw_rule list)
    | CondCexp (cexpr -> bool) (rw_rule list)
    | AsmExp (expr -> (bool list) option) (rw_rule list)
    | AsmCexp (cexpr -> (bool list) option) (rw_rule list)`;

val appAndLogExp_def = Define `
  appAndLogExp f rws =
    (\e. case f e of
      | SOME log => (rewriteExp rws e,log)
      | NONE => (e,[]))`;

val appAndLogCexp_def = Define `
  appAndLogCexp f rws =
    (\e. case f e of
      | SOME log => (rewriteCexp rws e,log)
      | NONE => (e,[]))`;

(**
  Conditional optimizer from Section 4
  Cases for AsmExp and AsmCexp are the ones where an assumption is introduced in
  the context (assume A rws in the paper)
  Cases for CondExp and CondCexp are conditional applications (cond P rws) in
  the paper
  uncond rws is obtained as cond True rws
**)
val optimizeCond_def = Define `
  optimizeCond [] e = (e,[],[]) /\
  optimizeCond ((CondExp P rws)::crws) e =
    (let (res, logA, logB) = optimizeCond crws e in
      (appAtOpt (\e. if P e then rewriteExp rws e else e) (\x.x) res, logA, logB)) /\
  optimizeCond ((CondCexp P rws)::crws) e =
    (let (res, logA, logB) = optimizeCond crws e in
    (appAtOpt (\x.x) (\ce. if P ce then rewriteCexp rws ce else ce) res, logA, logB)) /\
  optimizeCond (AsmExp f rws:: crws) e =
    (let
      (res, logAG, logBG) = optimizeCond crws e;
      (resG, logA, logB) =
        (appAndLogOpt (appAndLogExp f rws) (\x. (x,[])) res);
    in
      (resG, logA ++ logAG, logB ++ logBG)) /\
  optimizeCond (AsmCexp f rws:: crws) e =
    let
      (res, logAG, logBG) = optimizeCond crws e;
      (resG, logA, logB) =
        (appAndLogOpt
          (\x. (x,[]))
          (appAndLogCexp f rws) res);
    in
      (resG, logA ++ logAG, logB ++ logBG)`;

(**
  Draft of an implementation for NaN check removal, as explained in section 4.3
  of the paper.
  This implementation is not part of the contribution of the paper.
**)
val removeNaNcheck_def = Define `
  removeNaNcheck (Pred NaN e1) =
      SOME ([! cfg E. (? v. evalExpNonDet cfg E e1 (Val v)) /\
           (! v. evalExpNonDet cfg E e1 (Val v) ==> ~ fp64_isNan (tree2IEEE v))]) /\
  removeNaNcheck _ = NONE`;

val NONANCHECK_ASM_RULE_def = Define `
  NONANCHECK_ASM_RULE = AsmCexp removeNaNcheck [NONANCHECKRULE]`;

(**
  IEEE754 preserving translator as in section 3.1 of the paper
**)
val compileIEEE754_def = tDefine
  "compileIEEE754"
  `compileIEEE754 (Var v) = Var v /\
  compileIEEE754 (Wconst w) = Wconst w /\
  compileIEEE754 (List el) = List (compileIEEE754L el) /\
  compileIEEE754 (Ith n e) = Ith n (compileIEEE754 e) /\
  compileIEEE754 (Unop uop e) = Unop uop (compileIEEE754 e) /\
  compileIEEE754 (Binop bop e1 e2) =
    Binop bop (compileIEEE754 e1) (compileIEEE754 e2) /\
  compileIEEE754 (Terop top e1 e2 e3) =
    Terop top (compileIEEE754 e1) (compileIEEE754 e2) (compileIEEE754 e3) /\
  compileIEEE754 (Let x e1 e2) =
    Let x (compileIEEE754 e1) (compileIEEE754 e2) /\
  compileIEEE754 (Cond c e1 e2) =
    Cond (compileIEEE754c c) (compileIEEE754 e1) (compileIEEE754 e2) /\
  compileIEEE754 (Scope Opt e) = compileIEEE754 e /\
  compileIEEE754 (Map x f e) = Map x (compileIEEE754 f) (compileIEEE754 e) /\
  compileIEEE754 (Mapvl x f vl) = Mapvl x (compileIEEE754 f) vl /\
  compileIEEE754 (Fold p f i e) =
    Fold p (compileIEEE754 f) (compileIEEE754 i) (compileIEEE754 e) /\
  compileIEEE754 (Foldvl p f i vl) =
    Foldvl p (compileIEEE754 f) (compileIEEE754 i) vl
  /\
  compileIEEE754c (Bconst b) = Bconst b /\
  compileIEEE754c (Pred p e) = Pred p (compileIEEE754 e) /\
  compileIEEE754c (Cmp cmp e1 e2) = Cmp cmp (compileIEEE754 e1) (compileIEEE754 e2) /\
  compileIEEE754c (Cscope Opt ce) = compileIEEE754c ce
  /\
  compileIEEE754L [] = [] /\
  compileIEEE754L (e::el) = (compileIEEE754 e)::(compileIEEE754L el)`
  (WF_REL_TAC
    `measure
      (\x.
        case x of
          |INL x => expr_size x
          |INR (INL x) => cexpr_size x
          |INR (INR x) => expr1_size x)`
  \\ fs[]);

val _ = export_theory();
