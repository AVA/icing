(**
  Definition of a simple type checker for the Icing language
  We use the type checker and its soundness proof in the CakeML connection
  to show the backwards simulation
**)

open SyntaxTheory SemanticsTheory;
open preamble icingPreamble;

val _ = new_theory "wellDefinedExp";

val _ = Datatype `
  Ftype = Word | List num`

val wellDefined_def = tDefine
  "wellDefinedExp"
  `wellDefined Gamma (Wconst _) = SOME Word /\
  wellDefined Gamma (Syntax$Var x) = FLOOKUP Gamma x /\
  wellDefined Gamma (List el) =
    (case wellDefinedL Gamma el of
    | SOME (List m)=> SOME (List m)
    | _ => NONE) /\
  wellDefined Gamma (Ith n e) =
    (case wellDefined Gamma e of
    | SOME (List m) => (if n < m then SOME Word else NONE)
    | _ => NONE) /\
  wellDefined Gamma (Unop _ e) =
    (case wellDefined Gamma e of
    | SOME Word => SOME Word
    | _ => NONE) /\
  wellDefined Gamma (Binop _ e1 e2) =
    (case wellDefined Gamma e1, wellDefined Gamma e2 of
    |SOME Word, SOME Word => SOME Word
    | _, _ => NONE) /\
  wellDefined Gamma (Terop _ e1 e2 e3) =
    (case wellDefined Gamma e1, wellDefined Gamma e2, wellDefined Gamma e3 of
    |SOME Word, SOME Word, SOME Word => SOME Word
    | _, _, _ => NONE) /\
  wellDefined Gamma (Let x e1 e2) =
    (case wellDefined Gamma e1 of
    | SOME t => wellDefined (Gamma |+ (x, t)) e2
    | _ => NONE) /\
  wellDefined Gamma (Cond c e1 e2) =
    (if wellDefinedBool Gamma c
    then
      case wellDefined Gamma e1, wellDefined Gamma e2 of
      |SOME t1, SOME t2 => if (t1 = t2) then SOME t1 else NONE
      | _, _ => NONE
    else NONE) /\
  wellDefined Gamma (Scope _ e) = wellDefined Gamma e /\
  wellDefined Gamma (Map x f el) =
    (case wellDefined (Gamma |+ (x, Word)) f, wellDefined Gamma el of
    |SOME Word, SOME (List n) => SOME (List n)
    | _, _ => NONE) /\
  wellDefined Gamma (Mapvl x f vl) =
    (case wellDefined (Gamma |+ (x, Word)) f of
    | SOME Word => SOME (List (LENGTH vl))
    | _ => NONE) /\
  wellDefined Gamma (Fold (x,y) f i el) =
    (case wellDefined ((Gamma |+ (y, Word)) |+ (x, Word)) f,
      wellDefined Gamma i, wellDefined Gamma el of
    |SOME Word, SOME Word, SOME (List _)=> SOME Word
    |_, _, _ => NONE) /\
  wellDefined Gamma (Foldvl (x,y) f i vl) =
    (case wellDefined ((Gamma |+ (y, Word)) |+ (x, Word)) f,
      wellDefined Gamma i of
    |SOME Word, SOME Word => SOME Word
    |_, _ => NONE)
  /\
  wellDefinedL Gamma [] = SOME (List 0)/\
  wellDefinedL Gamma (e::el) =
    (case wellDefined Gamma e of
    |SOME Word =>
      (case wellDefinedL Gamma el
      of SOME (List n) => SOME (List (n+1))
        |_ => NONE)
    | _ => NONE)
  /\
  wellDefinedBool Gamma (Bconst b) = T /\
  wellDefinedBool Gamma (Pred p e) =
    (case wellDefined Gamma e of
    | SOME Word => T
    | _ => F) /\
  wellDefinedBool Gamma (Cmp _ e1 e2) =
    (case wellDefined Gamma e1, wellDefined Gamma e2 of
    | SOME Word, SOME Word => T
    | _, _ => F) /\
  wellDefinedBool Gamma (Cscope _ e) = wellDefinedBool Gamma e`
  (WF_REL_TAC
    `measure
      (\x. case x of
        |INL (_,e) => expr_size e
        |INR (INL (_, els)) => expr1_size els
        |INR (INR (_, e)) => cexpr_size e)`);

Theorem wellDefined_list_is_list
  `! Gamma l t. wellDefinedL Gamma l = SOME t ==>
    t = (List (LENGTH l)):Ftype`
  (Induct_on `l` \\ rw[wellDefined_def]
  \\ Cases_on `wellDefined Gamma h` \\ fs[]
  \\ Cases_on `x` \\ fs[]
  \\ Cases_on `wellDefinedL Gamma l` \\ fs[]
  \\ Cases_on `x` \\ fs[]
  \\ res_tac \\ rveq \\ fs[]);

val hasType_def = Define `
  hasType (Word) (Val _) = T /\
  hasType (List n) (Lval vl) = (LENGTH vl = n) /\
  hasType _ _ = F`;

Theorem list_acc_defined
  `! l n.
    n < LENGTH l ==>
  ? v. LLOOKUP l n = SOME v`
  (Induct_on `l` \\ rpt strip_tac \\ fs[]
  \\ Cases_on `n` \\ fs[LLOOKUP_def])

Theorem wellDefined_map_gives_eval
  `! vl Gamma f E n x.
    (! E Gamma t.
      (! x.
           x IN freeVarsExp f ==>
           ? v.
             E x = SOME v /\
             ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
      wellDefined Gamma f = SOME t ==>
      ? v. evalExpNonDet empty_cfg E f v /\ hasType t v) /\
    (! y. y IN freeVarsExp f /\ y <> x ==>
      ? v. E y = SOME v ∧
      ? t. FLOOKUP Gamma y = SOME t ∧ hasType t v) /\
    LENGTH vl = n /\
    wellDefined (Gamma |+ (x,Word)) f = SOME Word ==>
    ? v. evalExpNonDet empty_cfg E (Mapvl x f vl) v /\ hasType (List n) v`
  (Induct_on `vl` \\ rpt strip_tac
  \\ rveq \\ fs[evalExpNonDet_cases, hasType_def]
  \\ res_tac
  \\ first_x_assum (qspec_then `bind E x (Val h)` destruct) \\ fs[]
  >- (rpt strip_tac \\ fs[bind_def]
      \\ Cases_on `x' = x` \\ fs[FLOOKUP_DEF, hasType_def]
      \\ first_x_assum (qspec_then `x'` destruct) \\ fs[]
      \\ Q.ISPEC_THEN `Gamma` drule NOT_EQ_FAPPLY
      \\ disch_then (qspec_then `Word` assume_tac) \\ fs[])
  \\ Cases_on `v` \\ fs[hasType_def]
  \\ Cases_on `v'4'` \\ fs[hasType_def]
  \\ qexists_tac `Lval (v::l)` \\ fs[hasType_def]);

Theorem wellDefined_fold_gives_eval
  `! vl i f x y E Gamma v.
    (! z.
      z IN freeVarsExp f ∧ z ≠ x ∧ z ≠ y ∨ z IN freeVarsExp i ⇒
        ? v. E z = SOME v /\
        ? t. FLOOKUP Gamma z = SOME t /\
             hasType t v) ==>
    (! E.
      (! z. z IN freeVarsExp f ==>
        ? v. E z = SOME v /\
        ? t. FLOOKUP (Gamma |+ (y,Word) |+ (x,Word)) z = SOME t /\
             hasType t v) ==>
      ? v. evalExpNonDet empty_cfg E f v /\ hasType Word v) /\
    evalExpNonDet empty_cfg E i v /\
    hasType Word v ==>
    ? v. evalExpNonDet empty_cfg E (Foldvl (x,y) f i vl) v /\ hasType Word v`
  (Induct_on `vl` \\ rpt strip_tac \\ fs[evalExpNonDet_cases, hasType_def]
  >- (find_exists_tac \\ fs[])
  \\ res_tac
  \\ ntac 8 (pop_assum kall_tac)
  \\ first_x_assum (qspec_then `bind (bind E y (Val h)) x v'` destruct)
  >- (fs[bind_def] \\ rpt strip_tac
      \\ Cases_on `z = x` \\ fs[FLOOKUP_DEF]
      \\ Cases_on `z = y` \\ fs[FLOOKUP_DEF]
      >- (Q.ISPEC_THEN `Gamma |+ (y, Word)` drule NOT_EQ_FAPPLY
          \\ disch_then (qspec_then `Word` assume_tac) \\ fs[hasType_def])
      \\ first_x_assum (qspec_then `z` destruct) \\ fs[]
      \\ Q.ISPEC_THEN `Gamma` drule NOT_EQ_FAPPLY
      \\ disch_then (qspec_then `Word` assume_tac) \\ fs[hasType_def]
      \\ Q.ISPECL_THEN [`Gamma |+ (y, Word)`, `z`, `x`, `Word`] destruct NOT_EQ_FAPPLY
      \\ fs[])
  \\ qexists_tac `v'3'` \\ fs[]
  \\ find_exists_tac \\ fs[]);

Theorem hasType_wrapped
  `! t v. hasType t v ==> hasType t (wrapScope v)`
  (rpt strip_tac \\ Cases_on `v` \\ Cases_on `t`
  \\ fs[hasType_def, wrapScope_def]
  \\ pop_assum mp_tac \\ qid_spec_tac `n` \\ qid_spec_tac `l`
  \\ Induct_on `l` \\ fs[wrapAll_def]);

(**
  Soundness theorem for the type checker.
  If the type checker succeeds and all free variables are bound to a value
  the expression must evaluate and the result type agrees with the inferred type
**)
Theorem wellDefined_gives_eval
  `(! e E Gamma t.
    (! x. x IN freeVarsExp e ==>
      ? v. E x = SOME v /\ ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
    wellDefined Gamma e = SOME t ==>
    ? v. evalExpNonDet empty_cfg E e v /\ hasType t v) /\
  (! c E Gamma.
    (! x. x IN freeVarsCexp c ==>
      ? v. E x = SOME v /\ ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
    wellDefinedBool Gamma c ==>
    ? cv. evalCexpNonDet empty_cfg E c cv) /\
  (! l E Gamma n.
    (! x. x IN freeVarsExpl l ==>
      ? v. E x = SOME v /\ ? t. FLOOKUP Gamma x = SOME t /\ hasType t v) /\
    wellDefinedL Gamma l = SOME (List n) ==>
    ? vl. evalExpNonDet empty_cfg E (List l) vl /\ hasType (List n) vl)`
  (ho_match_mp_tac expr_induction
  \\ rpt strip_tac
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def] \\ rveq
      \\ fs[hasType_def])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def])
  >- (imp_res_tac wellDefined_list_is_list \\ rveq
      \\ fs[wellDefined_def]
      \\ Cases_on `wellDefinedL Gamma l` \\ fs[]
      \\ Cases_on `x` \\ fs[freeVarsExp_def]
      \\ res_tac \\ rveq
      \\ asm_exists_tac \\ fs[])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e` \\ fs[]
      \\ Cases_on `x` \\ fs[]
      \\ res_tac \\ rveq
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ rveq \\ drule list_acc_defined
      \\ disch_then assume_tac \\ fs[]
      \\ qexists_tac `Val v` \\ fs[hasType_def]
      \\ qexists_tac `l` \\ fs[])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ fs[empty_cfg_def]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ qexists_tac `Val (UnVal u v'')` \\ fs[hasType_def])
  >- (rename1 `Binop b e1 e2`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ Cases_on `wellDefined Gamma e2` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ fs[empty_cfg_def]
      \\ rpt (first_x_assum (qspec_then `E` destruct)) \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ Cases_on `v'` \\ fs[hasType_def]
      \\ qexists_tac `Val (BinVal b v'' v)` \\ fs[hasType_def])
  >- (rename1 `Terop t e1 e2 e3`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ Cases_on `wellDefined Gamma e2` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ Cases_on `wellDefined Gamma e3` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ fs[empty_cfg_def]
      \\ rpt (first_x_assum (qspec_then `E` destruct)) \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ Cases_on `v'` \\ fs[hasType_def]
      \\ Cases_on `v''` \\ fs[hasType_def]
      \\ qexists_tac `Val (TerVal t v'3' v v')` \\ fs[hasType_def])
  >- (rename1 `Let x e1 e2`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ res_tac
      \\ first_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ first_x_assum (qspec_then `bind E x v` destruct) \\ fs[bind_def]
      >- (rpt strip_tac \\ Cases_on `x'' = x` \\ fs[]
          >- (rveq \\ fs[FLOOKUP_DEF])
          \\ first_x_assum (qspec_then `x''` destruct)
          \\ fs[FLOOKUP_DEF]
          \\ Q.ISPECL_THEN [`Gamma`, `x''`, `x`, `x'`] destruct NOT_EQ_FAPPLY
          \\ fs[])
      \\ rpt (find_exists_tac \\ fs[]))
  >- (rename1 `Cond c e1 e2`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefinedBool Gamma c` \\ fs[]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ Cases_on `wellDefined Gamma e2` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ first_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ Cases_on `cTree2IEEE cv`
      >- (first_x_assum (qspec_then `E` destruct) \\ fs[]
          \\ qexists_tac `v` \\ fs[]
          \\ DISJ1_TAC \\ find_exists_tac \\ fs[])
      >- (last_x_assum (qspec_then `E` destruct) \\ fs[]
          \\ qexists_tac `v` \\ fs[]
          \\ DISJ2_TAC \\ find_exists_tac \\ fs[])
      >- (first_x_assum (qspec_then `E` destruct) \\ fs[]
          \\ qexists_tac `v` \\ fs[]
          \\ DISJ1_TAC \\ find_exists_tac \\ fs[])
      \\ last_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ qexists_tac `v` \\ fs[]
      \\ DISJ2_TAC \\ find_exists_tac \\ fs[])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `s` \\ fs[]
      \\ res_tac
      \\ qexists_tac `wrapScope v` \\ fs[]
      \\ conj_tac
      >- (qexists_tac `v` \\ fs[]
          \\ irule (CONJUNCT1 evalNonDet_cfg_det)
          \\ fs[empty_cfg_def]
          \\ find_exists_tac \\ fs[])
      \\ irule hasType_wrapped
      \\ fs[])
  >- (rename1 `Map x f e1`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined (Gamma |+ (x,Word)) f` \\ fs[]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x''` \\ fs[]
      \\ Cases_on `x'` \\ fs[]
      \\ rveq
      \\ qpat_x_assum `wellDefined Gamma e1 = _` resolve_tac
      \\ first_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ drule wellDefined_map_gives_eval
      \\ disch_then (qspecl_then [`l`, `Gamma`, `E`, `n`, `x`] destruct)
      \\ fs[]
      \\ qexists_tac `v` \\ fs[] \\ find_exists_tac \\ fs[])
  >- (fs[wellDefined_def]
      \\ Cases_on `wellDefined (Gamma |+ (s,Word)) e` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ irule wellDefined_map_gives_eval \\ fs[]
      \\ conj_tac
      >- (first_x_assum MATCH_ACCEPT_TAC)
      \\ qexists_tac `Gamma` \\ fs[freeVarsExp_def])
  >- (Cases_on `p` \\ rename1 `Fold (x,y) f i e1`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined (Gamma |+ (y, Word) |+ (x,Word)) f` \\ fs[]
      \\ Cases_on `x'` \\ fs[]
      \\ Cases_on `wellDefined Gamma i` \\ fs[]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x''` \\ fs[]
      \\ Cases_on `x'` \\ fs[]
      \\ qpat_x_assum `wellDefined Gamma e1 = _` resolve_tac
      \\ first_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def] \\ rveq
      \\ qpat_x_assum `wellDefined _ i = _` resolve_tac
      \\ first_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ qpat_x_assum `wellDefined _ f = _` resolve_tac
      \\ first_x_assum (mp_then Any assume_tac wellDefined_fold_gives_eval)
      \\ qpat_x_assum `evalExpNonDet empty_cfg E i v` resolve_tac
      \\ first_x_assum (qspec_then `l` destruct) \\ fs[]
      >- (rw[] \\ first_x_assum irule \\ fs[])
      \\ qexists_tac `v'` \\ fs[]
      \\ find_exists_tac \\ fs[])
  >- (Cases_on `p` \\ rename1 `Foldvl (x,y) f i vl`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined (Gamma |+ (y, Word) |+ (x,Word)) f` \\ fs[]
      \\ Cases_on `wellDefined Gamma i` \\ fs[]
      \\ Cases_on `x''` \\ fs[]
      \\ Cases_on `x'` \\ fs[] \\ rveq
      \\ drule wellDefined_fold_gives_eval
      \\ res_tac
      \\ last_x_assum (qspec_then `E` destruct) \\ fs[]
      \\ disch_then (qspecl_then [`vl`, `v`] destruct)  \\ fs[]
      \\ find_exists_tac \\ fs[])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def])
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ fs[empty_cfg_def]
      \\ Cases_on `p` \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ find_exists_tac \\ fs[])
  >- (rename1 `Cmp cmp e1 e2`
      \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `wellDefined Gamma e1` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ Cases_on `wellDefined Gamma e2` \\ fs[]
      \\ Cases_on `x` \\ fs[] \\ rveq
      \\ res_tac
      \\ fs[empty_cfg_def]
      \\ rpt (first_x_assum (qspec_then `E` destruct)) \\ fs[]
      \\ Cases_on `v` \\ fs[hasType_def]
      \\ Cases_on `v'` \\ fs[hasType_def]
      \\ rpt (find_exists_tac \\ fs[]))
  >- (fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
      \\ Cases_on `s` \\ fs[]
      \\ res_tac
      \\ qexists_tac `cv` \\ fs[]
      \\ irule (CONJUNCT2 evalNonDet_cfg_det)
      \\ fs[empty_cfg_def]
      \\ find_exists_tac \\ fs[])
  >- (fs[wellDefined_def, evalExpNonDet_cases]
      \\ rveq \\ fs[hasType_def])
  \\ fs[wellDefined_def, evalExpNonDet_cases, freeVarsExp_def]
  \\ Cases_on `wellDefined Gamma e` \\ fs[]
  \\ Cases_on `x` \\ fs[]
  \\ Cases_on `wellDefinedL Gamma l` \\ fs[]
  \\ Cases_on `x` \\ fs[] \\ rveq
  \\ res_tac
  \\ rpt (first_x_assum (qspec_then `E` destruct) \\ fs[])
  \\ Cases_on `v` \\ fs[hasType_def]
  \\ Cases_on `vl` \\ fs[hasType_def]
  \\ qexists_tac `Lval (v'::l')` \\ fs[hasType_def]);

val _ = export_theory ();
